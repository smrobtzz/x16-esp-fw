## Updating ESP32 Over WiFi
This method requires having already connected Calypso to WiFi using `cfg.prg`. If this method doesn't work, or you haven't connected to WiFi, you can try [Updating ESP32 Over USB](#updating-esp32-over-usb).

1. Download the latest `x16_esp_fw.bin` from [here](https://codeberg.org/smrobtzz/x16-esp-fw/releases).

2. Install using [caly](https://codeberg.org/smrobtzz/caly):
```
caly update x16_esp_fw.bin
```
Or `curl`:
```
curl -X POST --data-binary @x16_esp_fw.bin http://calypso.local:8080/esp32.bin
```

3. Calypso is now up to date

## Updating ESP32 Over USB
If you haven't connected Calypso to WiFi yet or had trouble updating its firmware over WiFi, you can update the USB Type C port.

1. Download the latest `bootloader.bin`, `partition-table.bin`, `ota_data_initial.bin`, and `x16_esp_fw.bin` from [here](https://codeberg.org/smrobtzz/x16-esp-fw/releases).

2. Download the lastest version of esptool from [here](https://github.com/espressif/esptool/releases).

3. Extract `esptool` (or `esptool.exe`), and put the 4 files from earlier into the same directory.

4. Plug Calypso into your computer and run the following command:
```
esptool -b 921600 --before=default_reset --after=hard_reset write_flash --flash_mode dio --flash_freq 80m --flash_size 4MB 0x0 bootloader.bin 0x8000 partition-table.bin 0xd000 ota_data_initial.bin 0x10000 x16_esp_fw.bin
```

5. Calypso is now up to date
