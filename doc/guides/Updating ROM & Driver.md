# Updating X16 ROM & Driver

What ROM and Calypso driver (if any) are already on your X16 determine what you need to update:

- If you have ROM R48 or earlier and haven't installed the Calypso driver before, you need to update both. See [Updating Both](#updating-both) for instructions.
- If you have the custom Calypso ROM Release 8 or earlier, you need to update both. See [Updating Both](#updating-both) for instructions.
- If you have the custom Calypso ROM Release 9 or later, you only need to update the Calypso driver. See [Updating Driver Only](#updating-driver-only) for instructions.

## Updating Both

1. Download the following files and put them on your X16's SD card:
- The latest custom Calypso ROM release from [here](https://codeberg.org/smrobtzz/x16-rom/releases). The file you want is called `rom.bin`.
- The latest Calypso driver [here](https://codeberg.org/smrobtzz/x16-esp-fw/releases). The file you want is called `drvinst.prg`.
- X16 update utility from [here](https://github.com/FlightControl-User/x16-flash/releases/download/r3.0.0/CX16-UPDATE-R3.0.0.PRG).

2. If Calypso is currently installed on your X16, power your X16 off and remove Calypso.

3. Turn your X16 on and run `CX16-UPDATE-R3.0.0.PRG` and follow the prompts to update the ROM.

4. Run `drvinst.prg` & follow the instructions on screen to install the Calypso driver.

5. The X16 ROM and Calypso driver are now up to date. In the future, you should only need to updat the Calypso driver.

## Updating Driver Only

1. Download the following file and put it on your X16's SD card:
- The latest Calypso driver [here](https://codeberg.org/smrobtzz/x16-esp-fw/releases). The file you want is called `drvinst.prg`.

2. Run `drvinst.prg` & follow the instructions on screen to install the Calypso driver.

3. The Calypso driver is now up to date.
