# Calypso First Time Setup
## 1. Update X16 ROM & Calypso Driver
Follow [this guide](Updating%20ROM%20&%20Driver.md) update your X16 ROM and the Calypso driver.

## 2. Format your microSD card & insert it into Calypso
The microSD card used in Calypso must use MBR and have a single FAT32 partition. The partition can be any size.

- These are the same requirements as the X16 kernel.
- Do not use exFAT.
- exFAT is not supported.
- NTFS is not supported.
- Any other filesystem is not supported and will not work.
- If you just bought a microSD card it is probably formatted with exFAT.
- Format it with FAT32.

## 3. Install & enable Calypso
1. Turn off your X16
2. Ensure Calypso is disconnected from power and install it on top of the VERA module on your X16. No SD card should be in the integrated SD card on VERA.
3. Turn your X16 on and run the MENU command

![X16 startup screen with MENU typed in](images/x16_1.png)

4. Navigate to `CONTROL PANEL` using the arrow keys and press enter to open it.

![X16 MENU with CONTROL PANEL selected](images/x16_2.png)

5. Navigate to `DEVICES` and open it.

![X16 CONTROL PANEL with DEVICES selected](images/x16_3.png)

6. Change `#8` to `CALYPSO` using the left or right arrow key. Then navigate to `EXIT` and press enter to go back.

![CONTROL PANEL DEVICES menu with CALYPSO next to #8](images/x16_4.png)

7. Navigate to `SAVE SETTINGS` and press enter __three__ times to save your changes. Then reset your X16.

![X16 CONTROL PANEL with SAVE SETTINGS selected](images/x16_5.png)

### 4. Configure Calypso

1. LOAD and RUN `cfg.prg`. This program is included with Calypso so you don't need to download anything.

2. Use the `Configure WiFi` option to connect to a WiFi network.

3. Select the `Save settings to flash` option when you are done, then reset your X16.
