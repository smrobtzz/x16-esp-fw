## DOS Interface
### About
The firmware contains a custom implementation of the Commodore DOS interface.

These tables were borrowed from [the CMDR-DOS guide](https://github.com/X16Community/x16-docs/blob/master/X16%20Reference%20-%2013%20-%20Working%20with%20CMDR-DOS.md) and show what DOS syntax and commands are currently supported.

### Feature Support
Of the core feature set, these are the supported functions:

| Feature                   | Syntax                        | Supported | Comment |
|---------------------------|-------------------------------|-----------|---------|
| Reading                   | `,?,R`                        | yes       |         |
| Writing                   | `,?,W`                        | yes       |         |
| Appending                 | `,?,A`                        | yes       |         |
| Modifying                 | `,?,M`                        | yes       |         |
| Types                     | `,S`/`,P`/`,U`/`,L`           | yes       | ignored on FAT32 |
| Overwriting               | `@:`                          | yes       |         |
| Magic channels 0/1        |                               | yes       |         |
| Channel 15 command        | _command_`:`_args_...         | yes       |         |
| Channel 15 status         | _code_`,`_string_`,`_a_`,`_b_ | yes       |         |
| CMD partition syntax      | `0:`/`1:`/...                 | never     |         |
| CMD subdirectory syntax   | `//DIR/:`/`/DIR/:`            | yes       |         |
| Directory listing         | `$`                           | yes       |         |
| Dir with name filtering   | `$:FIL*`                      | not yet   |         |
| Dir with name and type filtering   | `$:*=P`/`$:*=D`/`$:*=A` | yes    |         |
| Dir with timestamps       | `$=T`                         | yes       | with ISO-8601 times |
| Dir with time filtering   | `$=T<`/`$=T>`                 | not yet   |         |
| Dir long listing          | `$=L`                         | yes       | shows human readable file size instead of blocks, time in ISO-8601 syntax, attribute byte, and exact file size in hexadecimal |
| Partition listing         | `$=P`                         | not yet   |         |
| Partition filtering       | `$:NAME*=P`                   | not yet   |         |
| Current Working Directory | `$=C`                         | yes       |         |

And this table shows which of the standard commands are supported:

| Name             | Syntax                                                | Description                     | Supported |
|------------------|-------------------------------------------------------|---------------------------------|-----------|
| BLOCK-ALLOCATE   | `B-A` _medium_ _medium_ _track_ _sector_              | Allocate a block in the BAM     | never |
| BLOCK-EXECUTE    | `B-E` _channel_ _medium_ _track_ _sector_             | Load and execute a block        | never |
| BLOCK-FREE       | `B-F` _medium_ _medium_ _track_ _sector_              | Free a block in the BAM         | never |
| BLOCK-READ       | `B-R` _channel_ _medium_ _track_ _sector_             | Read block                      | never |
| BLOCK-STATUS     | `B-S` _channel_ _medium_ _track_ _sector_             | Check if block is allocated     | never |
| BLOCK-WRITE      | `B-W` _channel_ _medium_ _track_ _sector_             | Write block                     | never |
| BUFFER-POINTER   | `B-P` _channel_ _index_                               | Set r/w pointer within buffer   | never |
| CHANGE DIRECTORY | `CD`[_path_]`:`_name_                                 | Change the current sub-directory| yes   |
| CHANGE DIRECTORY | `CD`[_medium_]`:←`                                    | Change sub-directory up         | yes   |
| CHANGE PARTITION | `CP` _num_                                            | Make a partition the default    | never |
| COPY             | `C`[_path_a_]`:`_target_name_`=`[_path_b_]`:`_source_name_[`,`...] | Copy/concatenate files | yes   |
| COPY             | `C`_dst_medium_`=`_src_medium_                        | Copy all files between disk     | not yet |
| DUPLICATE        | `D:`_dst_medium_``=``_src_medium_                     | Duplicate disk                  | not yet |
| FILE LOCK        | `F-L`[_path_]`:`_name_[`,`...]                        | Enable file write-protect       | never |
| FILE RESTORE     | `F-R`[_path_]`:`_name_[`,`...]                        | Restore a deleted file          | never |
| FILE UNLOCK      | `F-U`[_path_]`:`_name_[`,`...]                        | Disable file write-protect      | never |
| GET DISKCHANGE   | `G-D`                                                 | Query disk change               | ???   |
| GET PARTITION    | `G-P`[_num_]                                          | Get information about partition | ???   |
| INITIALIZE       | `I`[_medium_]                                         | Re-mount filesystem             | never |
| LOCK             | `L`[_path_]`:`_name_                                  | Toggle file write protect       | never |
| MAKE DIRECTORY   | `MD`[_path_]`:`_name_                                 | Create a sub-directory          | yes   |
| MEMORY-EXECUTE   | `M-E` _addr_lo_ _addr_hi_                             | Execute code                    | never |
| MEMORY-READ      | `M-R` _addr_lo_ _addr_hi_ [_count_]                   | Read RAM                        | never |
| MEMORY-WRITE     | `M-W` _addr_lo_ _addr_hi_ _count_ _data_              | Write RAM                       | never |
| NEW              | `N`[_medium_]`:`_name_`,`_id_`,FAT32`                 | File system creation            | not yet |
| PARTITION        | `/`[_medium_][`:`_name_]                              | Select 1581 partition           | never |
| PARTITION        | `/`[_medium_]`:`_name_`,`_track_ _sector_ _count_lo_ _count_hi_ `,C` | Create 1581 partition | never |
| POSITION         | `P` _channel_ _p0_ _p1_ _p2_ _p3_                     | Set position within file (like sd2iec); all args binary | yes |
| REMOVE DIRECTORY | `RD`[_path_]`:`_name_                                 | Delete a sub-directory          | yes   |
| RENAME           | `R`[_path_]`:`_new_name_`=`_old_name_                 | Rename file                     | not yet |
| RENAME-HEADER    | `R-H`[_medium_]`:`_new_name_                          | Rename a filesystem             | not yet |
| RENAME-PARTITION | `R-P:`_new_name_`=`_old_name_                         | Rename a partition              | not yet |
| SCRATCH          | `S`[_path_]`:`_pattern_[`,`...]                       | Delete files                    | yes   |
| SWAP             | `S-`{`8`&#x7c;`9`&#x7c;`D`}                           | Change primary address          | never |
| TELL             | `T` _channel_                                         | Return the current position within a file and the file's size; channel arg is binary | yes |
| TIME READ ASCII  | `T-RA`                                                | Read Time/Date (ASCII)          | never |
| TIME READ BCD    | `T-RB`                                                | Read Time/Date (BCD)            | never |
| TIME READ DECIMAL| `T-RD`                                                | Read Time/Date (Decimal)        | never |
| TIME READ ISO    | `T-RI`                                                | Read Time/Date (ISO)            | never |
| TIME WRITE ASCII | `T-WA` _dow_ _mo_`/`_da_`/`_yr_ _hr_`:`_mi_`:`_se_ _ampm_ | Write Time/Date (ASCII)     | never |
| TIME WRITE BCD   | `T-WB` _b0_ _b1_ _b2_ _b3_ _b4_ _b5_ _b6_ _b7_ _b8_   | Write Time/Date (BCD)           | never |
| TIME WRITE DECIMAL| `T-WD` _b0_ _b1_ _b2_ _b3_ _b4_ _b5_ _b6_ _b7_       | Write Time/Date (Decimal)       | never |
| TIME WRITE ISO   | `T-WI` _yyyy_`-`_mm_`-`_dd_`T`_hh_`:`_mm_`:`_ss_ _dow_| Write Time/Date (ISO)           | never |
| U1/UA            | `U1` _channel_ _medium_ _track_ _sector_              | Raw read of a block             | never |
| U2/UB            | `U2` _channel_ _medium_ _track_ _sector_              | Raw write of a block            | never |
| U3-U8/UC-UH      | `U3` - `U8`                                           | Execute in user buffer          | never |
| U9/UI            | `UI`                                                  | Soft RESET                      | never |
| U:/UJ            | `UJ`                                                  | Hard RESET                      | never |
| USER             | `U0>` _pa_                                            | Set unit primary address        | never |
| USER             | `U0>B` _flag_                                         | Enable/disable Fast Serial      | never |
| USER             | `U0>D`_val_                                           | Set directory sector interleave | never |
| USER             | `U0>H` _number_                                       | Select head 0/1                 | never |
| USER             | `U0>L`_flag_                                          | Large REL file support on/off   | never |
| USER             | `U0>M` _flag_                                         | Enable/disable 1541 emulation mode| never |
| USER             | `U0>R` _num_                                          | Set number fo retries           | never |
| USER             | `U0>S` _val_                                          | Set sector interleave           | never |
| USER             | `U0>T`                                                | Test ROM checksum               | never |
| USER             | `U0>V` _flag_                                         | Enable/disable verify           | never |
| USER             | `U0>` _pa_                                            | Set unit primary address        | never |
| USER             | `UI`{`+`&#x7c;`-`}                                    | Use C64/VIC-20 Serial protocol  | never |
| UTILITY LOADER   | `&`[[_path_]`:`]_name_                                | Load and execute program        | never |
| VALIDATE         | `V`[_medium_]                                         | Filesystem check                | never |
| WRITE PROTECT    | `W-`{`0`&#x7c;`1`}                                    | Set/unset device write protect  | never |

The following special file syntax and `OPEN` options are from CMDR-DOS:

| Feature               | Syntax      | Description                                                                    | Supported |
|-----------------------|-------------|--------------------------------------------------------------------------------|-----------|
| Open for Read & Write | `,?,M`      | Allows arbitrarily reading, writing and setting the position (`P`)<sup>1</sup> | yes |
| Get current working directory | `$=C` | Produces a directory listing containing the name of the current working directory followed by all parent directory names all the way up to `/` | yes |
