## Network API
### About
A high-level network API is available for X16 programs to access the internet without having to worry about hardware-specific details.

The API is distributed as NET.BIN, which is compiled into the firmware. The API is designed to be replicated by other hardware / emulators, so other implementations may require placing NET.BIN on your SD card.

### Usage
To use the API, load NET.BIN into any RAM bank at $a000. Subroutines are implemented through a jump table at $a000. The current layout of the jump table is:

| Address      | Subroutine                      |
|--------------|---------------------------------|
| $a000        | net_open                        |
| $a003        | net_close                       |
| $a006        | net_send                        |
| $a009        | net_recv                        |
| $a00c        | net_poll                        |
| $a00f        | net_gethostbyname               |
| $a010..$a0ff | reserved for future subroutines |

Calling one of these functions can be done like:

```
jsr $a000      ; call net_open
```

All arguments are passed through direct page registers r0..r16, and the values of CPU registers after calls to the API should not be relied upon.

To make code easier to read & write, headers are provided for 64tass and prog8 that name these subroutines and their arguments.

- `x16/net/api.asm` - use with 64tass
- `x16/net/net.p8` - use with prog8

#### IP addresses
Some subroutines may utilize IP addresses in the `r1` through `r9l` registers. These consist of the bytes of an IPv4 or IPv6 in `r1` through `r8`, and the number `4` or `6` in `r9l`.

#### Sockets
Sockets are numbered from 0. The amount of available sockets is implementation-defined, but guaranted to be at least 4.

Types of sockets include:

| Name         | Value |
|--------------|-------|
| SOCK_RAW     | 0     |
| SOCK_TCP     | 1     |
| SOCK_DBG     | 2     |
| SOCK_INVALID | 3-255 |

##### SOCK_RAW  
`SOCK_RAW` sockets are raw IP sockets. Data sent to a `SOCK_RAW` socket must not include the IP header. Data received from a `SOCK_RAW` socket will include the IP header, however.

##### SOCK_TCP
Bidirectional TCP sockets. Currently, these sockets always behave as if `TCP_NODELAY` was set, but athat may become a configurable option in the future.

##### SOCK_DBG  
`SOCK_DBG` sockets are used to provide simple and extensible formatted output for debugging purposes.

Data sent to a `SOCK_DBG` socket takes consists of a one-byte "tag", followed by extra data specific to that tag.

| Tag            | Value | Data |
|----------------|-------|------|
| DBG_TAG_STR    | 0     | Null-terminated ASCII string with LF line endings |
| DBG_TAG_INT8	| 1     | Base (see below), 8-bit signed integer  |
| DBG_TAG_UINT8	| 2     | Base (see below), 8-bit unsigned integer  |
| DBG_TAG_INT16	| 3     | Base (see below), 16-bit signed integer  |
| DBG_TAG_UINT16	| 4     | Base (see below), 16-bit unsigned integer  |
| DBG_TAG_INT32	| 5     | Base (see below), 32-bit signed integer  |
| DBG_TAG_UINT32	| 6     | Base (see below), 32-bit unsigned integer  |

`DBG_BASE_HEX` = 0
`DBG_BASE_DEC` = 1

#### Result codes
TBD

## Subroutines

### net_open
Opens a socket.
##### Arguments
```
r1..r9l - IP address (see above)
r9h     - Socket
r10h    - Socket type
```
##### Return values
```
r1l     - Result code (see above)
```

### net_close
Closes a socket.
##### Arguments
```
r1l     - Socket
```
##### Return values
```
None (subject to change)
```

### net_send
Send data to socket. See above regarding specific considerations for `SOCK_RAW` and `SOCK_DBG` sockets.
##### Arugments
```
r1l      - Socket
r1h..r2l - Size of data
r2h..r3l - Address of data
```
##### Return values
```
r1l      - Result code (see above)
r1h..r2l - Size sent successfully
```

### net_recv
Receive data from socket. See above regarding specific considerations for `SOCK_RAW` sockets.
##### Arguments
```
r1l      - Socket
r1h..r2l - Size of buffer
r2h..r3l - Address of buffer
```
##### Return values
```
r1l      - Result code (see above)
r1h..r2l - Size received
```

### net_poll
Retrieve the current status of all sockets.
##### Arguments
```
None
```
##### Return values
```
1 byte status value for each socket, starting at r1l

[bit 0] - Data is available for reading (SOCK_STAT_HAS_DATA)
[bit 1] - Connected (SOCK_STAT_CONNECTED)
[bit 2] - An error occured (SOCK_STAT_ERROR)
[bits 3-4] - Reserved
[bits 5-7] - Error code (if applicable):
    0 - Connection aborted (SOCK_ERR_ABRT)
    1 - Connection reset (SOCK_ERR_RST)
    2 - Connection closed (SOCK_ERR_CLSD)
    7 - Other error occured (SOCK_ERR_OTHER)
```

### net_gethostbyname
Resolve a hostname to an IP address.
##### Arguments
```
r1       - Address of ASCII name
r2l      - Length of name
```
##### Return values
```
r1..r9l  - IP address (see above)
r9h      - Result code (see above)
```
