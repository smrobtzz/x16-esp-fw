; vim: set syntax=64tass
; SPDX-License-Identifier: GPL-3.0-only

        .cpu "65816"
*	= $a000
	.dsection jumptab
	.dsection code

	.section jumptab
	jmp net_open
	jmp net_close
	jmp net_send
	jmp net_recv
	jmp net_poll
	jmp net_gethostbyname
	.fill $a100 - *
	.endsection

	.section code
net_open .proc
	.if emu == 0
	pha
	lda #net.CMD_OPEN
	pha
	lda #24
	sta net.num_zp_args
	jmp spi_txrx_zp
	.else
	wdm #net.CMD_OPEN
	rts
	.endif
	.endproc

net_close .proc
	.if emu == 0
	pha
	lda #net.CMD_CLOSE
	pha
	lda #4
	sta net.num_zp_args
	jmp spi_txrx_zp
	.else
	wdm #net.CMD_CLOSE
	rts
	.endif
	.endproc

net_send .proc
	.if emu == 0
	pha
	lda #net.CMD_SEND
	pha
	lda #1
	sta spi_mutex
        #spi_start_request

        #spi_send_cmd

        ; Send socket
        lda net.send.socket
        sta VERA_SPI_DATA
        #wait_busy_or_nop
        ; Send size
        lda net.send.buf_sz
        sta VERA_SPI_DATA
        #wait_busy_or_nop
        lda net.send.buf_sz + 1
        sta VERA_SPI_DATA
        ; Send padding
        #wait_busy_or_nop
        sta VERA_SPI_DATA

        ; Send data
-	ldx #$ff
	ldy #0
	lda net.send.buf_sz + 1
	beq +
	dec a
	sta net.send.buf_sz + 1
	bra ++
+	ldx net.send.buf_sz
	stz net.send.buf_sz
	beq ++
+
-       lda (net.send.buf), y
        sta VERA_SPI_DATA
        iny
        dex
	bne -
	inc net.send.buf + 1
	bra --

        ; Pad to 4 bytes
+	lda net.send.buf_sz
	and #%00000111
	beq +
-	sta VERA_SPI_DATA
	ina
	and #%00000111
	bne -
+
        #spi_end_request
        #spi_start_response

	#wait_busy_or_nop
	; Receive length
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	ldx #0
	ldy VERA_SPI_DATA

	; Receive return values
-	beq +
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta net.zp_args, x
	inx
	dey
	bra -

+	#spi_end_response
	stz spi_mutex
	.else
	wdm #net.CMD_SEND
	.endif
	rts
	.endproc

net_recv .proc
	.if emu == 0
        pha
        lda #net.CMD_RECV
        pha
	lda #1
	sta spi_mutex
        #spi_start_request

        #spi_send_cmd

        ; Send socket
        lda net.recv.socket
        sta VERA_SPI_DATA
        #wait_busy_or_nop
        ; Send size
        lda net.recv.buf_sz
        sta VERA_SPI_DATA
        #wait_busy_or_nop
        lda net.recv.buf_sz + 1
        sta VERA_SPI_DATA
        ; Send padding
        #wait_busy_or_nop
        sta VERA_SPI_DATA

        #spi_end_request
        #spi_start_response
        #wait_busy_or_nop
        ; Receive length of return values
        stz VERA_SPI_DATA
	#wait_busy_or_nop
        ldx #0
	ldy VERA_SPI_DATA

	; Receive return values
-	beq +
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta net.zp_args, x
	inx
	dey
	bra -

+       ; Enable auto-tx
	#enable_cs_autotx
        ; Start auto-tx
        lda VERA_SPI_DATA

	lda net.recv.recv_sz
	pha
	lda net.recv.recv_sz + 1
	pha
	; Receive data
-	ldx #$ff
	ldy #0
	lda net.recv.recv_sz + 1
	beq +
	dec a
	sta net.recv.recv_sz + 1
	bra ++
+	ldx net.recv.recv_sz
	stz net.recv.recv_sz

	beq ++
+
- 	lda VERA_SPI_DATA
	sta (net.recv.buf), y
        iny
        dex
	bne -
	inc net.recv.buf + 1
	bra --

+	pla
	sta net.recv.recv_sz + 1
	pla
	sta net.recv.recv_sz

        ; Disable auto-tx
	#disable_autotx

        #spi_end_response
	stz spi_mutex
	.else
	wdm #net.CMD_RECV
	.endif
        rts
	.endproc

net_poll .proc
	.if emu == 0
	lda #1
	sta spi_mutex
	#enable_cs_autotx

	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA

	; ADDR = 10
	#wait_busy_or_nop
	lda #10
	sta VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; Start auto-tx
	#wait_busy_or_nop
	lda VERA_SPI_DATA

	ldx #0
	ldy #4
-	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta net.zp_args, x
	inx
	dey
	bne -

	#disable_cs

	stz spi_mutex
	.else
	; (CMD_POLL doesn't exist anymore)
	wdm #99
	.endif
	rts
	.endproc

net_gethostbyname .proc
	.if emu == 0
	pha
	lda #net.CMD_GETHOSTBYNAME
	pha
	lda #1
	sta spi_mutex
	#spi_start_request

        #spi_send_cmd

	; Send size
	lda net.gethostbyname.size
	sta VERA_SPI_DATA

	; Send data
	ldx net.gethostbyname.size
	ldy #0
-	lda (net.gethostbyname.buf), y
	sta VERA_SPI_DATA
	iny
	dex
	bne -

	; Pad to 4 bytes
	lda net.gethostbyname.size
	ina
	and #%00000111
	beq +
-	sta VERA_SPI_DATA
	ina
	and #%00000111
	bne -
+
	#spi_end_request
	#spi_start_response

	#wait_busy_or_nop
	; Receive length
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	ldx #0
	ldy VERA_SPI_DATA

	; Receive return values
-	beq +
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta net.zp_args, x
	inx
	dey
	bra -

+	#spi_end_response
	stz spi_mutex
	.else
	wdm #net.CMD_GETHOSTBYNAME
	.endif
	rts
	.endproc

spi_txrx_zp .proc
	lda #1
	sta spi_mutex
	#spi_start_request

	; Garbage padding bytes
	pha
	pha

	; Send 4 byte request
	ldx #4
	#wait_busy
-	pla
	sta VERA_SPI_DATA
	#wait_busy_or_nop
	dex
	bne -

	; Send function parameters
	ldx #0
	ldy net.num_zp_args
	#wait_busy
-	lda net.zp_args, x
	sta VERA_SPI_DATA
	#wait_busy_or_nop
	inx
	dey
	bne -

	#spi_end_request
	#spi_start_response

	#wait_busy_or_nop
	; Receive length
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	ldx #0
	ldy VERA_SPI_DATA

	; Receive return values
-	beq +
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta net.zp_args, x
	inx
	dey
	bra -

+	#spi_end_response
	stz spi_mutex
	rts
	.endproc
	.endsection
