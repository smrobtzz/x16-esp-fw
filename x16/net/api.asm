net	.namespace

num_zp_args	= $02
zp_args		= $04

CMD_OPEN           = 10
CMD_CLOSE          = 11
CMD_SEND           = 12
CMD_RECV           = 13
CMD_GETHOSTBYNAME  = 14

IP_V4	      = 4
IP_V6	      = 6

IP_PROTO_ICMP = 1

SOCK_RAW      = 0
SOCK_TCP      = 1
SOCK_DBG      = 2
SOCK_SERIAL   = 3

SOCK_STAT_HAS_DATA  = 1
SOCK_STAT_CONNECTED = 2

; SOCK_DBG data looks like:
; tag [1 byte]
; tag-specific data [some bytes]
; ...

DBG_TAG_STR	= 0
; tag-specific data:
; null-terminated ASCII string, newline is \n only

DBG_TAG_UINT8	= 2
DBG_TAG_UINT16	= 4
DBG_TAG_UINT32	= 6
	DBG_BASE_HEX = 0
	DBG_BASE_DEC = 1
; tag-specific data:
; base: hex (0), decimal (1), binary (2) [1 byte]
; integer [1, 2, or 4 bytes]

DBG_TAG_INT8	= 1
DBG_TAG_INT16	= 3
DBG_TAG_INT32	= 5
; tag-specific data:
; integer [1, 2, or 4 bytes]

		.virtual $a000
	open	.block
	ip	    = $0004
	ip_ty	    = $0014
	socket	    = $0015
	socket_ty   = $0016
	socket_arg0 = $0017
	socket_arg1 = $0018
		.endblock
		.endvirtual

		.virtual $a003
	close	.block
	socket	= $0004
		.endblock
		.endvirtual

		.virtual $a006
	send	.block
	socket	= $0004
	buf_sz	= $0005
	buf	= $0007

	res	= $0004
	sent_sz	= $0005
		.endblock
		.endvirtual

		.virtual $a009
	recv	.block
	socket	= $0004
	buf_sz	= $0005
	buf	= $0007

	res	= $0004
	recv_sz	= $0005
		.endblock
		.endvirtual

		.virtual $a00c
	poll	.block
	socket	= $0004
		.endblock
		.endvirtual

		.virtual $a00f
	gethostbyname	.block
	buf     = $0004
	size    = $0006
		.endblock
		.endvirtual

	FLOW_CTRL_NONE	   = 0
	FLOW_CTRL_XON_XOFF = 1
	FLOW_CTRL_CTS_RTS  = 2

	STOP_BITS_1   = 1
	STOP_BITS_1_5 = 2
	STOP_BITS_2   = 3

	PARITY_NONE = 0
	PARITY_EVEN = 2
	PARITY_ODD  = 3

	ser_arg0 .function baud, flow, data
		arg0 := 0

		.switch baud
		.case 9600
			arg0 |= 0
		.case 19200
			arg0 |= 1
		.case 38400
			arg0 |= 2
		.case 57600
			arg0 |= 3
		.case 115200
			arg0 |= 4
		.case 230400
			arg0 |= 5
		.case 460800
			arg0 |= 6
		.case 921600
			arg0 |= 7
		.default
			.error "invalid baud rate, valid baud rates are: 9600, 19200, 38400, 57600, 115200, 460800, 921600"
		.endswitch

		.switch flow
		.case FLOW_CTRL_NONE
			arg0 |= (FLOW_CTRL_NONE << 3)
		.case FLOW_CTRL_XON_XOFF
			arg0 |= (FLOW_CTRL_XON_XOFF << 3)
		.case FLOW_CTRL_CTS_RTS
			arg0 |= (FLOW_CTRL_CTS_RTS << 3)
		.default
			.error "invalid flow control mode, valid modes are: FLOW_CTRL_NONE, FLOW_CTRL_XON_XOFF, FLOW_CTRL_CTS_RTS"
		.endswitch

		.switch data
		.case 5
			arg0 |= (0 << 5)
		.case 6
			arg0 |= (1 << 5)
		.case 7
			arg0 |= (2 << 5)
		.case 8
			arg0 |= (3 << 5)
		.default
			.error "invalid number of data bits, valid values are: 5, 6, 7, 8"
		.endswitch
	.endfunction arg0

	ser_arg1 .function stop, parity
		arg1 := 0

		.switch stop
		.case STOP_BITS_1
			arg1 |= STOP_BITS_1
		.case STOP_BITS_1_5
			arg1 |= STOP_BITS_1_5
		.case STOP_BITS_2
			arg1 |= STOP_BITS_2
		.default
			.error "invalid number of stop bits, valid values are: STOP_BITS_1, STOP_BITS_1_5, STOP_BITS_2"
		.endswitch

		.switch parity
		.case PARITY_NONE
			arg1 |= (PARITY_NONE << 2)
		.case PARITY_EVEN
			arg1 |= (PARITY_EVEN << 2)
		.case PARITY_ODD
			arg1 |= (PARITY_ODD << 2)
		.default
			.error "invalid parity, valid values are: PARITY_NONE, PARITY_EVEN, PARITY_ODD"
		.endswitch
	.endfunction arg1
	.endnamespace
