; vim: set syntax=64tass
*	= $0022
	.dsection zp

*	= $0801
	.byte $0b, $08
	.byte $d9, $07
	.byte $9e
	.null "2061"
	.word $0000

	.dsection main
	.dsection code
	.dsection data

	SETNAM = $ffbd
	SETLFS = $ffba
	LOAD = $ffd5
	CHROUT = $ffd2
	GETIN = $ffe4
	screen_set_charset = $ff62

	.section main
main
	; Load net.bin into RAM bank 1
	lda #1
	lda #size(net_bin_str)
	ldx #<net_bin_str
	ldy #>net_bin_str
	jsr SETNAM

	lda #0
	ldx #8
	ldy #2
	jsr SETLFS

	ldx #<$a000
	ldy #>$a000
	lda #0
	jsr LOAD

	lda #1
	jsr screen_set_charset

	stz net.open.socket
	lda #net.SOCK_SERIAL
	sta net.open.socket_ty
	lda #net.ser_arg0(115200, net.FLOW_CTRL_NONE, 8)
	sta net.open.socket_arg0
	lda #net.ser_arg1(net.STOP_BITS_1, net.PARITY_NONE)
	sta net.open.socket_arg1
	jsr net.open

_poll	jsr net.poll
	lda net.poll.socket
	and #net.SOCK_STAT_HAS_DATA
	bne _recv
	jsr GETIN
	beq _poll
	bra _send

_recv	stz net.recv.socket
	lda #1
	sta net.recv.buf_sz
	stz net.recv.buf_sz + 1
	lda #<recvbuf
	sta net.recv.buf
	lda #>recvbuf
	sta net.recv.buf + 1
	jsr net.recv
	ldx net.recv.recv_sz
	beq _poll

	lda recvbuf
	jsr CHROUT

	bra _poll

_send	sta sndbuf
	stz net.send.socket
	lda #1
	sta net.send.buf_sz
	stz net.send.buf_sz + 1
	lda #<sndbuf
	sta net.send.buf
	lda #>sndbuf
	sta net.send.buf + 1
	jsr net.send
	bra _poll
	.endsection

	.section zp
recvbuf	.fill 1, ?
sndbuf	.fill 1, ?
	.endsection

	.section data
net_bin_str	.text "net.bin"
	.endsection
