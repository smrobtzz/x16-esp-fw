; vim: set syntax=64tass
	.section code

ansi_term .proc
	; ISO mode
	lda #$0f
	jsr kernal.CHROUT

	; CP437
	lda #7
	jsr kernal.screen_set_charset

	jsr ansi_palette
	stz isesc
	stz iscs
	stz xpos
	stz ypos
	lda #$0f
	sta color

	stz vera.addrl
	lda #$b0
	sta vera.addrm
	lda #%00010001
	sta vera.addrh

poll	sei
	jsr net.poll
	cli
	lda net.poll.socket
	and #net.SOCK_STAT_HAS_DATA
	beq key

	stz net.recv.socket
	lda #32
	sta net.recv.buf_sz
	stz net.recv.buf_sz + 1
	lda #<chrbuf
	sta net.recv.buf
	lda #>chrbuf
	sta net.recv.buf + 1
	sei
	jsr net.recv
	cli

	ldx #0
-	cpx net.recv.recv_sz
	beq key
	lda chrbuf, x
	phx
	jsr ansi_in
	plx
	inx
	bra -

key	jsr kernal.kbdbuf_get_modifiers
	bit #%00000010	; alt
	bne keyalt
	jsr kernal.GETIN
	cmp #0
	beq poll
	jsr ansi_out
	bra poll

keyalt	jsr kernal.GETIN
	cmp #0
	beq poll
	cmp #189
	bne +
	bra quit
+	bra poll

quit	stz net.close.socket
	jsr net.close
	jmp kernal.enter_basic
	.endproc

ansi_palette .proc
	stz vera.ctrl
	stz vera.addrl
	lda #$fa
	sta vera.addrm
	lda #%00010001
	sta vera.addrh

	ldx #0
-	lda palette, x
	sta vera.data0
	inx
	cpx #32
	bne -

	rts
	.endproc

; Input character
ansi_in .proc
	ldy iscs
	beq +
	jmp ansi_in_cs
+	ldy isesc
	beq +
	jmp ansi_in_esc

+	cmp #$0a
	bne +
	inc ypos
	inc vera.addrm
	stz xpos
	stz vera.addrl
	rts

+	cmp #$1b
	bne +
	sta isesc
	stz nescpar
	stz escpar
	stz escpar + 1
	rts

+	sta vera.data0
	lda color
	sta vera.data0
	inc xpos
	rts
	.endproc

; Input character following ESC
ansi_in_esc .proc
	cmp #'['
	bne +
	sta iscs
	rts
+	brk
	.endproc

; Input character following CSI
ansi_in_cs .proc
	cmp #';'
	beq semi
	cmp #$40
	bcs final
	; Assume it's a number
	; escpar[nescpar] *= 10 + (c - $30)
	sec
	sbc #$30
	pha
	ldy nescpar
	lda escpar, y
	asl a
	asl a
	asl a
	clc
	adc escpar, y
	adc escpar, y
	sta escpar, y
	pla
	adc escpar, y
	sta escpar, y
	rts

semi	inc nescpar
	rts

final	stz isesc
	stz iscs
	cmp #'A'	; cursor up
	bne +
	ldx xpos
	ldy ypos
	dey
	jmp goto_pos
+	cmp #'B'	; cursor down
	bne +
	ldx xpos
	ldy ypos
	iny
	jmp goto_pos
+	cmp #'C'	; cursor right
	bne +
	ldx xpos
	inx
	ldy ypos
	jmp goto_pos
+	cmp #'D'	; cursor left
	bne +
	ldx xpos
	dex
	ldy ypos
	jmp goto_pos
+	cmp #'H'	; cursor position
	bne +
	; need to adjust from 1-based to 0-based
	ldx escpar + 1
	beq xzero
	dex
xzero	ldy escpar
	beq yzero
	dey
yzero	jmp goto_pos

+	cmp #'m'
	bne +
	lda escpar
	; bright background color
	cmp #100
	bcc colr2
	sbc #92
bg	asl a
	asl a
	asl a
	asl a
	pha
	lda color
	and #$0f
	sta color
	pla
	ora color
	sta color
	rts
colr2	; bright foreground color
	cmp #90
	bcc colr3
	sbc #82
fg	pha
	lda color
	and #$f0
	sta color
	pla
	ora color
	sta color
	rts
colr3	; background color
	cmp #40
	bcc colr4
	sbc #40
	bra bg
colr4	; foreground color
	sec
	sbc #30
	bra fg

+	cmp #'J'
	bne +
	lda escpar
	cmp #2
	bne +
	jmp clear_screen
+	rts
	.endproc

clear_screen .proc
	stz vera.addrl
	lda #$b0
	sta vera.addrm

	ldx #0
	ldy #15
-	lda #$20
	sta vera.data0
	lda color
	sta vera.data0
	dex
	bne -
	dey
	bne -

	rts
	.endproc

; Move cursor to coordinate
; 0-based values
goto_pos .proc
	stx xpos
	txa
	asl a
	sta vera.addrl
	sty ypos
	tya
	clc
	adc #$b0
	sta vera.addrm
	rts
	.endproc

ansi_out .proc
	cmp #$11	; cursor down
	bne +
	lda #'B'
	bra esc
+	cmp #$91	; cursor up
	bne +
	lda #'A'
	bra esc
+	cmp #$1d	; cursor right
	bne +
	lda #'C'
	bra esc
+	cmp #$9d	; cursor left
	bne +
	lda #'D'
	bra esc
+	cmp #$14	; backspace
	bne +
	lda #$7f
	bra out
+	cmp #$0d	; enter
	bne +
	lda #$0a
	bra out
+
out	sta chrout
	stz net.send.socket
	lda #1
	sta net.send.buf_sz
	stz net.send.buf_sz + 1
	lda #<chrout
	sta net.send.buf
	lda #>chrout
	sta net.send.buf + 1
	jsr net.send
	rts

esc	sta escout + 2
	stz net.send.socket
	lda #3
	sta net.send.buf_sz
	stz net.send.buf_sz + 1
	lda #<escout
	sta net.send.buf
	lda #>escout
	sta net.send.buf + 1
	jsr net.send
	rts
	.endproc

	.endsection

	.section data
iscs	.byte ?
isesc	.byte ?
; Escape sequence parameters
escpar	.fill 2, ?
; Number of escape sequence parameters
nescpar	.byte ?

xpos	.byte ?
ypos	.byte ?

color	.byte ?

chrout	.byte ?
escout	.byte $1b, $5b, $00

palette .byte $00,$00	; black		- rgb($0, $0, $0)
	.byte $00,$08	; red		- rgb($8, $0, $0)
	.byte $80,$00	; green		- rgb($0, $8, $0)
	.byte $80,$08	; yellow	- rgb($8, $8, $0)
	.byte $08,$00	; blue		- rgb($0, $0, $8)
	.byte $08,$08	; magenta	- rgb($8, $0, $8)
	.byte $88,$00	; cyan		- rgb($0, $8, $8)
	.byte $bb,$0b	; white		- rgb($b, $b, $b)
	.byte $88,$08	; bright black	- rgb($8, $8, $8)
	.byte $00,$0f	; bright red	- rgb($f, $0, $0)
	.byte $f0,$00	; bright green	- rgb($0, $f, $0)
	.byte $f0,$0f	; bright yellow	- rgb($f, $f, $0)
	.byte $0f,$00	; bright blue	- rgb($0, $0, $f)
	.byte $0f,$0f	; bright magenta- rgb($f, $0, $f)
	.byte $ff,$00	; bright cyan	- rgb($0, $f, $f)
	.byte $ff,$0f	; bright white	- rgb($f, $f, $f)
	.endsection
