; vim: set syntax=64tass
	.cpu "65816"

*	= $0022
	.dsection zp

*	= $0801
	.byte $0b, $08
	.byte $d9, $07
	.byte $9e
	.null "2061"
	.word $0000

	.dsection main
	.dsection code
	.dsection data

	prog8 = 0

vera	.block
	addrl = $9f20
	addrm = $9f21
	addrh = $9f22
	data0 = $9f23
	ctrl = $9f25
	.endblock

kernal	.block
GETIN = $ffe4
CHROUT	= $ffd2
SCREEN_MODE = $ff5f
EXTAPI = $feab
	E_SCNSIZ = $0d
SETNAM = $ffbd
SETLFS = $ffba
LOAD = $ffd5
enter_basic = $ff47
kbdbuf_get_modifiers = $fec0
screen_set_charset = $ff62
	.endblock

x16	.block
ram_bank = $00
	.endblock

strptr = $22

	.section main
main
	lda #1
	sta x16.ram_bank

	lda #size(net_bin_str)
	ldx #<net_bin_str
	ldy #>net_bin_str
	jsr kernal.SETNAM

	lda #0
	ldx #8
	ldy #2
	jsr kernal.SETLFS

	ldx #<$a000
	ldy #>$a000
	lda #0
	jsr kernal.LOAD

	jsr do_80x24

	lda #127
	sta net.open.ip + 0
	stz net.open.ip + 1
	stz net.open.ip + 2
	lda #1
	sta net.open.ip + 3
	lda #23
	sta net.open.socket_arg0
	stz net.open.socket_arg0 + 1

	lda #net.IP_V4
	sta net.open.ip_ty
	stz net.open.socket
	lda #net.SOCK_TCP
	sta net.open.socket_ty
	jsr net.open

-	jsr net.poll
	lda net.poll.socket
	and #net.SOCK_STAT_CONNECTED
	beq -

	jmp ansi_term
	.endsection

	.section code
do_80x24 .proc
	lda #1
        clc
        jsr kernal.SCREEN_MODE
        ldx #80
        ldy #24
        lda #kernal.E_SCNSIZ
        jsr kernal.EXTAPI
        rts
	.endproc
	.endsection

	.section data
net_bin_str .text "NET.BIN"
chrbuf	.fill 50, 0
	.endsection
