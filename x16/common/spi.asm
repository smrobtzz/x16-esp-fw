	VERA_SPI_DATA = $9F3E
        VERA_SPI_CTRL = $9F3F
	VIA1_DDRB = $9F02
	VIA1_ORB = $9F00
        krn_ptr1 = $91
        spi_mutex = $2b3

enable_cs .macro
	; lda #1
	; sta VERA_SPI_CTRL

	; stz VIA1_ORB

	stz VERA_SPI_CTRL
	.endmacro

enable_cs_autotx .macro
	; lda #%00000101
	; sta VERA_SPI_CTRL

	;stz VIA1_ORB
	;lda #%00000100
	;sta VERA_SPI_CTRL

	lda #%00000100
	sta VERA_SPI_CTRL
	.endmacro

disable_autotx .macro
	; lda #%00000001
	; sta VERA_SPI_CTRL

	stz VERA_SPI_CTRL
	.endmacro

disable_cs .macro
	;lda #1
	;sta VIA1_ORB
	;stz VERA_SPI_CTRL

	lda #1
	sta VERA_SPI_CTRL
	.endmacro

wait_busy .macro
	.endmacro

; 3-cycle nop with no side effects
wait_busy_or_nop .macro
	.block
	jmp +
+
	.endblock
	.endmacro

spi_start_request .macro
	; Enable CS
	#enable_cs

-	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	#wait_busy_or_nop
	stz VERA_SPI_DATA

	#wait_busy_or_nop
	ldx VERA_SPI_DATA

	; Toggle CS
	#disable_cs
	nop
	nop
	nop
	#enable_cs

	cpx #1
	bne -

	; CMD = Wr_DMA
	lda #3
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; Ready to send DATA
        .endmacro

spi_end_request .macro
	#disable_cs
	nop
	nop
	nop
	#enable_cs

	; CMD = WR_DONE
	lda #7
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; Disable CS
	#wait_busy_or_nop
	#disable_cs
        .endmacro

spi_start_response .macro
	; Enable CS
	#enable_cs

-	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	#wait_busy_or_nop
	ldx VERA_SPI_DATA
	#disable_cs
	nop
	nop
	nop
	#enable_cs
	cpx #2
	bne -

	; CMD = Rd_DMA
	lda #4
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; Ready to receive DATA
        .endmacro

spi_end_response .macro
	#disable_cs
	nop
	nop
	nop
	#enable_cs

	; CMD = CMD8
	lda #8
	sta VERA_SPI_DATA

	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	#wait_busy_or_nop
	#disable_cs
        .endmacro

spi_send_cmd .macro
	; Garbage padding bytes
        pha
        pha

        ldx #4
        #wait_busy
-       pla
        sta VERA_SPI_DATA
        #wait_busy_or_nop
        dex
        bne -
	.endmacro
