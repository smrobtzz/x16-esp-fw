; vim: set syntax=64tass
; SPDX-License-Identifier: MIT

*	= $0801
	.byte $0b, $08
	.byte $d9, $07
	.byte $9e
	.null "2061"
	.word $0000

	romb = $1
	GETIN = $ffe4
	CHROUT = $ffd2
	key = $02

main	.block
	ldx #0
-	lda msg, x
	beq waitkey
	jsr CHROUT
	inx
	bra -
waitkey	jsr GETIN
	cmp #0
	beq waitkey
	sta key
	ldx #0
-	lda msg2, x
	beq waitkey2
	jsr CHROUT
	inx
	bra -
waitkey2 jsr GETIN
	cmp #0
	beq waitkey2
	cmp key
	beq waitkey2

	sei
	lda #<$c000
	sta erasesect.addr
	lda #>$c000
	sta erasesect.addr + 1
	jsr erasesect

	lda #<$d000
	sta erasesect.addr
	lda #>$d000
	sta erasesect.addr + 1
	jsr erasesect

	lda #<$e000
	sta erasesect.addr
	lda #>$e000
	sta erasesect.addr + 1
	jsr erasesect

	lda #<$f000
	sta erasesect.addr
	lda #>$f000
	sta erasesect.addr + 1
	jsr erasesect

	lda #<$c000
	sta writebyte.addr
	lda #>$c000
	sta writebyte.addr + 1

	ldx #0
	ldy #0
ldabyte	lda driver
	jsr writebyte
	inx
	bne next
	inc ldabyte + 2
	inc writebyte.addr + 1
	iny
	cpy #64
	beq done
next	inc ldabyte + 1
	inc writebyte.addr
	bra ldabyte

done	cli
	lda #4
	sta romb
	ldx #0
-	lda msg3, x
	beq ret
	jsr CHROUT
	inx
	bra -
ret	rts
.endblock

erasesect .proc
addr = essta_addr + 1
	lda #1
	sta romb
	lda #$aa
	sta $c000 + $5555 - $4000

	stz romb
	lda #$55
	sta $c000 + $2aaa

	lda #1
	sta romb
	lda #$80
	sta $c000 + $5555 - $4000

	lda #1
	sta romb
	lda #$aa
	sta $c000 + $5555 - $4000

	stz romb
	lda #$55
	sta $c000 + $2aaa

	lda #31
	sta romb
	lda #$30
essta_addr
	sta @w $0000
	jmp wait25ms
	.endproc

writebyte .proc
addr = wbsta_addr + 1
	pha
	lda #1
	sta romb
	lda #$aa
	sta $c000 + $5555 - $4000

	stz romb
	lda #$55
	sta $c000 + $2aaa

	lda #1
	sta romb
	lda #$a0
	sta $c000 + $5555 - $4000

	lda #31
	sta romb
	pla
wbsta_addr
	sta @w $0000
	jmp wait20us
	.endproc

; 80 nops * 2 cycles = 160 cycles @ 8MHz = 20us
wait20us .proc
	.rept 80
	nop
	.endrept
	rts
	.endproc

; 400 nops * 2 cycles * 250 = 200000 cycles @ 8MHz = 25ms
wait25ms .proc
	phx
	ldx #250
-	.rept 400
	nop
	.endrept
	dex
	beq +
	jmp -
+	plx
	rts
	.endproc

msg
	.text $93, "THIS PROGRAM WILL INSTALL THE CALYPSO", $0d
	.text "DRIVER INTO ROM BANK 31", $0d
	.text "", $0d
	.null "PRESS ANY KEY TO CONTINUE...", $0d

msg2
	.null "PRESS A DIFFERENT KEY TO CONTINUE...", $0d

msg3
	.text "", $0d
	.null "DONE", $0d

.include "driver.asm"
