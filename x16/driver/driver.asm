; vim: set syntax=64tass
; SPDX-License-Identifier: MIT

	.align $4000
driver	.block
	.logical $c000
	.dsection hdr
        .dsection code
	.fill $ffd2 - *, ?
	.dsection jmptab
	.endlogical

	.section hdr
	.text "DOSXT"
	.text "CALYPSO  "
	.endsection

        DOS_BANK = 2
        ieee_status = $0287
        ram_bank = 0
        rom_bank = 1
	bank_save = $93
	mousey = $a888
	mousex = $a886

        .section code
spi_init:
	pla
	#disable_cs
	stz $af00
	lda #1
	sta spi_mutex

	; Enable CS
	#enable_cs

	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA

	; ADDR = 9
	#wait_busy_or_nop
	lda #9
	sta VERA_SPI_DATA

	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA

	; DATA
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	#disable_cs
	lda VERA_SPI_DATA
	stz spi_mutex
	cmp #$42
	beq +
	; Magic number $42 not correct
	; 1 = Device not available
	lda #1
	pha
	jmp ext_ret
	; 0 = Device available
+	lda #0
	pha

	lda #3
	sta $af00
	ldx #af06_data_len
	ldy #0
-	lda af06_data, y
	sta $af06, y
	iny
	dex
	bne -

	jmp ext_ret

af06_data:
	.logical $af06
	jmp af06_get_keyboard
	jmp af06_get_mouse
	jmp af06_set_leds
af06_get_keyboard:
	lda rom_bank
	pha
	lda #31
	sta rom_bank
	php
	sei
	lda spi_mutex
	bne af06_nomutex
	lda #$01
	sta spi_mutex
	plp
	jsr spi_get_keyboard
	; $afff = 1 -> reset X16
	lda $afff
	cmp #1
	bne af06_done
	sec
	.byte $fb	; xce
	stz rom_bank
	jmp ($fffc)
	bra af06_done
af06_get_mouse:
	lda rom_bank
	pha
	lda #31
	sta rom_bank
	php
	sei
	lda spi_mutex
	bne af06_nomutex
	lda #$01
	sta spi_mutex
	plp
	jsr spi_get_mouse
	bra af06_done
af06_set_leds:
	; X = keyboard LEDs state
	lda rom_bank
	pha
	lda #31
	sta rom_bank
	php
	sei
	lda spi_mutex
	bne af06_nomutex
	lda #$01
	sta spi_mutex
	plp
	jsr spi_set_leds
	bra af06_done
af06_nomutex:
	stz $af05
	plp
	pla
	sta rom_bank
	rts
af06_done:
	stz spi_mutex
	pla
	sta rom_bank
	rts
	.endlogical
af06_data_len = * - af06_data

BANKING_START .macro
	lda ram_bank
	sta bank_save
	stz ram_bank
.endmacro

BANKING_END .macro
	pha
	lda bank_save
	sta ram_bank
	pla
.endmacro

spi_secnd:
	#BANKING_START
	lda #1
	sta spi_mutex
	lda #4
	pha
	jmp spi_txrx

spi_tksa:
	#BANKING_START
	lda #1
	sta spi_mutex
	lda #1
	pha
	jmp spi_txrx

spi_acptr:
	#BANKING_START
	lda #1
	sta spi_mutex
        pla
	lda #0
	pha
	lda #7
	pha
	jmp spi_txrx

spi_ciout:
	#BANKING_START
	lda #1
	sta spi_mutex
	lda #6
	pha
	jmp spi_txrx

spi_untlk:
	#BANKING_START
	lda #1
	sta spi_mutex
        pla
	lda #0
	pha
	lda #2
	pha
	jmp spi_txrx

spi_unlsn:
	#BANKING_START
	lda #1
	sta spi_mutex
        pla
	lda #0
	pha
	lda #5
	pha
	jmp spi_txrx

spi_listn:
	#BANKING_START
	lda #1
	sta spi_mutex
	lda #3
	pha
	jmp spi_txrx

spi_talk:
	BANKING_START
	lda #1
	sta spi_mutex
	lda #0
	pha
	jmp spi_txrx

spi_txrx:
	#spi_start_request

	; Garbage padding bytes
	pha
	pha

	; Send 4 byte request
	ldx #4
	#wait_busy
-	pla
	sta VERA_SPI_DATA
	#wait_busy_or_nop
	dex
	bne -

	#spi_end_request
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	#spi_start_response

	#wait_busy_or_nop
	; Receive 2 byte response
	ldx #2
	#wait_busy
-	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	pha
	dex
	bne -

	#spi_end_response

	clc
	pla
	sta ieee_status
	beq +
	sec
+	stz spi_mutex
	#BANKING_END
	jmp ext_ret

spi_macptr: .block
	#BANKING_START
	lda #1
	sta spi_mutex
	#BANKING_END
        pla
        ply
	sty krn_ptr1+1
        plx
	stx krn_ptr1
        pha
	php
	pha

	lda #9
	pha

	#spi_start_request

	; Garbage padding bytes
	pha
	pha

	; Send 4 byte request
	ldx #4
	#wait_busy
-	pla
	sta VERA_SPI_DATA
	#wait_busy_or_nop
	dex
	bne -

	#spi_end_request
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	#spi_start_response

	; Enable auto-tx
	#enable_cs_autotx

	; Start auto-tx
	ldx VERA_SPI_DATA

	plp
	bcs noinc
	cpy #$BF
	bne nowrap

	; Receive 'A'-byte response (will wrap on bank)
	plx
-	#wait_busy
	lda VERA_SPI_DATA
	sta (krn_ptr1)
	inc krn_ptr1
	bne ++
	stz krn_ptr1
	lda krn_ptr1+1
	cmp #$BF
	bne +
	lda #$A0
	sta krn_ptr1+1
	inc ram_bank
	bra ++
+	ina
	sta krn_ptr1+1
+	dex
	bne -
	bra status

nowrap:
	; Receive 'A'-byte response (will not wrap on bank)
	plx
	ldy #0
-	#wait_busy
	lda VERA_SPI_DATA
	sta (krn_ptr1), y
	iny
	bne +
	inc krn_ptr1+1
+	dex
	bne -
	bra status

noinc:
	; Receive 'A'-byte response (will not increment dest addr)
	plx
-	#wait_busy
	lda VERA_SPI_DATA
	sta (krn_ptr1)
	dex
	bne -

status:
	; Receive length low
	#wait_busy
	ldx VERA_SPI_DATA

	; Receive length high
	#wait_busy_or_nop
	ldy VERA_SPI_DATA

	; Disable auto-tx
	#wait_busy_or_nop
	#disable_autotx

	#wait_busy
	; Receive status
	lda VERA_SPI_DATA
	sta ieee_status

	#spi_end_response

	clc
        phx
        phy
        pha
	#BANKING_START
	stz spi_mutex
	#BANKING_END
        jmp ext_ret
        .endblock

; a - bytes
; x - lo
; y - hi
spi_mciout: .block
	#BANKING_START
	lda #1
	sta spi_mutex
	#BANKING_END
	pla
        ply
        sty krn_ptr1+1
        plx
        stx krn_ptr1
        pha
	php
	pha

	#spi_start_request

	#wait_busy_or_nop
	sta VERA_SPI_DATA	; junk
	#wait_busy_or_nop
	sta VERA_SPI_DATA	; junk
	#wait_busy_or_nop
	lda #8			; MCIOUT
	sta VERA_SPI_DATA
	#wait_busy_or_nop
	plx
	stx VERA_SPI_DATA	; length

	plp
	bcs noinc
	cpy #$BF
	bne nowrap

	; Send 'A'-byte request (will wrap on bank)
-	lda (krn_ptr1)
	sta VERA_SPI_DATA
	inc krn_ptr1
	bne ++
	lda krn_ptr1+1
	cmp #$BF
	bne +
	lda #$A0
	sta krn_ptr1+1
	inc ram_bank
	bra ++
+	ina
	sta krn_ptr1+1
+	dex
	bne -
	bra pad

	; Send 'A'-byte request (will not wrap on bank)
nowrap:
	ldy #0
-	lda (krn_ptr1), y
	sta VERA_SPI_DATA
	iny
	bne +
	inc krn_ptr1+1
+	dex
	bne -
	bra pad

noinc:
	; Send 'A'-byte request (will not increment dest addr)
-	lda (krn_ptr1)
	sta VERA_SPI_DATA
	dex
	bne -

pad:
	; Need to pad to 4 bytes to appease DMA controller
	pla
	and #%00000111
	beq +
-	sta VERA_SPI_DATA
	ina
	and #%00000111
	bne -
+
	#spi_end_request
	#spi_start_response

	; Enable auto-tx
	#enable_cs_autotx
	lda VERA_SPI_DATA

	; Receive length low
	#wait_busy_or_nop
	ldx VERA_SPI_DATA

	; Receive length high
	#wait_busy_or_nop
	ldy VERA_SPI_DATA

	; Disable auto-tx
	#wait_busy_or_nop
	#disable_autotx

	; Receive status
	lda VERA_SPI_DATA
	sta ieee_status

	#spi_end_response

	clc
        phx
        phy
        pha
	#BANKING_START
	stz spi_mutex
	#BANKING_END
        jmp ext_ret
        .endblock

spi_get_keyboard .block
	; Enable CS
	#enable_cs
	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA
	; ADDR = 1
	lda #1
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af01
	beq +
	lda #1
	sta $af05

+	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $afff

	; Disable CS
	#disable_cs
	#wait_busy_or_nop
	; Enable CS
	#enable_cs
	; CMD = CMD9
	lda #9
	sta VERA_SPI_DATA
	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	; Disable CS
	#disable_cs

	lda $afff
	bne spi_wait_reset
	rts
	.endblock

spi_wait_reset .block
	; Enable CS
	#enable_cs
	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA
	; ADDR = 2
	lda #2
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	cmp #2
	beq +
	; Disable CS
	#disable_cs
	bra spi_wait_reset

+	; Disable CS
	#disable_cs
	; Enable CS
	#enable_cs
	; CMD = CMD9
	lda #9
	sta VERA_SPI_DATA
	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	; Disable CS
	#disable_cs

	rts
	.endblock

spi_get_mouse .block
	; Enable CS
	#enable_cs
	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA
	; ADDR = 3
	lda #3
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	; DATA (5 bytes)
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af01

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af02

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af03

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af04

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta $af05

	; Disable CS
	#disable_cs
	nop
	nop
	nop

	; Enable CS
	#enable_cs
	; Read absolute mouse position
	; CMD = Rd_BUF
	lda #2
	sta VERA_SPI_DATA
	; ADDR = 15
	lda #15
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	beq +

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta mousex

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta mousex + 1

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta mousey

	stz VERA_SPI_DATA
	#wait_busy_or_nop
	lda VERA_SPI_DATA
	sta mousey + 1

	; Disable CS
	#disable_cs
	nop
	nop
	nop

	; Enable CS
	#enable_cs
	; Clear "absolute mouse position valid" byte
	; CMD = Wr_BUF
	lda #1
	sta VERA_SPI_DATA
	; ADDR = 15
	lda #15
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop

	; Disable CS
+	#disable_cs
	nop
	nop
	nop
	; Enable CS
	#enable_cs
	; CMD = CMDA
	lda #$A
	sta VERA_SPI_DATA
	; ADDR = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	#wait_busy_or_nop
	; Disable CS
	#disable_cs
	rts
	.endblock

spi_set_leds .block
	; Enable CS
	#enable_cs
	; CMD = Wr_BUF
	lda #1
	sta VERA_SPI_DATA
	; ADDR = 14
	#wait_busy_or_nop
	lda #14
	sta VERA_SPI_DATA
	; DUMMY = 0
	#wait_busy_or_nop
	stz VERA_SPI_DATA
	; DATA (1 byte)
	#wait_busy_or_nop
	stx VERA_SPI_DATA
	#wait_busy_or_nop
	; Disable CS
	#disable_cs
	rts
	.endblock
        .endsection

        .section jmptab

ext_jmptab:
        .fill 3
	jmp spi_secnd
	jmp spi_tksa
	jmp spi_acptr
	jmp spi_ciout
	jmp spi_untlk
	jmp spi_unlsn
	jmp spi_listn
	jmp spi_talk
	jmp spi_init
	jmp spi_macptr
	jmp spi_mciout
ext_ret:
	lda #DOS_BANK
	sta rom_bank
	.fill 6, 0

        .endsection
	.endblock
