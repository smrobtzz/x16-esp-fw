// SPDX-License-Identifier: GPL-3.0-only
#include <driver/sdmmc_host.h>
#include <driver/sdspi_host.h>
#include <driver/spi_common.h>
#include <esp_log.h>
#include <sdmmc_cmd.h>

#include <diskio_impl.h>
#include <esp_vfs_fat.h>
#include <ff.h>
#include <string.h>

#include "defs.h"
#include "disk.h"

esp_err_t sd_disk_read(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);
esp_err_t sd_disk_write(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);

static sdspi_dev_handle_t s_sdspi_handle;
static sdmmc_card_t       s_sdmmc_card;
char fat_volume_label[12] = "";

void sd_init()
{
    sdspi_device_config_t dev_config;
    spi_bus_config_t      bus_config;
    sdmmc_host_t          mmc_config = SDSPI_HOST_DEFAULT();
    struct disk          *disk;
    esp_err_t             res;
    int                   retries = 3;

    sdspi_host_init();

    bus_config = (spi_bus_config_t) {
        .mosi_io_num = CONFIG_X_GPIO_SD_MOSI,
        .miso_io_num = CONFIG_X_GPIO_SD_MISO,
        .sclk_io_num = CONFIG_X_GPIO_SD_SCLK,
        .data2_io_num = -1,
        .data3_io_num = -1,
        .data4_io_num = -1,
        .data5_io_num = -1,
        .data6_io_num = -1,
        .data7_io_num = -1,
        .max_transfer_sz = 0,
        .flags = 0,
        .isr_cpu_id = 1,
    };
    res = spi_bus_initialize(SPI3_HOST, &bus_config, SPI_DMA_CH_AUTO);
    if (res != ESP_OK)
        goto fail1;

    dev_config = (sdspi_device_config_t) {
        .host_id = SPI3_HOST,
        .gpio_cs = CONFIG_X_GPIO_SD_CS,
        .gpio_cd = SDSPI_SLOT_NO_CD,
        .gpio_wp = SDSPI_SLOT_NO_WP,
        .gpio_int = SDSPI_SLOT_NO_INT,
    };
    res = sdspi_host_init_device(&dev_config, &s_sdspi_handle);
    if (res != ESP_OK)
        goto fail2;

    mmc_config.slot = s_sdspi_handle;
    do {
        res = sdmmc_card_init(&mmc_config, &s_sdmmc_card);
        retries--;
    } while (res != ESP_OK && retries > 0);
    if (res != ESP_OK)
        goto fail3;

    disk = malloc(sizeof(struct disk));
    if (disk == NULL) {
        res = ESP_ERR_NO_MEM;
        goto fail3;
    }
    disk->name = "sd0";
    disk->sector_size = s_sdmmc_card.csd.sector_size;
    disk->size = disk->sector_size * s_sdmmc_card.csd.capacity;
    disk->read = sd_disk_read;
    disk->write = sd_disk_write;
    if ((res = disk_register(disk)) != ESP_OK)
        goto fail4;

    return;
fail4:
    free(disk);
fail3:
    sdspi_host_remove_device(s_sdspi_handle);
fail2:
    spi_bus_free(SPI3_HOST);
fail1:
    sdspi_host_deinit();
    LOG(sd, "failed to init: %s", esp_err_to_name(res));
}

esp_err_t sd_disk_read(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count)
{
    return sdmmc_read_sectors(&s_sdmmc_card, buf, sector, count);
}

esp_err_t sd_disk_write(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count)
{
    return sdmmc_write_sectors(&s_sdmmc_card, buf, sector, count);
}

/*
void sd_unmount()
{
    if (s_sd_mounted) {
        f_mount(NULL, "", 1);
        ff_diskio_register(0, NULL);
        ESP_ERROR_CHECK_WITHOUT_ABORT(esp_vfs_fat_unregister_path("/sd"));
        ESP_ERROR_CHECK_WITHOUT_ABORT(sdspi_host_remove_device(s_sdspi_handle));
        ESP_ERROR_CHECK_WITHOUT_ABORT(sdspi_host_deinit());
        ESP_ERROR_CHECK_WITHOUT_ABORT(spi_bus_free(SPI3_HOST));
        s_sd_mounted = false;
    }

    gpio_config_t gpio_cfg = {
        .pin_bit_mask = ((uint64_t)1 << CONFIG_X_GPIO_SD_MOSI) | ((uint64_t)1 << CONFIG_X_GPIO_SD_MISO)
            | ((uint64_t)1 << CONFIG_X_GPIO_SD_SCLK) | ((uint64_t)1 << CONFIG_X_GPIO_SD_CS),
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE,
    };
    ESP_ERROR_CHECK(gpio_config(&gpio_cfg));

    LOG_IF(sd, "unmounted");
}*/
