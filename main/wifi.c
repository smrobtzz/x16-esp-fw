// SPDX-License-Identifier: GPL-3.0-only
#include <stddef.h>

#include <esp_event.h>
#include <esp_log.h>
#include <esp_wifi.h>

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <string.h>

#include "defs.h"
#include "wifi.h"

static int s_retry_num = 0;

static void event_handler(void *arg, esp_event_base_t event_base,
    int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        LOG_IF(wifi, "wifi starting");
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED) {
        xTaskNotifyIndexed(g_config_task, CONFIG_MAILBOX, CONFIG_MSG_WIFI_SUCC, eSetValueWithOverwrite);
        g_config.cur_ap = 0;
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        g_config.cur_ap = 0xFF;
        if (!g_wifi_started) {

        } else if (s_retry_num < 1) {
            esp_wifi_connect();
            s_retry_num++;
        } else {
            LOG(wifi, "failed to connect");
            xTaskNotifyIndexed(g_config_task, CONFIG_MAILBOX, CONFIG_MSG_WIFI_FAIL, eSetValueWithOverwrite);
        }
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        LOG_IF(wifi, "connected, got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        memcpy(&g_config.ip, &event->ip_info.ip, sizeof(esp_ip4_addr_t));
    }
}

void wifi_init_sta(void)
{
    int                res;
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    res = esp_netif_init();
    if (res != ESP_OK)
        goto fail;

    res = esp_event_loop_create_default();
    if (res != ESP_OK)
        goto fail;

    esp_netif_create_default_wifi_sta();

    res = esp_wifi_init(&cfg);
    if (res != ESP_OK)
        goto fail;

    res = esp_event_handler_instance_register(WIFI_EVENT,
        ESP_EVENT_ANY_ID,
        &event_handler,
        NULL,
        NULL);
    if (res != ESP_OK)
        goto fail;

    res = esp_event_handler_instance_register(IP_EVENT,
        IP_EVENT_STA_GOT_IP,
        &event_handler,
        NULL,
        NULL);
    if (res != ESP_OK)
        goto fail;

    return;
fail:
    LOG(wifi, "failed to initialize: %s", esp_err_to_name(res));
}

void wifi_configure(char *ssid, char *password)
{
    int           res;
    size_t        len;
    wifi_config_t wifi_config = {
        .sta = {
            .threshold.authmode = ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD,
        },
    };

    len = strlen(ssid);
    if (len > 31) {
        LOG(wifi, "SSID exceeds maximum length (31)");
        return;
    }
    memcpy(wifi_config.sta.ssid, ssid, len);
    wifi_config.sta.ssid[len] = 0;

    len = strlen(password);
    if (len > 63) {
        LOG(wifi, "password exceeds maximum length (63)");
        return;
    }
    memcpy(wifi_config.sta.password, password, len);
    wifi_config.sta.password[len] = 0;

    if (g_wifi_started) {
        g_wifi_started = false;
        esp_wifi_stop();
    }

    LOG_IF(wifi, "configuring with SSID: \"%s\", password: \"%s\"", ssid, password);
    res = esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
    if (res != ESP_OK)
        goto fail;

    res = esp_wifi_set_ps(WIFI_PS_NONE);
    if (res != ESP_OK)
        goto fail;

    s_retry_num = 0;
    res = esp_wifi_start();
    if (res != ESP_OK)
        goto fail;

    g_wifi_started = true;
    return;

fail:
    ESP_LOGE("wifi", "failed to configure: %s", esp_err_to_name(res));
}
