// SPDX-License-Identifier: GPL-3.0-only
// Parts taken from ESP-IDF
/*
 * SPDX-FileCopyrightText: 2023-2024 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <driver/gpio.h>
#include <driver/i2c_slave.h>
#include <esp_log.h>
#include <esp_pm.h>
#include <soc/i2c_periph.h>
#include <soc/i2c_struct.h>
#include <soc/system_struct.h>

#include <hal/gpio_hal.h>
#include <hal/i2c_hal.h>

#include <esp_rom_gpio.h>

#include "defs.h"

#define I2C_PORT            0
#define I2C_INTR_ALLOC_FLAG (ESP_INTR_FLAG_SHARED | ESP_INTR_FLAG_IRAM | ESP_INTR_FLAG_LOWMED)

// In ESP-IDF
void      periph_rcc_enter(void);
void      periph_rcc_exit(void);
esp_err_t gpio_func_sel(gpio_num_t gpio_num, uint32_t func);
//

static void i2c_set_pins();

static DRAM_ATTR uint8_t s_i2c_cmd = 0xFF;

static IRAM_ATTR void i2c_isr(void *arg)
{
    i2c_dev_t *dev = (i2c_dev_t *)arg;
    uint32_t   int_mask, cnt;

    i2c_ll_get_intr_mask(dev, &int_mask);
    i2c_ll_clear_intr_mask(dev, int_mask);

    if (int_mask == I2C_RXFIFO_WM_INT_ENA_M) {
        i2c_ll_get_rxfifo_cnt(dev, &cnt);
        if (cnt == 0) {
            return;
        }

        uint8_t byte = 0;
        i2c_ll_read_rxfifo(dev, &byte, 1);

        if (s_i2c_cmd != 0xFF) {
            switch (s_i2c_cmd) {
#if CONFIG_X_USB_HOST_ENABLE
            case 0x34:
                xQueueSendFromISR(g_kb_light_queue, &byte, NULL);
                break;
#endif
#if CONFIG_X_DOS_ENABLE
            case 0x7f:
                if (byte == 0x00) {
                    DRAM_LOG_IF(i2c, "starting dos");
                    xTaskNotifyIndexedFromISR(
                        g_dos_task,
                        DOS_MAILBOX,
                        DOS_MSG_START,
                        eSetValueWithOverwrite,
                        NULL);
                } else if (byte == 0x01) {
                    DRAM_LOG_IF(i2c, "stopping dos");
                    xTaskNotifyIndexedFromISR(
                        g_dos_task,
                        DOS_MAILBOX,
                        DOS_MSG_STOP_I2C,
                        eSetValueWithOverwrite,
                        NULL);
                }
                break;
#endif
            }
            s_i2c_cmd = 0xFF;
        } else {
            s_i2c_cmd = byte;
        }
    } else {
        uint8_t byte = 0;
        i2c_ll_read_rxfifo(dev, &byte, 1);

        switch (byte) {
        case 0x30:
            byte = 12;
            i2c_ll_write_txfifo(dev, &byte, 1);
            break;
        case 0x31:
            byte = 34;
            i2c_ll_write_txfifo(dev, &byte, 1);
            break;
        case 0x32:
            byte = 56;
            i2c_ll_write_txfifo(dev, &byte, 1);
            break;
#if CONFIG_X_USB_HOST_ENABLE
        case 0x33:
            byte = 0;
            xQueueReceiveFromISR(g_key_queue, &byte, NULL);
            i2c_ll_write_txfifo(dev, &byte, 1);
            break;
        case 0x35: {
            struct mouse_event *evt = g_mouse_evt + g_cur_mouse_evt;
            if (g_cur_mouse_evt == 0) {
                g_mouse_evt[1].bt = evt->bt;
                g_cur_mouse_evt = 1;
            } else {
                g_mouse_evt[0].bt = evt->bt;
                g_cur_mouse_evt = 0;
            }

            i2c_ll_write_txfifo(dev, (uint8_t *)evt, sizeof(struct mouse_event));

            evt->x = 0;
            evt->y = 0;
            evt->w = 0;
        } break;
#else
        case 0x33:
            byte = 0;
            i2c_ll_write_txfifo(dev, &byte, 1);
            break;
        case 0x35: {
            byte = 0;
            i2c_ll_write_txfifo(dev, &byte, 1);
            i2c_ll_write_txfifo(dev, &byte, 1);
            i2c_ll_write_txfifo(dev, &byte, 1);
            i2c_ll_write_txfifo(dev, &byte, 1);
            i2c_ll_write_txfifo(dev, &byte, 1);
            i2c_ll_write_txfifo(dev, &byte, 1);
        } break;
#endif
        }

        i2c_ll_slave_clear_stretch(dev);
    }
}

void i2c_task(void *arg)
{
    i2c_dev_t *dev = I2C_LL_GET_HW(I2C_PORT);
    {
        periph_rcc_enter();
        if (I2C_PORT == 0) {
            SYSTEM.perip_clk_en0.i2c_ext0_clk_en = 1;
        } else if (I2C_PORT == 1) {
            SYSTEM.perip_clk_en0.i2c_ext1_clk_en = 1;
        }
        if (I2C_PORT == 0) {
            SYSTEM.perip_rst_en0.i2c_ext0_rst = 1;
            SYSTEM.perip_rst_en0.i2c_ext0_rst = 0;
        } else if (I2C_PORT == 1) {
            SYSTEM.perip_rst_en0.i2c_ext1_rst = 1;
            SYSTEM.perip_rst_en0.i2c_ext1_rst = 0;
        }
        periph_rcc_exit();
    }
    i2c_ll_enable_controller_clock(dev, true);
    i2c_set_pins();

    ESP_ERROR_CHECK(esp_intr_alloc_intrstatus(
        i2c_periph_signal[I2C_PORT].irq,
        I2C_INTR_ALLOC_FLAG,
        (uint32_t)i2c_ll_get_interrupt_status_reg(dev),
        I2C_RXFIFO_WM_INT_ENA_M | I2C_SLAVE_STRETCH_INT_ENA_M,
        i2c_isr,
        dev,
        NULL));

    i2c_ll_slave_init(dev);
    i2c_ll_set_data_mode(dev, I2C_DATA_MODE_MSB_FIRST, I2C_DATA_MODE_MSB_FIRST);
    i2c_ll_txfifo_rst(dev);
    i2c_ll_rxfifo_rst(dev);
    i2c_ll_set_source_clk(dev, I2C_CLK_SRC_DEFAULT);
    i2c_ll_set_slave_addr(dev, 0x11, false);
    i2c_ll_set_txfifo_empty_thr(dev, 0);
    i2c_ll_set_rxfifo_full_thr(dev, 2);
    i2c_ll_set_sda_timing(dev, 10, 0);
    i2c_ll_set_tout(dev, 32000);
    i2c_ll_slave_enable_scl_stretch(dev, true);
    i2c_ll_slave_tx_auto_start_en(dev, true);
    dev->ctr.sda_force_out = 0;
    dev->ctr.scl_force_out = 0;
    dev->scl_stretch_conf.stretch_protect_num = 0x3FF;
    // dev->scl_stretch_conf.slave_byte_ack_ctl_en = 1;
    // dev->scl_stretch_conf.slave_byte_ack_lvl = 0;
    i2c_ll_update(dev);
    i2c_ll_enable_intr_mask(dev, I2C_RXFIFO_WM_INT_ENA_M | I2C_SLAVE_STRETCH_INT_ENA_M);

    while (true) {
        vTaskDelay(10);
    }
}

static void i2c_set_pins()
{
    gpio_config_t sda_conf, scl_conf;

    // SDA pin configurations
    sda_conf = (gpio_config_t) {
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_INPUT_OUTPUT_OD,
        .pull_down_en = false,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pin_bit_mask = 1ULL << CONFIG_X_GPIO_I2C_SDA,
    };
    gpio_set_level(CONFIG_X_GPIO_I2C_SDA, 1);
    gpio_config(&sda_conf);
    gpio_hal_iomux_func_sel(GPIO_PIN_MUX_REG[CONFIG_X_GPIO_I2C_SDA], PIN_FUNC_GPIO);
    esp_rom_gpio_connect_out_signal(CONFIG_X_GPIO_I2C_SDA, i2c_periph_signal[I2C_PORT].sda_out_sig, 0, 0);
    esp_rom_gpio_connect_in_signal(CONFIG_X_GPIO_I2C_SDA, i2c_periph_signal[I2C_PORT].sda_in_sig, 0);

    // SCL pin configurations
    scl_conf = (gpio_config_t) {
        .intr_type = GPIO_INTR_DISABLE,
        .mode = GPIO_MODE_INPUT_OUTPUT_OD,
        .pull_down_en = false,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pin_bit_mask = 1ULL << CONFIG_X_GPIO_I2C_SCL,
    };
    gpio_set_level(CONFIG_X_GPIO_I2C_SCL, 1);
    gpio_config(&scl_conf);
    gpio_hal_iomux_func_sel(GPIO_PIN_MUX_REG[CONFIG_X_GPIO_I2C_SCL], PIN_FUNC_GPIO);
    esp_rom_gpio_connect_out_signal(CONFIG_X_GPIO_I2C_SCL, i2c_periph_signal[I2C_PORT].scl_out_sig, 0, 0);
    esp_rom_gpio_connect_in_signal(CONFIG_X_GPIO_I2C_SCL, i2c_periph_signal[I2C_PORT].scl_in_sig, 0);
}
