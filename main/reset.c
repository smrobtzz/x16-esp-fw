// SPDX-License-Identifier: GPL-3.0-only

#include <freertos/FreeRTOS.h>

#include <driver/gpio.h>
#include <esp_log.h>

#include "config.h"
#include "defs.h"
#include "sd.h"
#include "spi.h"
#include "wifi.h"

void do_reset();
void reset_loop();
void default_cfg();
void reload_cfg();

static bool DRAM_ATTR s_reset_done = false;
static bool DRAM_ATTR s_cdone = false;
static bool DRAM_ATTR s_not_started = false;

static TimerHandle_t s_startup_timer;

static void startup_timer_callback(TimerHandle_t timer)
{
    if (!s_cdone) {
        s_reset_done = false;
        s_not_started = true;
    }
}

static void IRAM_ATTR cdone_isr(void *arg)
{
    if (s_reset_done && !s_cdone) {
        gpio_set_level(CONFIG_X_GPIO_CDONE, 0);
#if CONFIG_X_I2C_ENABLE
        gpio_set_level(CONFIG_X_GPIO_SD_EN, 1);
#else
        xTaskNotifyIndexedFromISR(g_dos_task, DOS_MAILBOX, DOS_MSG_START, eSetValueWithOverwrite, NULL);
#endif
        s_cdone = true;
    }
}

static void IRAM_ATTR reset_isr(void *arg)
{
    if (gpio_get_level(CONFIG_X_GPIO_RESET) == 0 && s_reset_done) {
        gpio_set_level(CONFIG_X_GPIO_CDONE, 1);
        s_cdone = false;
        s_reset_done = false;
        s_not_started = false;
        BaseType_t higher_priority_task_woken = pdFALSE;
        xTaskNotifyIndexedFromISR(
            g_reset_task,
            RESET_MAILBOX,
            RESET_MSG_RESET,
            eSetValueWithoutOverwrite,
            &higher_priority_task_woken);
        portYIELD_FROM_ISR(higher_priority_task_woken);
    } else if (gpio_get_level(CONFIG_X_GPIO_RESET) == 1 && s_not_started) {
        gpio_set_level(CONFIG_X_GPIO_CDONE, 1);
        s_cdone = false;
        s_reset_done = false;
        s_not_started = false;
        BaseType_t higher_priority_task_woken = pdFALSE;
        xTaskNotifyIndexedFromISR(
            g_reset_task,
            RESET_MAILBOX,
            RESET_MSG_RESET,
            eSetValueWithoutOverwrite,
            &higher_priority_task_woken);
        portYIELD_FROM_ISR(higher_priority_task_woken);
    }
}

void reset_task(void *arg)
{
    s_startup_timer = xTimerCreate("", 100, pdFALSE, NULL, startup_timer_callback);
    gpio_set_intr_type(CONFIG_X_GPIO_RESET, GPIO_INTR_ANYEDGE);
    gpio_isr_handler_add(CONFIG_X_GPIO_RESET, reset_isr, NULL);

    gpio_set_intr_type(CONFIG_X_GPIO_CDONE, GPIO_INTR_POSEDGE);
    gpio_isr_handler_add(CONFIG_X_GPIO_CDONE, cdone_isr, NULL);
    gpio_intr_enable(CONFIG_X_GPIO_CDONE);

    spi_start();
    do_reset();
    reset_loop();
}

void reset_loop()
{
    bool stopped = false;
    while (true) {
        uint32_t msg = ulTaskNotifyTakeIndexed(RESET_MAILBOX, pdTRUE, portMAX_DELAY);
        switch (msg) {
        case RESET_MSG_START:
            spi_start();
            stopped = false;
            break;
#if CONFIG_X_VERA_FLASH_ENABLE
        case RESET_MSG_STOP_FLASH:
            spi_end();
            xTaskNotifyIndexed(g_flash_task, FLASH_MAILBOX, FLASH_MSG_ACK, eSetValueWithOverwrite);
            stopped = true;
            continue;
#endif
        case RESET_MSG_RESET:
            if (!stopped) {
                spi_end();
                spi_start();
                break;
            }
        default:
            continue;
        }

        do_reset();
    }
}

void do_reset()
{
    gpio_set_level(CONFIG_X_GPIO_RESET, 0);

    LOG_IF(reset, "resetting");
    s_cdone = false;
#if CONFIG_X_DOS_ENABLE
    while (g_dos_task == NULL)
        vTaskDelay(1);
    xTaskNotifyIndexed(g_dos_task, DOS_MAILBOX, DOS_MSG_STOP_RESET, eSetValueWithOverwrite);
    assert(ulTaskNotifyTakeIndexed(RESET_MAILBOX, pdTRUE, portMAX_DELAY) == RESET_MSG_ACK);
#endif

    gpio_set_level(CONFIG_X_GPIO_SD_EN, 0);

    xTimerStart(s_startup_timer, portMAX_DELAY);
    s_reset_done = true;
    gpio_set_level(CONFIG_X_GPIO_RESET, 1);
}
