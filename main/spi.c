// SPDX-License-Identifier: GPL-3.0-only
#include <driver/gpio.h>
#include <driver/gpio_filter.h>
#include <driver/gptimer.h>
#include <driver/spi_slave.h>
#include <driver/spi_slave_hd.h>
#include <esp_log.h>
#include <rom/ets_sys.h>

#include <hal/spi_hal.h>
#include <soc/spi_periph.h>
#include <soc/spi_struct.h>

#include "defs.h"
#include "hid.h"
#include "net.h"

#define SPI_STATUS_REQUEST  1
#define SPI_STATUS_RESPONSE 2

// SPI HD Shared buffer usage:
// 0      - Ready for request = 1, ready for response = 2
// 1      - Keyboard key
// 2      - Wait = 1, Reset = 2
// 3-8    - Mouse data
// 9      - Always $42, used for detecting if Calypso is present
// 10-13  - Socket statuses
// 14     - Keyboard LEDS

// cmd byte + padding      [               4 bytes]
// net_send args + padding [               4 bytes]
// max net_send size       [NET_MAX_BUF_SIZE bytes]
#define SPIBUF_SIZE 8 + NET_MAX_BUF_SIZE

static gpio_glitch_filter_handle_t s_glitch;
spi_slave_hd_data_t                s_xfer;
DMA_ATTR uint8_t                   s_spibuf[SPIBUF_SIZE] __attribute__((aligned(4)));

static bool IRAM_ATTR cb_sent(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    xTaskNotifyIndexedFromISR(
        g_dos_task,
        DOS_MAILBOX,
        DOS_MSG_SPI,
        eSetValueWithoutOverwrite,
        awoken);
    return false;
}

static bool IRAM_ATTR cb_recv(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    xTaskNotifyIndexedFromISR(
        g_dos_task,
        DOS_MAILBOX,
        DOS_MSG_SPI,
        eSetValueWithoutOverwrite,
        awoken);
    return false;
}

static bool IRAM_ATTR cb_send_dma_ready(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    uint8_t buf = SPI_STATUS_RESPONSE;
    spi_slave_hd_write_buffer(SPI2_HOST, 0, &buf, 1);
    return true;
}

static bool IRAM_ATTR cb_recv_dma_ready(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    uint8_t buf = SPI_STATUS_REQUEST;
    spi_slave_hd_write_buffer(SPI2_HOST, 0, &buf, 1);
    return true;
}

static bool IRAM_ATTR cb_cmd9(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    static uint8_t buf[2] = { 0, 0 };
    if (buf[1] == 1) {
        xTaskNotifyIndexedFromISR(
            g_flash_task,
            FLASH_MAILBOX,
            FLASH_MSG_CONT_SOFT_RESET,
            eSetValueWithOverwrite,
            awoken);
        g_reset_byte = 0;
    } else if (g_reset_byte == 2) {
        g_reset_byte = 0;
    }

    // Load next key and reset byte
    buf[0] = 0;
    buf[1] = g_reset_byte;
    xQueueReceiveFromISR(g_key_queue, &buf, NULL);
    spi_slave_hd_write_buffer(SPI2_HOST, 1, buf, 2);

    return true;
}

static bool IRAM_ATTR cb_cmdA(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    struct mouse_event *evt = g_mouse_evt + g_cur_mouse_evt;
    uint8_t             ps2_mouse_packet[5] = { 0 };

    ps2_mouse_packet[0] |= evt->bt | ((evt->dx >> 4) & 0b00010000)
        | ((evt->dy >> 3) & 0b00100000) | 0b00001000;
    ps2_mouse_packet[1] = (uint8_t)evt->dx;
    ps2_mouse_packet[2] = (uint8_t)evt->dy;
    ps2_mouse_packet[3] = (uint8_t)evt->dw;

    evt->dx = 0;
    evt->dy = 0;
    evt->dw = 0;

    if (evt->changed) {
        ps2_mouse_packet[4] = 4;
        evt->changed = false;
    } else {
        ps2_mouse_packet[4] = 0;
    }

    spi_slave_hd_write_buffer(SPI2_HOST, 3, ps2_mouse_packet, 5);

    return true;
}

static bool IRAM_ATTR cb_buffer_rx(void *arg, spi_slave_hd_event_t *event, BaseType_t *awoken)
{
    static uint8_t last_lights = 0;
    uint8_t        lights;

    spi_slave_hd_read_buffer(SPI2_HOST, 14, &lights, 1);

    if (last_lights != lights) {
        struct hid_event hid_event = {
            .event = HID_EVENT_SET_LIGHTS,
            .arg = (void *)(uint64_t)lights
        };
        last_lights = lights;
        xQueueSendFromISR(g_hid_event_queue, &hid_event, NULL);
    }

    return true;
}

void spi_start()
{
    spi_bus_config_t buscfg = {
        .mosi_io_num = CONFIG_X_GPIO_FPGA_MOSI,
        .miso_io_num = CONFIG_X_GPIO_FPGA_MISO,
        .sclk_io_num = CONFIG_X_GPIO_FPGA_SCLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .isr_cpu_id = ESP_INTR_CPU_AFFINITY_0,
        .max_transfer_sz = SPIBUF_SIZE,
        .flags = SPICOMMON_BUSFLAG_GPIO_PINS
    };
    spi_slave_hd_slot_config_t slvcfg = {
        .mode = 0,
        .spics_io_num = CONFIG_X_GPIO_FPGA_CS,
        .flags = 0,
        .command_bits = 8,
        .address_bits = 8,
        .dummy_bits = 8,
        .queue_size = 1,
        .dma_chan = SPI_DMA_CH_AUTO,
        .cb_config = {
            .cb_sent = cb_sent,
            .cb_recv = cb_recv,
            .cb_send_dma_ready = cb_send_dma_ready,
            .cb_recv_dma_ready = cb_recv_dma_ready,
            .cb_cmd9 = cb_cmd9,
            .cb_cmdA = cb_cmdA,
            .cb_buffer_tx = NULL,
            .cb_buffer_rx = cb_buffer_rx }
    };
    gpio_pin_glitch_filter_config_t glitch_cfg = {
        .gpio_num = CONFIG_X_GPIO_FPGA_SCLK,
    };
    spi_dev_t *spi2 = SPI_LL_GET_HW(1);

    gpio_config_t gpio_cfg = {
        .pin_bit_mask = (1ULL << CONFIG_X_GPIO_FPGA_MISO),
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE
    };
    gpio_config(&gpio_cfg);

    gpio_set_pull_mode(CONFIG_X_GPIO_FPGA_MOSI, GPIO_PULLUP_ONLY);
    gpio_set_pull_mode(CONFIG_X_GPIO_FPGA_SCLK, GPIO_PULLUP_ONLY);

    spi_slave_hd_init(SPI2_HOST, &buscfg, &slvcfg);
    gpio_new_pin_glitch_filter(&glitch_cfg, &s_glitch);
    // ESP_ERROR_CHECK(gpio_glitch_filter_enable(s_glitch));

    // Set GPIO_FUNC110_IN_INV_SEL = 1
    *(uint32_t *)(0x60004000 + 0x154 + 0x4 * 110) |= (1 << 6);

    s_xfer.len = SPIBUF_SIZE;
    s_xfer.data = s_spibuf;

    // magic bit to output data half a SPI cycle early
    spi2->slave.rsck_data_out = 1;

    uint8_t buf = 0x42;
    spi_slave_hd_write_buffer(SPI2_HOST, 9, &buf, 1);
}

void spi_end()
{
    spi_slave_hd_deinit(SPI2_HOST);

    // ESP_ERROR_CHECK(gpio_glitch_filter_disable(s_glitch));
    gpio_del_glitch_filter(s_glitch);

    gpio_config_t g = {
        .pin_bit_mask = ((uint64_t)1 << CONFIG_X_GPIO_FPGA_MOSI)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_MISO)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_SCLK)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_CS),
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE
    };
    gpio_config(&g);
}
