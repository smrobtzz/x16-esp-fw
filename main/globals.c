// SPDX-License-Identifier: GPL-3.0-only
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include "config.h"
#include "defs.h"
#include "hid.h"

TaskHandle_t g_reset_task = NULL;

#if CONFIG_X_VERA_FLASH_ENABLE
TaskHandle_t g_flash_task = NULL;
#endif

#if CONFIG_X_DOS_ENABLE
TaskHandle_t g_dos_task = NULL;
#endif

#if CONFIG_X_I2C_ENABLE
TaskHandle_t g_i2c_task = NULL;
#endif

TaskHandle_t g_config_task = NULL;

QueueHandle_t g_hid_event_queue;

QueueHandle_t g_key_queue = NULL;

uint8_t            g_cur_mouse_evt = 0;
struct mouse_event g_mouse_evt[2];

uint8_t         *g_autoboot_prg = NULL;
uint64_t         g_autoboot_size = 0;
volatile uint8_t g_reset_byte = 0;

bool g_wifi_started = false;

struct config g_config = {
    .log.val = 0,
    .cur_ap = 0xFF
};
