// SPDX-License-Identifier: GPL-3.0-only
#include <stddef.h>
#include <stdlib.h>

#include <driver/spi_slave_hd.h>

#include "device.h"
#include "dos.h"
#include "spi.h"

#ifndef CONFIG_X_DOS_ENABLE
#define CONFIG_X_DOS_ENABLE 0
#endif
#ifndef CONFIG_X_NET_ENABLE
#define CONFIG_X_NET_ENABLE 0
#endif

const size_t NIFACE = CONFIG_X_DOS_ENABLE + CONFIG_X_NET_ENABLE;

void     device_exec(struct device *dev);
uint32_t device_loop(struct device *dev);
uint32_t device_spi_xfer(struct device *dev, spi_slave_chan_t chan);

#if CONFIG_X_NET_ENABLE == 1
struct device_iface *net_init();
#endif

void device_task(void *arg)
{
    size_t i = 0;

    struct device dev = {
        .iface = calloc(sizeof(struct device_iface), NIFACE)
    };

#if CONFIG_X_DOS_ENABLE == 1
    dev.iface[i++] = dos_init();
#endif
#if CONFIG_X_NET_ENABLE == 1
    dev.iface[i++] = net_init();
#endif

    device_exec(&dev);
}

void device_exec(struct device *dev)
{
    uint32_t msg;
    while (1) {
        msg = ulTaskNotifyTakeIndexed(DOS_MAILBOX, pdTRUE, portMAX_DELAY);
        switch (msg) {
        case DOS_MSG_START:
        case DOS_MSG_RESET:
            break;
#if CONFIG_X_VERA_FLASH_ENABLE
        case DOS_MSG_STOP_FLASH:
            xTaskNotifyIndexed(
                g_flash_task,
                FLASH_MAILBOX,
                FLASH_MSG_ACK,
                eSetValueWithOverwrite);
            continue;
#endif
        case DOS_MSG_STOP_RESET:
            xTaskNotifyIndexed(
                g_reset_task,
                RESET_MAILBOX,
                RESET_MSG_ACK,
                eSetValueWithOverwrite);
            continue;
        default:
            continue;
        }

        for (size_t i = 0; i < NIFACE; i++) {
            struct device_iface *iface = dev->iface[i];
            iface->start(iface);
        }

        msg = device_loop(dev);

        for (size_t i = 0; i < NIFACE; i++) {
            struct device_iface *iface = dev->iface[i];
            iface->stop(iface);
        }

        xTaskNotifyStateClearIndexed(NULL, DOS_MAILBOX);
        switch (msg) {
#if CONFIG_X_VERA_FLASH_ENABLE
        case DOS_MSG_STOP_FLASH:
            xTaskNotifyIndexed(
                g_flash_task,
                FLASH_MAILBOX,
                FLASH_MSG_ACK,
                eSetValueWithOverwrite);
            break;
#endif
        case DOS_MSG_STOP_RESET:
            xTaskNotifyIndexed(
                g_reset_task,
                RESET_MAILBOX,
                RESET_MSG_ACK,
                eSetValueWithOverwrite);
            break;
        }
    }
}

uint32_t device_loop(struct device *dev)
{
    uint32_t msg;

    while (1) {
        if ((msg = device_spi_xfer(dev, SPI_SLAVE_CHAN_RX)) != DOS_MSG_SPI)
            return msg;

        uint8_t cmd = s_spibuf[2];

        for (size_t i = 0; i < NIFACE; i++) {
            struct device_iface *iface = dev->iface[i];
            if (cmd >= iface->cmd_start && cmd <= iface->cmd_end)
                iface->cmd(iface, cmd, s_spibuf);
        }

        if ((msg = device_spi_xfer(dev, SPI_SLAVE_CHAN_TX)) != DOS_MSG_SPI)
            return msg;
    }
}

uint32_t device_spi_xfer(struct device *dev, spi_slave_chan_t chan)
{
    uint32_t msg;

    spi_slave_hd_queue_trans(SPI2_HOST, chan, &s_xfer, portMAX_DELAY);
    msg = ulTaskNotifyTakeIndexed(DOS_MAILBOX, pdTRUE, portMAX_DELAY);
    return msg;
}
