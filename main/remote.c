// SPDX-License-Identifier: GPL-3.0-only
#include <driver/spi_slave_hd.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>

#include <lwip/tcpip.h>
#include <lwip/udp.h>

#include "defs.h"
#include "esp_log.h"
#include "hid.h"
#include "remote.h"

void remote_udp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *pb, const ip_addr_t *addr, uint16_t port)
{
    uint8_t              one = 1;
    struct remote_report *report = pb->payload;

    if (report->key != 0)
        xQueueSend(g_key_queue, &report->key, 3);

    g_mouse_evt[g_cur_mouse_evt].bt = report->bt | (1 << 3);
    g_mouse_evt[g_cur_mouse_evt].dw = report->w;
    g_mouse_evt[g_cur_mouse_evt].changed = true;

    spi_slave_hd_write_buffer(SPI2_HOST, 16, (uint8_t *)report, 4);
    spi_slave_hd_write_buffer(SPI2_HOST, 15, &one, 1);

    pbuf_free(pb);
}

static struct udp_pcb *s_remote_pcb;

void remote_init()
{
    LOCK_TCPIP_CORE();
    s_remote_pcb = udp_new();
    udp_bind(s_remote_pcb, IP_ANY_TYPE, REMOTE_PORT);
    udp_recv(s_remote_pcb, remote_udp_recv, NULL);
    UNLOCK_TCPIP_CORE();
}
