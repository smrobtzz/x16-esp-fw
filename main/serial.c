// SPDX-License-Identifier: GPL-3.0-only
#include <driver/uart.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_intr_alloc.h>

#include <freertos/FreeRTOS.h>

#include "defs.h"
#include "net.h"

void serial_task(void *arg)
{
    esp_err_t     res;
    QueueHandle_t uart_queue;
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_CTS_RTS,
    };

    res = uart_set_pin(UART_NUM, CONFIG_X_GPIO_TX, CONFIG_X_GPIO_RX, CONFIG_X_GPIO_RTS, CONFIG_X_GPIO_CTS);
    if (res != ESP_OK) {
        LOG(serial, "failed to set uart pins: %s", esp_err_to_name(res));
        vTaskDelete(NULL);
    }

    res = uart_param_config(UART_NUM, &uart_config);
    if (res != ESP_OK) {
        LOG(serial, "failed to configure uart: %s", esp_err_to_name(res));
        vTaskDelete(NULL);
    }

    res = uart_driver_install(UART_NUM, 512, 512, 10, &uart_queue, 0);
    if (res != ESP_OK) {
        LOG(serial, "failed to install uart driver: %s", esp_err_to_name(res));
        vTaskDelete(NULL);
    }

    while (true) {
        uart_event_t event;
        xQueueReceive(uart_queue, &event, portMAX_DELAY);

        if (event.type == UART_DATA)
            net_serial_did_receive();
    }
}

uint8_t serial_set_params(uint8_t arg0, uint8_t arg1)
{
    const uint32_t baud_rates[] = {
        9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600
    };
    esp_err_t res = 0;

    res |= uart_set_baudrate(UART_NUM, baud_rates[(arg0 >> SER_ARG0_BAUD_SHIFT) & SER_ARG0_BAUD_MASK]);
    if (((arg0 >> SER_ARG0_FLOW_SHIFT) & SER_ARG0_FLOW_MASK) == 2) {
        res |= uart_set_sw_flow_ctrl(UART_NUM, false, 0, 0);
        res |= uart_set_hw_flow_ctrl(UART_NUM, UART_HW_FLOWCTRL_CTS_RTS, 122);
    } else if (((arg0 >> SER_ARG0_FLOW_SHIFT) & SER_ARG0_FLOW_MASK) == 1) {
        res |= uart_set_sw_flow_ctrl(UART_NUM, true, 20, 100);
        res |= uart_set_hw_flow_ctrl(UART_NUM, UART_HW_FLOWCTRL_DISABLE, 0);
    } else {
        res |= uart_set_hw_flow_ctrl(UART_NUM, UART_HW_FLOWCTRL_DISABLE, 0);
        res |= uart_set_sw_flow_ctrl(UART_NUM, false, 0, 0);
    }

    res |= uart_set_word_length(UART_NUM, (arg0 >> SER_ARG0_DATA_BITS_SHIFT) & SER_ARG0_DATA_BITS_MASK);
    res |= uart_set_stop_bits(UART_NUM, (arg1 >> SER_ARG1_STOP_BITS_SHIFT) & SER_ARG1_STOP_BITS_MASK);
    res |= uart_set_parity(UART_NUM, (arg1 >> SER_ARG1_PARITY_SHIFT) & SER_ARG1_PARITY_MASK);

    return (res != 0);
}
