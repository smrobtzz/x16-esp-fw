// SPDX-License-Identifier: GPL-3.0-only
#include <sys/param.h>
#include <unistd.h>

#include <driver/gpio.h>
#include <driver/spi_common.h>
#include <esp_flash_spi_init.h>
#include <esp_http_server.h>
#include <esp_log.h>
#include <esp_ota_ops.h>
#include <mdns.h>

#include "defs.h"
#include "flash.h"
#include "spi.h"

#define SPI_HOST SPI2_HOST

esp_err_t vera_post(httpd_req_t *req);
esp_err_t vera_get(httpd_req_t *req);
esp_err_t esp32_post(httpd_req_t *req);
esp_err_t run_post(httpd_req_t *req);
esp_err_t runh_post(httpd_req_t *req);

const httpd_uri_t VERA_POST = {
    .uri = "/vera.bin",
    .method = HTTP_POST,
    .handler = vera_post,
    .user_ctx = NULL,
};

const httpd_uri_t VERA_GET = {
    .uri = "/vera.bin",
    .method = HTTP_GET,
    .handler = vera_get,
    .user_ctx = NULL,
};

const httpd_uri_t ESP32_POST = {
    .uri = "/esp32.bin",
    .method = HTTP_POST,
    .handler = esp32_post,
    .user_ctx = NULL
};

const httpd_uri_t RUN_POST = {
    .uri = "/run.prg",
    .method = HTTP_POST,
    .handler = run_post,
    .user_ctx = NULL
};

const httpd_uri_t RUNH_POST = {
    .uri = "/runh.prg",
    .method = HTTP_POST,
    .handler = runh_post,
    .user_ctx = NULL
};

static esp_flash_t *s_chip;
static httpd_req_t *s_req;
static uint8_t     *s_vera_fw_dat;
static size_t       s_vera_fw_len;

static void flash_vera_begin()
{
    int                           res;
    spi_bus_config_t              bus_config;
    esp_flash_spi_device_config_t flash_config;

    gpio_set_level(CONFIG_X_GPIO_RESET, 0);
    gpio_set_level(CONFIG_X_GPIO_SD_EN, 0);

    while (gpio_get_level(CONFIG_X_GPIO_CDONE) == 1) { }

    bus_config = (spi_bus_config_t) {
        .mosi_io_num = CONFIG_X_GPIO_FPGA_MOSI,
        .miso_io_num = CONFIG_X_GPIO_FPGA_MISO,
        .sclk_io_num = CONFIG_X_GPIO_FPGA_SCLK,
        .max_transfer_sz = 0,
        .flags = 0,
        .isr_cpu_id = 1,
    };
    res = spi_bus_initialize(SPI_HOST, &bus_config, SPI_DMA_CH_AUTO);
    if (res != ESP_OK)
        goto fail1;

    flash_config = (esp_flash_spi_device_config_t) {
        .host_id = SPI_HOST,
        .cs_io_num = CONFIG_X_GPIO_FPGA_CS,
        .io_mode = SPI_FLASH_FASTRD,
        .input_delay_ns = 0,
        .freq_mhz = 15,
    };
    res = spi_bus_add_flash_device(&s_chip, &flash_config);
    if (res != ESP_OK)
        goto fail2;

    res = esp_flash_init(s_chip);
    if (res != ESP_OK)
        goto fail3;

    LOG_IF(flash, "initialized");
    return;

fail3:
    spi_bus_remove_flash_device(s_chip);
fail2:
    spi_bus_free(SPI_HOST);
fail1:
    LOG(flash, "failed to initialize: %s", esp_err_to_name(res));
}

static void flash_vera_end()
{
    spi_bus_remove_flash_device(s_chip);
    spi_bus_free(SPI_HOST);
    gpio_config_t gpio_cfg = {
        .pin_bit_mask = ((uint64_t)1 << CONFIG_X_GPIO_FPGA_MOSI)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_MISO)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_SCLK)
            | ((uint64_t)1 << CONFIG_X_GPIO_FPGA_CS),
        .mode = GPIO_MODE_INPUT,
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE,
    };
    gpio_config(&gpio_cfg);

    LOG_IF(flash, "uninitialized");
}

esp_err_t vera_post(httpd_req_t *req)
{
    size_t left = req->content_len, addr = 0;
    int    res;

    LOG_IF(flash, "POST vera.bin");
    s_vera_fw_len = req->content_len;
    if (s_vera_fw_len % 4096 != 0) {
        s_vera_fw_len += 4096 - s_vera_fw_len % 4096;
    }
    s_vera_fw_dat = malloc(s_vera_fw_len);
    if (s_vera_fw_dat == NULL) {
        LOG(flash, "failed to allocate buffer for firmware file");
        httpd_resp_sendstr(req, "VERA flash failed");
    }

    while (left != 0) {
        res = httpd_req_recv(req, (char *)s_vera_fw_dat + addr, left);
        if (res <= 0) {
            LOG(flash, "failed to receive firwmare data");
            httpd_resp_sendstr(req, "VERA flash failed");
            return ESP_OK;
        }
        left -= res;
        addr += res;
    }

    httpd_req_async_handler_begin(req, &s_req);
    xTaskNotifyIndexed(
        g_flash_task,
        FLASH_MAILBOX,
        FLASH_MSG_START_WRITE_VERA, eSetValueWithOverwrite);

    return ESP_OK;
}

esp_err_t vera_get(httpd_req_t *req)
{
    LOG_IF(flash, "GET vera.bin");
    httpd_req_async_handler_begin(req, &s_req);
    xTaskNotifyIndexed(
        g_flash_task,
        FLASH_MAILBOX,
        FLASH_MSG_START_READ_VERA, eSetValueWithOverwrite);

    return ESP_OK;
}

esp_err_t esp32_post(httpd_req_t *req)
{
    LOG_IF(flash, "POST esp32.bin");

    size_t                 left, cnt;
    char                  *fw_bin;
    int                    res;
    const esp_partition_t *part;
    esp_ota_handle_t       ota;
    size_t                 total = req->content_len;
    left = total;

    part = esp_ota_get_next_update_partition(NULL);
    if (part == NULL) {
        httpd_resp_sendstr(req, "Update failed: no valid OTA partition\n");
        return ESP_OK;
    }

    fw_bin = calloc(1, 4096);
    if (fw_bin == NULL) {
        httpd_resp_sendstr(req, "Update failed: out of memory\n");
        return ESP_OK;
    }

    res = esp_ota_begin(part, total, &ota);
    if (res != ESP_OK) {
        free(fw_bin);
        httpd_resp_sendstr_chunk(req, "Update failed: ");
        httpd_resp_sendstr_chunk(req, esp_err_to_name(res));
        httpd_resp_sendstr_chunk(req, "\n");
        httpd_resp_sendstr_chunk(req, NULL);
        return ESP_OK;
    }

    while (left != 0) {
        cnt = MIN(left, 4096);

        res = httpd_req_recv(req, fw_bin, cnt);
        if (res < 0)
            goto flash_esp32_fail1;
        left -= res;

        res = esp_ota_write(ota, fw_bin, res);
        if (res != ESP_OK)
            goto flash_esp32_fail1;
    }

    res = esp_ota_end(ota);
    if (res != ESP_OK)
        goto flash_esp32_fail2;

    res = esp_ota_set_boot_partition(part);
    if (res != ESP_OK)
        goto flash_esp32_fail2;

    httpd_resp_sendstr(req, "Update complete\n");
    vTaskDelay(50);
    esp_restart();
    return ESP_OK;

flash_esp32_fail1:
    esp_ota_abort(ota);
flash_esp32_fail2:
    free(fw_bin);
    httpd_resp_sendstr_chunk(req, "Update failed: ");
    httpd_resp_sendstr_chunk(req, esp_err_to_name(res));
    httpd_resp_sendstr_chunk(req, "\n");
    httpd_resp_sendstr_chunk(req, NULL);
    return ESP_OK;
}

static void run_post_base(httpd_req_t *req)
{
    size_t len = req->content_len, left = len, pos = 0;
    char  *buf = malloc(len);
    int    res;

    if (buf == NULL) {
        httpd_resp_sendstr(req, "Upload failed: out of memory");
        return;
    }

    while (left != 0) {
        res = httpd_req_recv(req, buf + pos, left);
        if (res < 0) {
            free(buf);
            httpd_resp_sendstr(req, "Upload failed: HTTP server error");
            return;
        }

        left -= res;
        pos += res;
    }

    g_autoboot_prg = (uint8_t *)buf;
    g_autoboot_size = len;

    httpd_resp_sendstr(req, "Upload succeeded\n");
}

// Soft reset sequence:
//  1. [HTTP task]   send FLASH_MSG_START_SOFT_RESET
//  2. [flash task]  set g_reset_byte = 1
//  3. [X16]         keyboard handler sees reset byte == 1,
//                   sends cmd9, waits for reset byte == 2
//  4. [cmd9 irq]    send FLASH_MSG_CONT_SOFT_RESET
//  5. [flash task]  send DOS_MSG_STOP_FLASH, wait for FLASH_MSG_ACK
//  6. [flash task]  spi_end(), spi_start() to clear transactions
//  7. [flash task]  send DOS_MSG_START
//  8. [flash task]  set g_reset_byte = 2
//  9. [X16]         sees reset byte == 2, exits native mode,
//                   jumps to reset vector
esp_err_t run_post(httpd_req_t *req)
{
    run_post_base(req);
    xTaskNotifyIndexed(
        g_flash_task,
        FLASH_MAILBOX,
        FLASH_MSG_START_SOFT_RESET, eSetValueWithOverwrite);
    return ESP_OK;
}

esp_err_t runh_post(httpd_req_t *req)
{
    run_post_base(req);
    gpio_set_level(CONFIG_X_GPIO_RESET, 0);
    return ESP_OK;
}

void flash_task(void *arg)
{
    httpd_config_t config;
    httpd_handle_t server;
    char          *fw_bin = NULL;
    size_t         left, addr, cnt;
    int            res;
    uint32_t       msg;

    config = (httpd_config_t)HTTPD_DEFAULT_CONFIG();
    config.core_id = tskNO_AFFINITY;
    config.server_port = 8080;
    config.ctrl_port = 8081;

    ESP_ERROR_CHECK(httpd_start(&server, &config));
    httpd_register_uri_handler(server, &VERA_POST);
    httpd_register_uri_handler(server, &VERA_GET);
    httpd_register_uri_handler(server, &ESP32_POST);
    httpd_register_uri_handler(server, &RUN_POST);
    httpd_register_uri_handler(server, &RUNH_POST);

    while (1) {
        msg = ulTaskNotifyTakeIndexed(FLASH_MAILBOX, pdTRUE, portMAX_DELAY);
        if (msg != FLASH_MSG_START_SOFT_RESET) {
            xTaskNotifyIndexed(g_dos_task,
                DOS_MAILBOX,
                DOS_MSG_STOP_FLASH,
                eSetValueWithOverwrite);
            assert(ulTaskNotifyTakeIndexed(FLASH_MAILBOX, pdTRUE, portMAX_DELAY) == FLASH_MSG_ACK);
            xTaskNotifyIndexed(g_reset_task,
                RESET_MAILBOX,
                RESET_MSG_STOP_FLASH,
                eSetValueWithOverwrite);
            assert(ulTaskNotifyTakeIndexed(FLASH_MAILBOX, pdTRUE, portMAX_DELAY) == FLASH_MSG_ACK);
        }

        switch (msg) {
        case FLASH_MSG_START_READ_VERA: {
            httpd_resp_set_type(s_req, "application/octet-stream");

            flash_vera_begin();

            fw_bin = heap_caps_malloc(8192, MALLOC_CAP_DMA);
            if (fw_bin == NULL) {
                LOG(flash, "failed to allocate memory for reading flash");
                goto done_vera_read;
            }

            addr = 0;
            left = 131072;
            cnt = 8192;
            while (left != 0) {
                res = esp_flash_read(s_chip, fw_bin, addr, cnt);
                httpd_resp_send_chunk(s_req, fw_bin, cnt);
                left -= cnt;
                addr += cnt;
            }

        done_vera_read:
            flash_vera_end();
            httpd_resp_send_chunk(s_req, NULL, 0);
            free(fw_bin);
            break;
        }
        case FLASH_MSG_START_WRITE_VERA: {
            flash_vera_begin();

            fw_bin = heap_caps_malloc(4096, MALLOC_CAP_DMA);
            if (fw_bin == NULL) {
                LOG(flash, "failed to allocate memory for writing flash");
                flash_vera_end();
                httpd_resp_sendstr(s_req, "VERA flash failed");
                break;
            }

            addr = 0;
            left = s_vera_fw_len;
            cnt = 4096;
            while (left != 0) {
                memcpy(fw_bin, s_vera_fw_dat + addr, cnt);

                res = esp_flash_erase_region(s_chip, addr, cnt);
                if (res != ESP_OK) {
                    ESP_LOGE("flash", "esp_flash_erase_region fail: %s", esp_err_to_name(res));
                }

                esp_flash_write(s_chip, fw_bin, addr, cnt);
                if (res != ESP_OK) {
                    ESP_LOGE("flash", "esp_flash_write fail: %s", esp_err_to_name(res));
                }

                addr += cnt;
                left -= cnt;
            }
            flash_vera_end();

            httpd_resp_sendstr(s_req, "VERA flash complete");
            free(fw_bin);
            break;
        }
        case FLASH_MSG_START_SOFT_RESET:
            g_reset_byte = 1;
            assert(ulTaskNotifyTakeIndexed(FLASH_MAILBOX, pdTRUE, portMAX_DELAY)
                == FLASH_MSG_CONT_SOFT_RESET);

            xTaskNotifyIndexed(g_dos_task,
                DOS_MAILBOX,
                DOS_MSG_STOP_FLASH,
                eSetValueWithOverwrite);
            assert(ulTaskNotifyTakeIndexed(FLASH_MAILBOX, pdTRUE, portMAX_DELAY)
                == FLASH_MSG_ACK);

            spi_end();
            spi_start();

            xTaskNotifyIndexed(g_dos_task,
                DOS_MAILBOX,
                DOS_MSG_START,
                eSetValueWithOverwrite);

            g_reset_byte = 2;
            spi_slave_hd_write_buffer(SPI2_HOST, 2, (uint8_t *)&g_reset_byte, 1);

            continue;
        default:
            break;
        }

        httpd_req_async_handler_complete(s_req);
        xTaskNotifyIndexed(g_reset_task, RESET_MAILBOX, RESET_MSG_START, eSetValueWithOverwrite);
    }
}
