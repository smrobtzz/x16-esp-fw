// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <stdint.h>

#define DOS_CMD_START  DOS_CMD_TALK
#define DOS_CMD_TALK   0
#define DOS_CMD_TKSA   1
#define DOS_CMD_UNTLK  2
#define DOS_CMD_LISTN  3
#define DOS_CMD_SECND  4
#define DOS_CMD_UNLSN  5
#define DOS_CMD_CIOUT  6
#define DOS_CMD_ACPTR  7
#define DOS_CMD_MCIOUT 8
#define DOS_CMD_MACPTR 9
#define DOS_CMD_END    DOS_CMD_MACPTR

#define NET_CMD_START         NET_CMD_OPEN
#define NET_CMD_OPEN          10
#define NET_CMD_CLOSE         11
#define NET_CMD_SEND          12
#define NET_CMD_RECV          13
#define NET_CMD_GETHOSTBYNAME 14
#define NET_CMD_END           NET_CMD_GETHOSTBYNAME

struct device_iface {
    void (*start)(struct device_iface *i);
    void (*cmd)(struct device_iface *i, uint8_t cmd, uint8_t *data);
    void (*stop)(struct device_iface *i);
    uint8_t cmd_start, cmd_end;
};

struct device {
    struct device_iface **iface;
};

void device_task(void *);
