// SPDX-License-Identifier: GPL-3.0-only
#pragma once

void sd_init();
void sd_mount();
