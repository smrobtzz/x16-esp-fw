// SPDX-License-Identifier: GPL-3.0-only

#pragma once

#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD   WIFI_AUTH_WPA2_PSK

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

extern const char *WIFI_TAG;

void wifi_init_sta(void);
void wifi_configure(char *ssid, char *password);
