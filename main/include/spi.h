// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <stdint.h>

#include <driver/spi_slave_hd.h>

extern spi_slave_hd_data_t s_xfer;
extern uint8_t             s_spibuf[260];

void spi_start();
void spi_end();
