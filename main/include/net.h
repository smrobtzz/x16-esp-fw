// SPDX-License-Identifier: GPL-3.0-only

#include <lwip/ip_addr.h>
#include <lwip/raw.h>
#include <lwip/tcp.h>

#include "device.h"

#define NET_MAX_BUF_SIZE 16384

#define IP_V4 4
#define IP_V6 6

#define NET_SOCK_RAW     0
#define NET_SOCK_TCP     1
#define NET_SOCK_DBG     2
#define NET_SOCK_SERIAL  3
#define NET_SOCK_UDP     4
#define NET_SOCK_INVALID 0xFF

#define SER_ARG0_BAUD_SHIFT      0
#define SER_ARG0_BAUD_MASK       0b111
#define SER_ARG0_FLOW_SHIFT      3
#define SER_ARG0_FLOW_MASK       0b11
#define SER_ARG0_DATA_BITS_SHIFT 5
#define SER_ARG0_DATA_BITS_MASK  0b11
#define SER_ARG1_STOP_BITS_SHIFT 0
#define SER_ARG1_STOP_BITS_MASK  0b11
#define SER_ARG1_PARITY_SHIFT    2
#define SER_ARG1_PARITY_MASK     0b11

#define DBG_TAG_STR 0

#define DBG_TAG_INT8   1
#define DBG_TAG_UINT8  2
#define DBG_TAG_INT16  3
#define DBG_TAG_UINT16 4
#define DBG_TAG_INT32  5
#define DBG_TAG_UINT32 6
#define DBG_BASE_HEX   0
#define DBG_BASE_DEC   1

#define SOCK_STAT_HAS_DATA  1
#define SOCK_STAT_CONNECTED 2
#define SOCK_STAT_ERROR     3

#define SOCK_ERR_ABRT  (0 << 5)
#define SOCK_ERR_RST   (1 << 5)
#define SOCK_ERR_CLSD  (2 << 5)
#define SOCK_ERR_OTHER (7 << 5)

struct net_buf {
    uint8_t        *data;
    struct net_buf *next;
    size_t          pos;
    size_t          len;
};

struct net_ip {
    uint8_t data[16];
    uint8_t type;
};

struct net_socket {
    union {
        struct raw_pcb *raw_pcb;
        struct tcp_pcb *tcp_pcb;
        struct udp_pcb *udp_pcb;
    };
    uint8_t           id;
    uint8_t           type;
    uint8_t           status;
    struct net_ip     ip;
    struct net_buf   *bhead;
    struct net_buf   *btail;
    SemaphoreHandle_t socklock;
};

struct net_iface {
    struct device_iface d;
    struct net_socket   s[4];
};

struct net_open_args {
    struct net_ip addr;
    uint8_t       socket;
    uint8_t       type;
    uint8_t       arg0;
    uint8_t       arg1;
};

struct net_close_args {
    uint8_t socket;
};

struct net_send_args {
    uint8_t  socket;
    uint16_t size;
} __attribute__((packed));

struct net_recv_args {
    uint8_t  socket;
    uint16_t size;
} __attribute__((packed));

struct net_tcpip_open_args {
    struct net_socket *s;
    uint16_t           port;
};

struct net_tcpip_send_args {
    struct net_socket *s;
    uint8_t           *data;
    uint16_t           size;
};

struct net_dns_found_cb_args {
    TaskHandle_t task;
    ip_addr_t   *ip;
};

struct net_tcpip_open_raw_args {
    struct net_socket *s;
    uint8_t            arg0;
};

struct net_tcpip_send_raw_args {
    struct net_socket *s;
    uint8_t           *data;
    uint16_t           size;
};

void net_serial_did_receive();
