// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <esp_wifi.h>

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/queue.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <usb/hid_host.h>
#include <usb/msc_host.h>

#include "config.h"

#define TAG     "x16_esp_fw"
#define DOS_TAG "dos"

#define DOS_MAILBOX        0
#define DOS_MSG_START      1
#define DOS_MSG_RESET      2
#define DOS_MSG_STOP_FLASH 3
#define DOS_MSG_STOP_I2C   4
#define DOS_MSG_STOP_RESET 5
#define DOS_MSG_SPI        6

#define FLASH_MAILBOX              0
#define FLASH_MSG_START_READ_VERA  1
#define FLASH_MSG_START_WRITE_VERA 2
#define FLASH_MSG_START_SOFT_RESET 3
#define FLASH_MSG_CONT_SOFT_RESET  4
#define FLASH_MSG_ACK              5

#define RESET_MAILBOX        0
#define RESET_MSG_RESET      1
#define RESET_MSG_ACK        2
#define RESET_MSG_STOP_FLASH 3
#define RESET_MSG_START      4

#define CONFIG_MAILBOX       0
#define CONFIG_MSG_WIFI_SUCC 1
#define CONFIG_MSG_WIFI_FAIL 2

#define UART_NUM UART_NUM_1

struct config;

#if CONFIG_X_LOG_ENABLE

#define LOG_IF(a, ...)                                        \
    if (g_config.log.a) {                                     \
        ESP_LOGE(CONFIG_X_NET_NAME " [" #a "]", __VA_ARGS__); \
    }

#define DRAM_LOG_IF(a, ...)                                        \
    if (g_config.log.a) {                                          \
        ESP_DRAM_LOGE(CONFIG_X_NET_NAME " [" #a "]", __VA_ARGS__); \
    }

#define LOG(a, ...) ESP_LOGE(CONFIG_X_NET_NAME " [" #a "]", __VA_ARGS__);

#else

#define LOG_IF(a, ...)
#define DRAM_LOG_IF(a, ...)
#define LOG(a, ...)

#endif

void i2c_task(void *);
void reset_task(void *);
void usb_task(void *);

extern TaskHandle_t g_reset_task;

#if CONFIG_X_VERA_FLASH_ENABLE
extern TaskHandle_t g_flash_task;
#endif

#if CONFIG_X_DOS_ENABLE
extern TaskHandle_t g_dos_task;
#endif

#if CONFIG_X_I2C_ENABLE
extern TaskHandle_t g_i2c_task;
#endif

extern TaskHandle_t g_config_task;

#if CONFIG_X_USB_HOST_ENABLE
struct usb_driver_event {
    enum {
        HID_DRIVER_EVENT,
        MSC_DRIVER_EVENT,
    } event;
    union {
        struct {
            hid_host_device_handle_t handle;
            hid_host_driver_event_t  event;
        } hid;
        const msc_host_event_t msc;
    };
    void *arg;
};
#endif

extern uint8_t         *g_autoboot_prg;
extern uint64_t         g_autoboot_size;
extern volatile uint8_t g_reset_byte;

extern bool g_wifi_started;

extern struct config g_config;

extern SemaphoreHandle_t g_volumes_lock;
