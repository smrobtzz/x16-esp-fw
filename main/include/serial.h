// SPDX-License-Identifier: GPL-3.0-only
#include <stdint.h>

uint8_t serial_set_params(uint8_t arg0, uint8_t arg1);
