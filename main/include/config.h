// SPDX-License-Identifier: GPL-3.0-only
#include <stdbool.h>

#include <esp_netif_ip_addr.h>
#include <esp_wifi_types.h>
#include <lwip/sockets.h>

#include <mdns.h>
#include <nvs.h>

#pragma once

#define SERV_PORT 2

#define K_CURS_UP    0x100
#define K_CURS_DOWN  0x101
#define K_CURS_RIGHT 0x102
#define K_CURS_LEFT  0x103
#define K_ENTER      0x0a
#define K_BS         0x7f
#define K_TAB        0x09
#define K_CTRL_X     0x18

#define BOX_TOP_LEFT     0xb0
#define BOX_TOP_RIGHT    0xae
#define BOX_BOTTOM_LEFT  0xed
#define BOX_BOTTOM_RIGHT 0xbd
#define BOX_HORZ         0x60
#define BOX_VERT         0x7d

struct cli;

void config_load();
void config_handle_client(struct cli *cli);
bool config_main_menu(struct cli *cli);
void config_wifi_menu(struct cli *cli);
void config_hostname_menu(struct cli *cli);
void config_log_menu(struct cli *cli);
void config_key_repeat_menu(struct cli *cli);

void config_nvs_update(nvs_handle_t nvs);

void cli_println(struct cli *cli, char *str);
void cli_println_ascii(struct cli *cli, char *str);
void cli_print(struct cli *cli, char *str);
void cli_print_ascii(struct cli *cli, char *str);
void cli_goto_line(struct cli *cli, uint8_t line);
void cli_reverse_on(struct cli *cli);
void cli_reverse_off(struct cli *cli);
void cli_menu_option(struct cli *cli, char *opt, bool sel);
void cli_cls(struct cli *cli);
void cli_putc(struct cli *cli, char c);
void cli_putc_many(struct cli *cli, char c, unsigned cnt);
void cli_wrote(struct cli *cli, unsigned cnt);
bool cli_getc(struct cli *cli, uint16_t *c);
bool cli_waitin(struct cli *cli, unsigned ms);
void cli_goto(struct cli *cli, uint8_t x, uint8_t y);

void cli_draw_box(struct cli *cli, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void cli_draw_divider(struct cli *cli, uint8_t x, uint8_t y, uint8_t width);

void utf8_fill(char *buf, char *chr, size_t cnt);

wifi_ap_record_t *wifi_scan(struct cli *cli, uint16_t *ap_num);

struct cli {
    int                     fd;
    fd_set                  fds_rd;
    struct sockaddr        *addr;
    struct sockaddr_storage _addr;
    uint8_t                 x, y;
    bool                    cp437;
};

struct wifi_ap {
    char mac[6];
    char ssid[33];
    char password[64];
    bool valid;
};

struct log_options {
    union {
        struct {
            uint32_t dos0 : 1;
            uint32_t dos1 : 1;
            uint32_t webdav : 1;
            uint32_t flash : 1;
            uint32_t i2c : 1;
            uint32_t usb : 1;
            uint32_t sd : 1;
            uint32_t reset : 1;
            uint32_t net : 1;

            // Keep wifi at the end so the memset(..., sizeof(struct log_options) - 1)
            // in reset_cfg_log works as intended
            uint32_t wifi : 1;
        };
        uint32_t val;
    };
};

struct config {
    struct log_options log;
    struct wifi_ap     known_aps[4];
    esp_ip4_addr_t     ip;
    unsigned char      cur_ap; // 255 = none, 0-3 = known_aps
    uint8_t            ap_cnt;
    char               hostname[65];

    // Specified in FreeRTOS ticks
    uint32_t repeat_delay;
    uint32_t repeat_interval;

    char main_volume[32];
    char dos_volumes[4][32];
};
