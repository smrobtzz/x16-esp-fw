// SPDX-License-Identifier: GPL-3.0-only
#include <stdint.h>

void remote_init();

struct remote_report {
    uint16_t x;
    uint16_t y;
    int8_t   w;
    uint8_t  bt;
    uint8_t  key;
} __attribute__((packed));

#define REMOTE_PORT 44444
