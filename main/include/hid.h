// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <stdint.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/timers.h>

#define HID_EVENT_REGISTER_DEVICE   0
#define HID_EVENT_UNREGISTER_DEVICE 1
#define HID_EVENT_SET_LIGHTS        2

// No key is held
#define REPEAT_STATE_NONE 0
// A key is held, delay before repeating
#define REPEAT_STATE_DELAY 1
// A key is repeating
#define REPEAT_STATE_REPEAT 2

#define DEFAULT_REPEAT_DELAY    pdMS_TO_TICKS(500)
#define DEFAULT_REPEAT_INTERVAL pdMS_TO_TICKS(100)

struct hid {
    // --- Provided by device driver ---
    // Called when a device is removed. Must free
    // `report_buf` and `device_data`.
    void (*remove)(struct hid *dev);

    // Handle a HID report. Device already knows the format,
    // and sends events to keyboard or mouse queues
    // on its own.
    void (*handle_report)(struct hid *dev, size_t report_len);

    // Set keyboard lights. May be NULL.
    // `lights` is in the format sent by the X16 kernel:
    //     Bit 0 - Scroll lock
    //     Bit 1 - Num lock
    //     Bit 2 - Caps lock
    void (*set_lights)(struct hid *dev, uint8_t lights);

    uint8_t *report_buf;
    size_t   report_max_len;

    // See `REPEAT_STATE_*`
    uint8_t repeat_state;
    // Key that may repeat
    uint8_t       repeat_key;
    TimerHandle_t repeat_timer;

    // Device (e.g. boot keyboard, generic HID) data
    void *device_data;

    // --- Provided by host driver ---
    // HID SET_REPORT request
    void (*set_report)(
        struct hid *dev,
        uint8_t     report_type,
        uint8_t     report_id,
        uint8_t    *report,
        size_t      report_len);

    // Host (e.g. USB, BT) data
    void *host_data;

    struct hid *prev, *next;
};

struct hid_event {
    uint8_t event;
    void   *arg;
};

struct mouse_event {
    uint8_t bt;
    int16_t dx, dy;
    int8_t  dw;
    bool changed;
};

extern QueueHandle_t g_key_queue;
extern QueueHandle_t g_hid_event_queue;

extern uint8_t            g_cur_mouse_evt;
extern struct mouse_event g_mouse_evt[2];

void hid_init();
bool hid_device_init(struct hid *dev, uint8_t sub_class, uint8_t proto);
bool boot_kb_init(struct hid *dev);
bool boot_mouse_init(struct hid *dev);
