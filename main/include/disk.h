// SPDX-License-Identifier: GPL-3.0-only
#pragma once
#include <stdbool.h>
#include <stdlib.h>

#include <esp_err.h>

struct disk {
    // --- Provided by device driver ---
    char *name;

    uint64_t size;
    uint64_t sector_size;

    esp_err_t (*read)(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);
    esp_err_t (*write)(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);

    // --- Provided by disk subsystem ---
    struct volume *first_volume;
    struct volume *last_volume;
};

struct volume {
    char     label[17];
    char     name[17];
    uint64_t len; // in sectors
    uint64_t offset; // in sectors

    uint8_t drive;
    uint8_t mounts;

    struct disk   *disk;
    struct volume *prev, *next;
};

void           disk_init();
esp_err_t      disk_register(struct disk *disk);
void           disk_unregister(struct disk *disk);
void           volume_mount(struct volume *vol);
struct volume *volume_by_name(char *name);
struct volume *volume_by_label(char *name);
