// SPDX-License-Identifier: GPL-3.0-only

#include <usb/hid_host.h>
#include <usb/msc_host.h>

void usb_hid_handle_driver_event(
    hid_host_device_handle_t      hid_device_handle,
    const hid_host_driver_event_t event,
    void                         *arg);

void usb_msc_handle_driver_event(
    const msc_host_event_t event,
    void                   *arg);
