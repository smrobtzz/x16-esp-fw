// SPDX-License-Identifier: GPL-3.0-only
#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <smb2/libsmb2.h>
#include <smb2/smb2.h>
#include <time.h>

#include "defs.h"
#include "device.h"
#include "driver/gptimer_types.h"

#define DOS_STATUS_OK            0
#define DOS_STATUS_TIMEOUT_READ  (1 << 0)
#define DOS_STATUS_TIMEOUT_WRITE (1 << 1)
#define DOS_STATUS_EOF           (1 << 6)
#define DOS_STATUS_NOT_PRESENT   (1 << 7)

#define DOS_CHANNEL_LOAD 0
#define DOS_CHANNEL_SAVE 1
#define DOS_CHANNEL_CMD  15

#define DOS_OPEN_READ   (1 << 0)
#define DOS_OPEN_WRITE  (1 << 1)
#define DOS_OPEN_APPEND (1 << 2)
// File is stored entirely in RAM and may
// not exist in any filesystem
#define DOS_OPEN_RAM (1 << 3)
// File is included with the firmware and
// may not exist on any filesystem
#define DOS_OPEN_FW (1 << 4)

// http://cini.classiccmp.org/pdf/Commodore/CBM%20DOS%20Quick%20Ref.pdf
#define D_OK             0
#define D_SYNTAX_ERR     30
#define D_CONNECT_FAIL   35
#define D_INVALID_URL    36
#define D_FILE_NOT_FOUND 62
#define D_FILE_EXISTS    63
#define D_NOT_READY      74
#define D_NOT_MOUNTED    75

#define DOS_OK                                 \
    (dos_status_t)                             \
    {                                          \
        .code = D_OK, .extra1 = 0, .extra2 = 0 \
    }

#define DOS_ERR(e)                              \
    (dos_status_t)                              \
    {                                           \
        .code = D_##e, .extra1 = 0, .extra2 = 0 \
    }

typedef struct fs_t   fs_t;
typedef struct file_t file_t;
typedef struct dir_t  dir_t;

typedef struct {
    file_t *file;
    char   *name;

    uint8_t mode;

    uint8_t *buf;
    uint32_t bufpos;
    uint32_t buflen;

    uint32_t pos;
    uint32_t len;
} channel_t;

typedef struct {
    uint8_t code;
    uint8_t extra1;
    uint8_t extra2;
} dos_status_t;

typedef struct file_t {
    dos_status_t (*close)(file_t *file);
    dos_status_t (*read)(file_t *file, channel_t *chan);
    dos_status_t (*write)(file_t *file, uint8_t *buf, size_t len);
    dos_status_t (*seek)(file_t *file, uint32_t pos);

    uint32_t len;
} file_t;

#define DOS_DIRENT_FILE 0
#define DOS_DIRENT_DIR  1

typedef struct {
    const char     *name;
    char            type;
    uint64_t        sz;
    struct timespec mtime;
} dirent_t;

typedef struct dir_t {
    dirent_t *(*next)(dir_t *dir);
    void (*close)(dir_t *dir);
} dir_t;

typedef struct fs_t {
    dos_status_t (*open)(fs_t *fs, char *name, uint8_t mode, bool overwrite, file_t **out, uint32_t *len);
    dos_status_t (*opendir)(fs_t *fs, char *name, dir_t **out);
    void (*unmount)(fs_t *fs);
    const char *(*name)(fs_t *fs);
    const char *(*type)(fs_t *fs);
    dos_status_t (*delete)(fs_t *fs, char *name);
    dos_status_t (*stat)(fs_t *fs, char *path, dirent_t *out);
    dos_status_t (*mkdir)(fs_t *fs, char *path);
    dos_status_t (*rmdir)(fs_t *fs, char *path);
} fs_t;

#define LISTING_REG  0
#define LISTING_TIME 1
#define LISTING_LONG 2
#define LISTING_CWD  3

#define FILTER_ANY 0
#define FILTER_PRG 1
#define FILTER_DIR 2

typedef struct {
    file_t  file;
    dir_t  *dir;
    uint8_t kind;
    uint8_t filter;
    char   *name;
    bool    should_dot_dot;
    bool    done;
} listing_file_t;

// An imaginary directory containing directories
// corresponding to each component of the
// cwd path. Only used when using the "$=C"
// directory listing form.
//
// `cwd` is a copy of the cwd path that lives until
// the directory is closed.
typedef struct {
    dir_t dir;
    dirent_t dirent;
    char    *cwd;
    size_t   cwd_pos;
    bool     done;
} cwd_dir_t;

typedef struct dos_t dos_t;

struct vdev {
    channel_t *chans;
    channel_t *cur_chan;
    uint8_t    cur_chan_idx;

    dos_t *dos;
    fs_t  *fs;

    char           cwd[255];
    size_t         cwd_len;
    listing_file_t listing;
    cwd_dir_t      cwd_dir;
    char           pathbuf[256];
};

typedef struct dos_t {
    struct device_iface i;
    uint8_t             talking;
    uint8_t             listening;
    bool                opening;
    struct vdev         devices[4];
} dos_t;

struct device_iface *dos_init();
void                 dos_exec(dos_t *dos);
dos_status_t         dos_from_errno(int errn);
void                 dos_cmd_mount(dos_t *, struct vdev *);
void                 dos_cmd_unmount(dos_t *dos, struct vdev *vdev);

dos_status_t smb_fs_mount(fs_t **out_fs, char *uri);
dos_status_t vfs_fs_mount(fs_t **out_fs, char *vfs_name);

struct vdev vdev_new(dos_t *dos);
void        vdev_start(struct vdev *vdev);
void        vdev_stop(struct vdev *vdev);
void        vdev_chan_open(struct vdev *);
void        vdev_chan_close(struct vdev *vdev);
void        vdev_chan_read(struct vdev *vdev);
void        vdev_set_status(struct vdev *, dos_status_t);
