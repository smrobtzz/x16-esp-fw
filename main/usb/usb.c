// SPDX-License-Identifier: GPL-3.0-only
#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>

#include <esp_log.h>
#include <usb/hid_host.h>
#include <usb/msc_host.h>
#include <usb/usb_host.h>

#include "defs.h"
#include "hid.h"
#include "usb.h"

static QueueHandle_t s_usb_driver_event_queue = NULL;

void usb_task2(void *arg)
{
    struct usb_driver_event driver_event;
    while (true) {
        if (xQueueReceive(s_usb_driver_event_queue, &driver_event, portMAX_DELAY) == pdTRUE) {
            if (driver_event.event == HID_DRIVER_EVENT) {
                usb_hid_handle_driver_event(driver_event.hid.handle, driver_event.hid.event, driver_event.arg);
            } else if (driver_event.event == MSC_DRIVER_EVENT) {
                usb_msc_handle_driver_event(driver_event.msc, arg);
            }
        }
    }
}

static void hid_queue_driver_event(
    hid_host_device_handle_t      hid_device_handle,
    const hid_host_driver_event_t event,
    void                         *arg)
{
    struct usb_driver_event driver_event = {
        .event = HID_DRIVER_EVENT,
        .hid = {
            .handle = hid_device_handle,
            .event = event,
        },
        .arg = arg
    };
    xQueueSend(s_usb_driver_event_queue, &driver_event, 0);
}

static void msc_queue_driver_event(
    const msc_host_event_t *event,
    void                   *arg)
{
    struct usb_driver_event driver_event = {
        .event = MSC_DRIVER_EVENT,
        .msc = *event,
        .arg = arg
    };
    xQueueSend(s_usb_driver_event_queue, &driver_event, 0);
}

void usb_task(void *arg)
{
    int res = ESP_ERR_NO_MEM;
    s_usb_driver_event_queue = xQueueCreate(8, sizeof(struct usb_driver_event));
    if (s_usb_driver_event_queue == NULL)
        goto fail;

    xTaskCreate(usb_task2, "USB2", 8192, NULL, 2, NULL);

    usb_host_config_t usb_host_config = {
        .skip_phy_setup = false,
        .intr_flags = ESP_INTR_FLAG_LEVEL1,
        .enum_filter_cb = NULL
    };
    res = usb_host_install(&usb_host_config);
    if (res != ESP_OK)
        goto fail;

    hid_host_driver_config_t hid_host_config = {
        .create_background_task = true,
        .task_priority = 3,
        .stack_size = 4096,
        .core_id = tskNO_AFFINITY,
        .callback = hid_queue_driver_event
    };
    res = hid_host_install(&hid_host_config);
    if (res != ESP_OK)
        LOG(usb, "failed to initialize HID host: %s", esp_err_to_name(res));

    msc_host_driver_config_t msc_host_config = {
        // "backround"
        .create_backround_task = true,
        .task_priority = 2,
        .stack_size = 4096,
        .core_id = tskNO_AFFINITY,
        .callback = msc_queue_driver_event
    };
    res = msc_host_install(&msc_host_config);
    if (res != ESP_OK)
        LOG(usb, "failed to initialize MSC host: %s", esp_err_to_name(res));

    while (true) {
        uint32_t event;
        usb_host_lib_handle_events(portMAX_DELAY, &event);
    }

fail:
    LOG(usb, "failed to initialize: %s", esp_err_to_name(res));
    vTaskDelete(NULL);
}
