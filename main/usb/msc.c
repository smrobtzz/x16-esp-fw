// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include <esp_private/msc_scsi_bot.h>
#include <usb/msc_host.h>

#include "defs.h"
#include "disk.h"

esp_err_t msc_disk_read(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);
esp_err_t msc_disk_write(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count);

struct msc_disk {
    struct disk              disk;
    msc_host_device_handle_t device;
};

static struct msc_disk *s_msc_disk = NULL;

void usb_msc_handle_driver_event(
    const msc_host_event_t event,
    void                   *arg)
{
    esp_err_t res;

    switch (event.event) {
    case MSC_DEVICE_CONNECTED: {
        struct msc_disk       *disk;
        msc_host_device_info_t info;

        if (s_msc_disk != NULL)
            return;

        disk = malloc(sizeof(struct msc_disk));
        if (disk == NULL) {
            LOG(usb, "failed to allocate msc_disk");
            return;
        }

        res = msc_host_install_device(event.device.address, &disk->device);
        if (res != ESP_OK) {
            LOG(usb, "failed to install msc device: %s", esp_err_to_name(res));
            free(disk);
            return;
        }

        res = msc_host_get_device_info(disk->device, &info);
        if (res != ESP_OK) {
            LOG(usb, "failed to get msc device info: %s", esp_err_to_name(res));
            msc_host_uninstall_device(disk->device);
            free(disk);
            return;
        }

        disk->disk.name = "usb0";
        disk->disk.size = info.sector_count;
        disk->disk.sector_size = info.sector_size;
        disk->disk.read = msc_disk_read;
        disk->disk.write = msc_disk_write;
        res = disk_register((struct disk *)disk);
        if (res != ESP_OK) {
            LOG(usb, "failed to register msc disk: %s", esp_err_to_name(res));
            msc_host_uninstall_device(disk->device);
            free(disk);
            return;
        }

        s_msc_disk = disk;
    } break;
    case MSC_DEVICE_DISCONNECTED:
        if (s_msc_disk != NULL && event.device.handle == s_msc_disk->device) {
            msc_host_uninstall_device(s_msc_disk->device);
            disk_unregister((struct disk *)s_msc_disk);
            s_msc_disk = NULL;
        }
        break;
    }
}

esp_err_t msc_disk_read(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count)
{
    struct msc_disk *msc_disk = (struct msc_disk *)disk;
    return scsi_cmd_read10(msc_disk->device, buf, sector, count, disk->sector_size);
}

esp_err_t msc_disk_write(struct disk *disk, uint8_t *buf, uint32_t sector, uint32_t count)
{
    struct msc_disk *msc_disk = (struct msc_disk *)disk;
    return scsi_cmd_write10(msc_disk->device, buf, sector, count, disk->sector_size);
}
