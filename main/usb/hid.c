// SPDX-License-Identifier: GPL-3.0-only
#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>

#include <esp_log.h>
#include <usb/hid_host.h>
#include <usb/usb_host.h>

#include "defs.h"
#include "hid.h"

static void usb_hid_handle_interface_event(
    hid_host_device_handle_t         hid_device_handle,
    const hid_host_interface_event_t event,
    void                            *arg)
{
    struct hid *dev = arg;
    size_t      report_len = 0;

    switch (event) {
    case HID_HOST_INTERFACE_EVENT_INPUT_REPORT:
        if (hid_host_device_get_raw_input_report_data(
                hid_device_handle,
                dev->report_buf,
                dev->report_max_len,
                &report_len)
            != ESP_OK)
            return;
        dev->handle_report(dev, report_len);
        break;
    case HID_HOST_INTERFACE_EVENT_DISCONNECTED: {
        struct hid_event hid_event = {
            .event = HID_EVENT_UNREGISTER_DEVICE,
            .arg = arg,
        };
        LOG_IF(usb, "device disconnected");

        hid_host_device_close(hid_device_handle);
        dev->remove(dev);
        xQueueSend(g_hid_event_queue, &hid_event, 0);
    } break;
    case HID_HOST_INTERFACE_EVENT_TRANSFER_ERROR:
        break;
    default:
        break;
    }
}

static void usb_hid_set_report(
    struct hid *dev,
    uint8_t     report_type,
    uint8_t     report_id,
    uint8_t    *report,
    size_t      report_len)
{
    hid_class_request_set_report(dev->host_data, report_type, report_id, report, report_len);
}

void usb_hid_handle_driver_event(
    hid_host_device_handle_t      hid_dev_handle,
    const hid_host_driver_event_t event,
    void                         *arg)
{
    hid_host_dev_params_t dev_params;
    hid_host_device_get_params(hid_dev_handle, &dev_params);

    switch (event) {
    case HID_HOST_DRIVER_EVENT_CONNECTED: {
        LOG_IF(usb, "device connected, interface %u, subclass %u, proto %u",
            dev_params.iface_num, dev_params.sub_class, dev_params.proto);

        struct hid *dev = malloc(sizeof(struct hid));
        if (dev == NULL) {
            LOG(usb, "Failed to allocate hid_device");
            return;
        }

        *dev = (struct hid) {
            .set_report = usb_hid_set_report,
            .host_data = hid_dev_handle
        };

        if (hid_device_init(dev, dev_params.sub_class, dev_params.proto)) {
            hid_host_device_config_t dev_config = {
                .callback = usb_hid_handle_interface_event,
                .callback_arg = dev,
            };
            hid_host_device_open(hid_dev_handle, &dev_config);
            hid_class_request_set_protocol(hid_dev_handle, HID_REPORT_PROTOCOL_BOOT);
            hid_host_device_start(hid_dev_handle);

            struct hid_event hid_event = {
                .event = HID_EVENT_REGISTER_DEVICE,
                .arg = dev,
            };
            xQueueSend(g_hid_event_queue, &hid_event, 0);
        } else {
            free(dev);
        }
        break;
    }
    default:
        break;
    }
}
