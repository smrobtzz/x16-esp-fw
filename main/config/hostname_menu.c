// SPDX-License-Identifier: GPL-3.0-only

#include "config.h"
#include "defs.h"

void config_hostname_menu(struct cli *cli)
{
    uint16_t c;
    size_t   hostname_len = 0;
    char     hostname[29];
    memset(hostname, 0, 29);

    cli_cls(cli);
    cli_draw_box(cli, 15, 9, 50, 5);

    cli_goto(cli, 36, 9);
    cli_print(cli, "Hostname");

    cli_goto(cli, 16, 10);
    cli_print(cli, "Current hostname is: ");
    cli_print(cli, g_config.hostname);
    cli_goto(cli, 16, 12);
    cli_print(cli, "Enter new hostname: ");

    cli_goto(cli, 15, 14);
    cli_reverse_on(cli);
    cli_print(cli, "ENTER");
    cli_reverse_off(cli);
    cli_print(cli, " to confirm and return to main menu");

    cli_goto(cli, 15, 15);
    cli_reverse_on(cli);
    cli_print(cli, "CTRL");
    cli_reverse_off(cli);
    cli_print(cli, "-");
    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to return to main menu");

    cli_goto(cli, 36, 12);
    while (cli_getc(cli, &c)) {
        switch (c) {
        case K_CTRL_X:
            return;
        case K_BS:
            if (hostname_len != 0) {
                hostname[hostname_len - 1] = 0;
                hostname_len--;
                dprintf(cli->fd, "\e[D \e[D");
            }
            break;
        case K_ENTER:
            strcpy(g_config.hostname, hostname);
            mdns_hostname_set(hostname);
            return;
        default:
            if (hostname_len != 28) {
                hostname[hostname_len] = (char)c;
                hostname_len++;
                write(cli->fd, &c, 1);
            }
            break;
        }
    }
}
