// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include "config.h"
#include "defs.h"
#include "hid.h"

void config_key_repeat_menu(struct cli *cli)
{
    uint16_t c;
    uint8_t  sel = 0;
    size_t   delay_len;
    char     delay[5];
    size_t   interval_len;
    char     interval[5];
    size_t  *cur_len = &delay_len;
    char    *cur_text = delay;

    cli_cls(cli);
    cli_draw_box(cli, 15, 9, 50, 5);

    cli_goto(cli, 35, 9);
    cli_print(cli, "Key Repeat");

    cli_goto(cli, 16, 10);
    cli_print(cli, "Repeat Delay (ms): ");
    delay_len = snprintf(delay, 5, "%lu", g_config.repeat_delay);
    cli_print(cli, delay);

    cli_goto(cli, 16, 12);
    cli_print(cli, "Repeat Interval (ms): ");
    interval_len = snprintf(interval, 5, "%lu", g_config.repeat_interval);
    cli_print(cli, interval);

    cli_goto(cli, 15, 14);
    cli_reverse_on(cli);
    cli_print(cli, "TAB");
    cli_reverse_off(cli);
    cli_print(cli, " to select delay/interval");

    cli_goto(cli, 15, 15);
    cli_reverse_on(cli);
    cli_print(cli, "CTRL");
    cli_reverse_off(cli);
    cli_print(cli, "-");
    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to return to main menu");

    cli_goto(cli, 35 + delay_len, 10);
    while (cli_getc(cli, &c)) {
        switch (c) {
        case K_CTRL_X:
            g_config.repeat_delay = pdMS_TO_TICKS(strtol(delay, NULL, 10));
            if (g_config.repeat_delay == 0)
                g_config.repeat_delay = DEFAULT_REPEAT_DELAY;

            g_config.repeat_interval = pdMS_TO_TICKS(strtol(interval, NULL, 10));
            if (g_config.repeat_interval == 0)
                g_config.repeat_interval = DEFAULT_REPEAT_INTERVAL;

            return;
        case K_BS:
            if (*cur_len != 0) {
                cur_text[*cur_len - 1] = 0;
                (*cur_len)--;
                dprintf(cli->fd, "\e[D \e[D");
            }
            break;
        case K_TAB:
            if (sel == 0) {
                sel = 1;
                cli_goto(cli, 38 + interval_len, 12);
                cur_text = interval;
                cur_len = &interval_len;
            } else {
                sel = 0;
                cli_goto(cli, 35 + delay_len, 10);
                cur_text = delay;
                cur_len = &delay_len;
            }
            break;
        default:
            if (*cur_len != 4 && c >= 0x30 && c <= 0x39) {
                cur_text[*cur_len] = (char)c;
                (*cur_len)++;
                write(cli->fd, &c, 1);
            }
            break;
        }
    }
}
