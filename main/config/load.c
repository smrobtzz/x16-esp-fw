// SPDX-License-Identifier: GPL-3.0-only
#include <string.h>

#include <esp_log.h>
#include <nvs.h>

#include "config.h"
#include "defs.h"
#include "hid.h"
#include "wifi.h"

void config_nvs_load(nvs_handle_t nvs)
{
    esp_err_t res;
    uint32_t  tmp;
    size_t    len;
    char      buf[14];
    strcpy(buf, "wifi_ap0_");

    res = nvs_get_u8(nvs, "wifi_ap_cnt", &g_config.ap_cnt);
    if (res == ESP_OK) {
        for (int i = 0; i < g_config.ap_cnt; i++) {
            struct wifi_ap *ap = &g_config.known_aps[i];

            strcpy(buf + 9, "ssid");
            len = 33;
            res = nvs_get_str(nvs, buf, ap->ssid, &len);
            if (res != ESP_OK)
                LOG(config, "config_nvs_load nvs_get_str(ssid) fail: %s", esp_err_to_name(res));

            strcpy(buf + 9, "pass");
            len = 64;
            nvs_get_str(nvs, buf, ap->password, &len);

            strcpy(buf + 9, "mac");
            len = 6;
            nvs_get_blob(nvs, buf, ap->mac, &len);

            ap->valid = true;

            buf[7]++; // wifi_ap0 -> wifi_ap1
        }
    } else {
        LOG(config, "nvs_get_u8 for wifi_ap_cnt failed: %s", esp_err_to_name(res));
    }

    len = 65;
    res = nvs_get_str(nvs, "hostname", g_config.hostname, &len);
    if (res != ESP_OK)
        strcpy(g_config.hostname, CONFIG_X_NET_NAME);

    res = nvs_get_u32(nvs, "log", &g_config.log.val);
    if (res != ESP_OK)
        g_config.log.val = 0;

    res = nvs_get_u32(nvs, "repeat_delay", &tmp);
    if (res == ESP_OK) {
        g_config.repeat_delay = pdMS_TO_TICKS(tmp);
    } else {
        g_config.repeat_delay = DEFAULT_REPEAT_DELAY;
    }

    res = nvs_get_u32(nvs, "repeat_interval", &tmp);
    if (res == ESP_OK) {
        g_config.repeat_interval = pdMS_TO_TICKS(tmp);
    } else {
        g_config.repeat_interval = DEFAULT_REPEAT_INTERVAL;
    }

    strcpy(buf, "dos8_volume");
    for (int i = 0; i < 4; i++) {
        len = 32;
        buf[3] = '8' + i;
        res = nvs_get_str(nvs, buf, g_config.dos_volumes[i], &len);
        if (res != ESP_OK) {
            strcpy(g_config.dos_volumes[i], "vol:sd0p0");
        }
    }

    len = 32;
    res = nvs_get_str(nvs, "main_volume", g_config.main_volume, &len);
    if (res != ESP_OK) {
        strcpy(g_config.main_volume, "sd0p0");
    }
}

void config_load()
{
    nvs_handle_t nvs;
    esp_err_t    res;

    res = nvs_open("calypso", NVS_READWRITE, &nvs);
    if (res != ESP_OK) {
        LOG(config, "nvs_open failed: %s", esp_err_to_name(res));
        return;
    }

    config_nvs_load(nvs);
    nvs_close(nvs);
}
