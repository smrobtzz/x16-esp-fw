// SPDX-License-Identifier: GPL-3.0-only
#include <string.h>
#include <sys/unistd.h>

#include "config.h"
#include "defs.h"
#include "esp_log.h"

void cli_println(struct cli *cli, char *str)
{
    write(cli->fd, str, strlen(str));
    write(cli->fd, "\n", 1);
    cli->x = 0;
    cli->y += 1;
}

void cli_print(struct cli *cli, char *str)
{
    unsigned len = strlen(str);
    write(cli->fd, str, len);
    cli_wrote(cli, len);
}

void cli_goto_line(struct cli *cli, uint8_t line)
{
    dprintf(cli->fd, "\e[%u;1H", line + 1);
}

void cli_reverse_on(struct cli *cli)
{
    // black on white text
    dprintf(cli->fd, "\e[30m\e[107m");
}

void cli_reverse_off(struct cli *cli)
{
    // white on black text
    dprintf(cli->fd, "\e[97m\e[40m");
}

void cli_menu_option(struct cli *cli, char *opt, bool sel)
{
    if (sel)
        cli_reverse_on(cli);

    dprintf(cli->fd, " %s\n", opt);
    cli->y += 1;

    if (sel)
        cli_reverse_off(cli);
}

void cli_cls(struct cli *cli)
{
    dprintf(cli->fd, "\e[2J\e[1;1H");
    cli->x = 0;
    cli->y = 0;
}

void cli_putc(struct cli *cli, char c)
{
    write(cli->fd, &c, 1);
    cli_wrote(cli, 1);
}

void cli_putc_many(struct cli *cli, char c, unsigned cnt)
{
    char buf[80];
    assert(cnt <= 80);
    if (cnt == 0)
        return;

    memset(buf, c, cnt);
    write(cli->fd, buf, cnt);
    cli_wrote(cli, cnt);
}

void cli_wrote(struct cli *cli, unsigned cnt)
{
    cli->x += cnt;
    /*if (cli->x >= 80) {
        write(cli->fd, "\n", 1);
        cli->x -= 80;
        cli->y += 1;
    }*/
}

// false = error occurred
// true = character received
bool cli_getc(struct cli *cli, uint16_t *c)
{
    char b;

    if (read(cli->fd, &b, 1) <= 0)
        return false;

    if (b != '\e') {
        *c = b;
        return true;
    }

    if (read(cli->fd, &b, 1) <= 0)
        return false;

    if (b != '[')
        return true;

    if (read(cli->fd, &b, 1) <= 0)
        return false;

    switch (b) {
    case 'A':
        *c = K_CURS_UP;
        break;
    case 'B':
        *c = K_CURS_DOWN;
        break;
    case 'C':
        *c = K_CURS_RIGHT;
        break;
    case 'D':
        *c = K_CURS_LEFT;
        break;
    }
    return true;
}

// false = timeout expired
// true = should try cli_getc
bool cli_waitin(struct cli *cli, unsigned ms)
{
    struct timeval tim = {
        .tv_sec = 0,
        .tv_usec = ms * 1000
    };
    fd_set fds_rd = cli->fds_rd;
    return select(cli->fd + 1, &fds_rd, NULL, NULL, &tim) != 0;
}

void cli_goto(struct cli *cli, uint8_t x, uint8_t y)
{
    dprintf(cli->fd, "\e[%u;%uH", y + 1, x + 1);
}

void utf8_fill(char *buf, char *chr, size_t cnt)
{
    size_t chrlen = strlen(chr);
    for (size_t i = 0; i < cnt; i++) {
        for (size_t j = 0; j < chrlen; j++) {
            buf[i * chrlen + j] = chr[j];
        }
    }
}

void cli_draw_box(struct cli *cli, uint8_t x, uint8_t y, uint8_t width, uint8_t height)
{
    static char buf[256];
    if (cli->cp437) {
        cli_goto(cli, x, y);
        buf[0] = 0xd5; // top left corner
        memset(buf + 1, 0xcd, width - 2); // horizontal line
        buf[width - 1] = 0xb8; // top right corner
        write(cli->fd, buf, width);

        buf[0] = 0xb3; // vertical line
        memset(buf + 1, 0x20, width - 2); // space
        buf[width - 1] = 0xb3; // vertical line
        for (uint8_t i = 0; i < height - 2; i++) {
            cli_goto(cli, x, y + 1 + i);
            write(cli->fd, buf, width);
        }

        cli_goto(cli, x, y + height - 1);
        buf[0] = 0xd4; // bottom left corner
        memset(buf + 1, 0xcd, width - 2); // horizontal line
        buf[width - 1] = 0xbe; // bottom right corner
        write(cli->fd, buf, width);
    } else {
        cli_goto(cli, x, y);
        utf8_fill(buf, "╒", 1);
        utf8_fill(buf + 3, "═", width - 2);
        utf8_fill(buf + 3 + 3 * (width - 2), "╕", 1);
        write(cli->fd, buf, width * 3);

        utf8_fill(buf, "│", 1);
        memset(buf + 3, ' ', width - 2);
        utf8_fill(buf + 3 + width - 2, "│", 1);
        for (uint8_t i = 0; i < height - 2; i++) {
            cli_goto(cli, x, y + 1 + i);
            write(cli->fd, buf, 6 + width - 2);
        }

        cli_goto(cli, x, y + height - 1);
        utf8_fill(buf, "╘", 1);
        utf8_fill(buf + 3, "═", width - 2);
        utf8_fill(buf + 3 + 3 * (width - 2), "╛", 1);
        write(cli->fd, buf, width * 3);
    }
}

void cli_draw_divider(struct cli *cli, uint8_t x, uint8_t y, uint8_t width)
{
    static char buf[256];
    if (cli->cp437) {
        cli_goto(cli, x, y);
        buf[0] = 0xc6; // vertical single, right double
        memset(buf + 1, 0xcd, width - 2); // horizontal
        buf[width - 1] = 0xb5; // vertical single, left double
        write(cli->fd, buf, width);
    } else {
        cli_goto(cli, x, y);
        utf8_fill(buf, "╞", 1);
        utf8_fill(buf + 3, "═", width - 2);
        utf8_fill(buf + 3 + 3 * (width - 2), "╡", 1);
        write(cli->fd, buf, width * 3);
    }
}
