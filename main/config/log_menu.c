// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include "config.h"
#include "defs.h"
#include "wifi.h"

#define NUM_LOG_OPTIONS    (sizeof(LOG_OPTIONS) / sizeof(LOG_OPTIONS[0]))
#define LONGEST_LOG_OPTION 34
#define MENU_BOX_HEIGHT    (NUM_LOG_OPTIONS + 2)
#define MENU_BOX_WIDTH     (LONGEST_LOG_OPTION + 4)
#define MENU_BOX_X         (80 - MENU_BOX_WIDTH) / 2
#define MENU_BOX_Y         (25 - MENU_BOX_HEIGHT) / 2

static char *LOG_OPTIONS[] = {
    "DOS (advanced)",
    "DOS (simple)",
    "WebDAV",
    "Flash",
    "I2C",
    "USB",
    "SD",
    "Reset",
    "Net",
    "WiFi (includes SSID and password!)"
};

void draw_menu_option(struct cli *cli, unsigned opt, bool sel)
{
    cli_goto(cli, MENU_BOX_X + 1, MENU_BOX_Y + 1 + opt);

    if (sel)
        cli_reverse_on(cli);

    cli_print(cli, LOG_OPTIONS[opt]);
    cli_putc_many(cli, ' ', LONGEST_LOG_OPTION - strlen(LOG_OPTIONS[opt] + 1));
    if ((g_config.log.val & (1 << opt)) != 0) {
        if (cli->cp437) {
            cli_putc(cli, 0xfe);
        } else {
            cli_print(cli, "■");
        }
    } else {
        cli_putc(cli, ' ');
    }

    if (sel)
        cli_reverse_off(cli);
}

void draw_all_menu_options(struct cli *cli)
{
    for (unsigned i = 0; i < NUM_LOG_OPTIONS; i++) {
        draw_menu_option(cli, i, false);
    }
}

void config_log_menu(struct cli *cli)
{
    uint16_t c;
    unsigned menu_opt = 0;

    cli_cls(cli);
    cli_draw_box(
        cli,
        MENU_BOX_X,
        MENU_BOX_Y,
        MENU_BOX_WIDTH,
        MENU_BOX_HEIGHT);
    cli_goto(cli, (80 - 15) / 2, MENU_BOX_Y);
    cli_print(cli, "Logging Options");

    draw_all_menu_options(cli);
    draw_menu_option(cli, 0, true);

    cli_goto(cli, MENU_BOX_X, MENU_BOX_Y + MENU_BOX_HEIGHT);
    cli_reverse_on(cli);
    cli_print(cli, "UP");
    cli_reverse_off(cli);
    cli_print(cli, "/");
    cli_reverse_on(cli);
    cli_print(cli, "DOWN");
    cli_reverse_off(cli);
    cli_print(cli, " to select option");

    cli_goto(cli, MENU_BOX_X, MENU_BOX_Y + MENU_BOX_HEIGHT + 1);
    cli_reverse_on(cli);
    cli_print(cli, "SPACE");
    cli_reverse_off(cli);
    cli_print(cli, " to toggle option");

    cli_goto(cli, MENU_BOX_X, MENU_BOX_Y + MENU_BOX_HEIGHT + 2);
    cli_reverse_on(cli);
    cli_print(cli, "E");
    cli_reverse_off(cli);
    cli_print(cli, " to enable all options");

    cli_goto(cli, MENU_BOX_X, MENU_BOX_Y + MENU_BOX_HEIGHT + 3);
    cli_reverse_on(cli);
    cli_print(cli, "D");
    cli_reverse_off(cli);
    cli_print(cli, " to disable all options");

    cli_goto(cli, MENU_BOX_X, MENU_BOX_Y + MENU_BOX_HEIGHT + 4);
    cli_reverse_on(cli);
    cli_print(cli, "CTRL");
    cli_reverse_off(cli);
    cli_print(cli, "-");
    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to return to main menu");

    while (cli_getc(cli, &c)) {
        switch (c) {
        case K_CURS_UP:
            if (menu_opt > 0) {
                draw_menu_option(cli, menu_opt - 1, true);
                draw_menu_option(cli, menu_opt, false);
                menu_opt -= 1;
            }
            break;
        case K_CURS_DOWN:
            if (menu_opt < NUM_LOG_OPTIONS - 1) {
                draw_menu_option(cli, menu_opt, false);
                draw_menu_option(cli, menu_opt + 1, true);
                menu_opt += 1;
            }
            break;
        case ' ':
            g_config.log.val ^= (1 << menu_opt);
            draw_menu_option(cli, menu_opt, true);
            break;
        case 'e':
            g_config.log.val = 0xFFFFFFFF;
            draw_all_menu_options(cli);
            draw_menu_option(cli, menu_opt, true);
            break;
        case 'd':
            g_config.log.val = 0;
            draw_all_menu_options(cli);
            draw_menu_option(cli, menu_opt, true);
            break;
        case K_CTRL_X:
            return;
        default:
            break;
        }
    }
}
