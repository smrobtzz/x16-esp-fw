// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include "config.h"
#include "defs.h"

#define NUM_MENU_OPTS 5

char *main_menu_opts[] = {
    "WiFi                  ",
    "Hostname              ",
    "Logging               ",
    "Key Repeat            ",
//    "DOS Devices           ",
    "Save Settings to Flash",
};

static void draw_main_menu(struct cli *cli)
{
    char buf[24];

    cli_reverse_off(cli);
    cli_cls(cli);

    cli_println(cli, "Calypso Config Utility");

    cli_print(cli, "Firmware Version: ");
    cli_print(cli, CONFIG_X_FW_VERSION);
    cli_putc(cli, '\n');

    cli_print(cli, "SSID: ");
    cli_print(cli, g_config.known_aps[g_config.cur_ap].ssid);
    cli_putc(cli, '\n');

    snprintf(buf, 24, "IP: " IPSTR, IP2STR(&g_config.ip));
    cli_println(cli, buf);

    cli_print(cli, "Hostname: ");
    cli_print(cli, g_config.hostname);
    cli_putc(cli, '\n');

    cli_println(cli, "");

    cli_menu_option(cli, main_menu_opts[0], true);
    for (int i = 1; i < sizeof(main_menu_opts) / sizeof(main_menu_opts[0]); i++) {
        cli_menu_option(cli, main_menu_opts[i], false);
    }
}

bool config_main_menu(struct cli *cli)
{
    unsigned      menu_opt = 0;
    const uint8_t menu_opt_line_start = 6;
    uint16_t      c;
    nvs_handle_t  nvs;
    esp_err_t     res;

    draw_main_menu(cli);

    while (cli_getc(cli, &c)) {
        switch (c) {
        case K_CURS_UP:
            if (menu_opt > 0) {
                cli_goto_line(cli, menu_opt_line_start + menu_opt - 1);
                cli_menu_option(cli, main_menu_opts[menu_opt - 1], true);
                cli_menu_option(cli, main_menu_opts[menu_opt], false);
                menu_opt -= 1;
            }
            break;
        case K_CURS_DOWN:
            if (menu_opt < NUM_MENU_OPTS - 1) {
                cli_goto_line(cli, menu_opt_line_start + menu_opt);
                cli_menu_option(cli, main_menu_opts[menu_opt], false);
                cli_menu_option(cli, main_menu_opts[menu_opt + 1], true);
                menu_opt += 1;
            }
            break;
        case K_ENTER:
            switch (menu_opt) {
            case 0:
                config_wifi_menu(cli);
                break;
            case 1:
                config_hostname_menu(cli);
                break;
            case 2:
                config_log_menu(cli);
                break;
            case 3:
                config_key_repeat_menu(cli);
                break;
            case 4:
                if ((res = nvs_open("calypso", NVS_READWRITE, &nvs)) != ESP_OK) {
                    LOG(config, "nvs_open failed: %s", esp_err_to_name(res));
                    continue;
                }
                config_nvs_update(nvs);
                nvs_close(nvs);
                break;
            default:
                break;
            }
            draw_main_menu(cli);
            menu_opt = 0;
            break;
        default:
            LOG(config, "chr %u", (unsigned)c);
            break;
        }
    }
    return false;
}
