// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>

#include "config.h"
#include "defs.h"
#include "wifi.h"

#define KNOWN_NETS_CONTENT_HEIGHT 4
#define KNOWN_NETS_TOTAL_HEIGHT   KNOWN_NETS_CONTENT_HEIGHT + 4
#define AVAIL_NETS_CONTENT_HEIGHT 10
#define AVAIL_NETS_TOTAL_HEIGHT   AVAIL_NETS_CONTENT_HEIGHT + 4

static void connect_to(struct cli *cli, wifi_ap_record_t *ap);
static void draw_known_aps(struct cli *cli, uint16_t sel);
static void draw_known_networks(struct cli *cli);
static void draw_available_aps(struct cli *cli,
    wifi_ap_record_t                      *ap_found,
    uint16_t                               ap_num,
    uint16_t                               first,
    uint16_t                               new_first,
    uint16_t                               sel,
    uint16_t                               new_sel);
static void draw_avail_networks(
    struct cli       *cli,
    wifi_ap_record_t *ap_found,
    uint16_t          ap_num);
static void draw_help(struct cli *cli);
static void connect_fail(struct cli *cli, wifi_ap_record_t *ap);

// clang-format off
static char s_rowbuf[79] = {
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
    0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00
};
// clang-format on

void config_wifi_menu(struct cli *cli)
{
    wifi_ap_record_t *ap_found;
    uint16_t          ap_num;
    uint16_t          first, k_sel;
    uint16_t          c;
    uint8_t           box; // 0 = known, 1 = available

again:
    box = 1;
    first = 0;
    k_sel = 0;

    ap_found = wifi_scan(cli, &ap_num);
    if (ap_found == NULL)
        return;

    cli_cls(cli);

    draw_known_networks(cli);
    draw_avail_networks(cli, ap_found, ap_num);
    draw_help(cli);

    while (true) {
        if (!cli_waitin(cli, 500)) {
            continue;
        }
        if (!cli_getc(cli, &c))
            break;

        switch (c) {
        case 'x':
            if (box == 1)
                break;
            g_config.known_aps[0].valid = false;
            // fall through
        case K_TAB:
            if (box == 0) {
                draw_known_aps(cli, 9999);
                draw_available_aps(
                    cli, ap_found, ap_num, first, first, k_sel, k_sel);
                box = 1;
            } else if (g_config.known_aps[0].valid) {
                draw_known_aps(cli, 0);
                draw_available_aps(
                    cli, ap_found, ap_num, first, first, k_sel, 9999);
                box = 0;
            }
            break;
        case K_CURS_DOWN:
            if (box == 1 && k_sel < ap_num - 1) {
                if (k_sel == first + AVAIL_NETS_CONTENT_HEIGHT - 1) {
                    draw_available_aps(
                        cli, ap_found, ap_num, first, first + 1, k_sel, k_sel + 1);
                    first += 1;
                    k_sel += 1;
                } else {
                    draw_available_aps(
                        cli, ap_found, ap_num, first, first, k_sel, k_sel + 1);
                    k_sel += 1;
                }
            }
            break;
        case K_CURS_UP:
            if (box == 1 && k_sel > 0) {
                if (k_sel == first) {
                    draw_available_aps(
                        cli, ap_found, ap_num, first, first - 1, k_sel, k_sel - 1);
                    first -= 1;
                    k_sel -= 1;
                } else {
                    draw_available_aps(
                        cli, ap_found, ap_num, first, first, k_sel, k_sel - 1);
                    k_sel -= 1;
                }
            }
            break;
        case K_ENTER:
            if (box == 1) {
                connect_to(cli, ap_found + k_sel);
                draw_avail_networks(cli, ap_found, ap_num);
                draw_available_aps(
                    cli, ap_found, ap_num, 9999, first, k_sel, k_sel);
            }
            break;
        case 's':
            goto again;
        case K_CTRL_X:
            return;
        default:
            break;
        }
    }

    free(ap_found);
}

static void draw_known_aps(struct cli *cli, uint16_t sel)
{
    // Draw APs
    for (int i = 0; i < 4; i++) {
        struct wifi_ap *ap = &g_config.known_aps[i];
        cli_goto(cli, 1, 3 + i);
        if (ap->valid) {
            int cnt = snprintf(s_rowbuf, 78, "%-32s %02x:%02x:%02x:%02x:%02x:%02x   %s", ap->ssid,
                ap->mac[0], ap->mac[1], ap->mac[2],
                ap->mac[3], ap->mac[4], ap->mac[5],
                (g_config.cur_ap == i) ? "CONNECTED" : "NOT CONNECTED");
            s_rowbuf[cnt] = ' ';
            if (sel == i)
                cli_reverse_on(cli);
            cli_print(cli, s_rowbuf);
            if (sel == i)
                cli_reverse_off(cli);
        } else {
            cli_putc_many(cli, ' ', 78);
        }
    }
    memset(s_rowbuf, ' ', 78);
}

static void draw_known_networks(struct cli *cli)
{
    cli_draw_box(cli, 0, 0, 80, KNOWN_NETS_TOTAL_HEIGHT);

    // Draw top of "Known Networks" box
    cli_goto(cli, 33, 0);
    cli_print(cli, "Known Networks");

    // Draw column headers
    cli_goto(cli, 1, 1);
    cli_print(cli, "SSID                             MAC                 STATUS                   ");

    cli_draw_divider(cli, 0, 2, 80);

    draw_known_aps(cli, 9999);
}

static void draw_available_aps(
    struct cli       *cli,
    wifi_ap_record_t *ap_found,
    uint16_t          ap_num,
    uint16_t          first,
    uint16_t          new_first,
    uint16_t          sel,
    uint16_t          new_sel)
{
    char     buf[256];
    uint16_t redraw_ap_start = (sel > new_sel) ? new_sel : sel;
    uint16_t redraw_ap_cnt = 2;
    uint8_t  first_line = 0;

    if (first != new_first) {
        // Draw top of box
        // Up arrows shown instead of dashes if there are more APs above the current
        cli_goto(cli, 1, KNOWN_NETS_TOTAL_HEIGHT + 2);
        if (new_first > 0) {
            if (cli->cp437) {
                cli_putc_many(cli, 0x1e, 78); // up arrow * 78
            } else {
                utf8_fill(buf, "▲", 78);
                write(cli->fd, buf, 78 * 3);
            }
        } else {
            if (cli->cp437) {
                cli_putc_many(cli, 0xcd, 78); // hor line * 78
            } else {
                utf8_fill(buf, "═", 78);
                write(cli->fd, buf, 78 * 3);
            }
        }

        // Draw bottom of box
        cli_goto(cli, 1, KNOWN_NETS_TOTAL_HEIGHT + AVAIL_NETS_TOTAL_HEIGHT - 1);
        if (new_first + AVAIL_NETS_CONTENT_HEIGHT < ap_num) {
            if (cli->cp437) {
                cli_putc_many(cli, 0x1f, 78); // down arrow * 78
            } else {
                utf8_fill(buf, "▼", 78);
                write(cli->fd, buf, 78 * 3);
            }
        } else {
            if (cli->cp437) {
                cli_putc_many(cli, 0xcd, 78); // hor line * 78
            } else {
                utf8_fill(buf, "═", 78);
                write(cli->fd, buf, 78 * 3);
            }
        }

        redraw_ap_start = new_first;
        redraw_ap_cnt = (ap_num > 10) ? 10 : ap_num;
    }

    // Draw APs
    first_line = KNOWN_NETS_TOTAL_HEIGHT + 3 + redraw_ap_start - new_first;
    for (uint16_t i = redraw_ap_start; i < redraw_ap_start + redraw_ap_cnt; i++) {
        wifi_ap_record_t *ap = &ap_found[i];
        int               cnt = snprintf(s_rowbuf, 78, "%-32s %02x:%02x:%02x:%02x:%02x:%02x   %d", ap->ssid,
                          ap->bssid[0], ap->bssid[1], ap->bssid[2],
                          ap->bssid[3], ap->bssid[4], ap->bssid[5],
                          ap->rssi);
        cli_goto(cli, 1, first_line + i - redraw_ap_start);
        s_rowbuf[cnt] = ' ';
        if (new_sel == i)
            cli_reverse_on(cli);
        cli_print(cli, s_rowbuf);
        if (new_sel == i)
            cli_reverse_off(cli);
    }

    memset(s_rowbuf, ' ', 78);
}

static void draw_avail_networks(
    struct cli       *cli,
    wifi_ap_record_t *ap_found,
    uint16_t          ap_num)
{
    cli_draw_box(cli, 0, KNOWN_NETS_TOTAL_HEIGHT, 80, AVAIL_NETS_TOTAL_HEIGHT);

    // Draw top of "Available Networks" box
    cli_goto(cli, 31, KNOWN_NETS_TOTAL_HEIGHT);
    cli_print(cli, "Available Networks");

    // Draw column headers
    cli_goto(cli, 1, KNOWN_NETS_TOTAL_HEIGHT + 1);
    cli_print(cli, "SSID                             MAC                 RSSI                     ");

    cli_draw_divider(cli, 0, KNOWN_NETS_TOTAL_HEIGHT + 2, 80);

    draw_available_aps(
        cli, ap_found, ap_num, 1, 0, 0, 0);
}

static void draw_password_box(struct cli *cli, wifi_ap_record_t *ap)
{
    cli_draw_box(cli, 0, 10, 80, 6);

    cli_goto(cli, 2, 11);
    cli_print(cli, "Password for ");
    cli_print(cli, (char *)ap->ssid);
    cli_putc(cli, ':');

    cli_goto(cli, 2, 14);
    cli_print(cli, "Press ");
    cli_reverse_on(cli);
    cli_print(cli, "CTRL");
    cli_reverse_off(cli);
    cli_print(cli, "-");
    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to cancel");
}

static void connect_to(struct cli *cli, wifi_ap_record_t *ap)
{
    uint32_t msg;
    uint16_t c;
    size_t   password_len = 0;
    char     password[64];
    memset(password, 0, 64);

    draw_password_box(cli, ap);
    cli_goto(cli, 2, 12);
    while (cli_getc(cli, &c)) {
        switch (c) {
        case K_CTRL_X:
            return;
        case K_ENTER:
            ulTaskNotifyValueClearIndexed(NULL, CONFIG_MAILBOX, 0xffffffff);
            wifi_configure((char *)ap->ssid, password);

            msg = ulTaskNotifyTakeIndexed(CONFIG_MAILBOX, pdTRUE, portMAX_DELAY);
            if (msg == CONFIG_MSG_WIFI_SUCC) {
                struct wifi_ap *known_ap = &g_config.known_aps[0];
                memcpy(known_ap->ssid, ap->ssid, 33);
                memcpy(known_ap->mac, ap->bssid, 6);
                memcpy(known_ap->password, password, 64);
                known_ap->valid = true;

                draw_known_aps(cli, 9999);

                return;
            } else if (msg == CONFIG_MSG_WIFI_FAIL) {
                connect_fail(cli, ap);
                draw_password_box(cli, ap);
                memset(password, 0, 64);
                password_len = 0;
                cli_goto(cli, 2, 12);
            }

            break;
        case K_BS:
            if (password_len != 0) {
                password[password_len - 1] = 0;
                password_len--;
                dprintf(cli->fd, "\e[D \e[D");
            }
            break;
        default:
            if (password_len != 63) {
                password[password_len] = (char)c;
                password_len++;
                write(cli->fd, &c, 1);
            }
            break;
        }
    }
}

static void connect_fail(struct cli *cli, wifi_ap_record_t *ap)
{
    uint16_t c;
    cli_draw_box(cli, 20, 11, 40, 4);

    cli_goto(cli, 22, 12);
    cli_print(cli, "Connection failed");
    cli_goto(cli, 22, 13);
    cli_print(cli, "Press any key to continue...");

    cli_getc(cli, &c);
}

static void draw_help(struct cli *cli)
{
    cli_goto(cli, 0, KNOWN_NETS_TOTAL_HEIGHT + AVAIL_NETS_TOTAL_HEIGHT);
    cli_reverse_on(cli);
    cli_print(cli, "TAB");
    cli_reverse_off(cli);
    cli_print(cli, " to switch between known/available networks\n");

    cli_reverse_on(cli);
    cli_print(cli, "UP");
    cli_reverse_off(cli);
    cli_print(cli, "/");
    cli_reverse_on(cli);
    cli_print(cli, "DOWN");
    cli_reverse_off(cli);
    cli_print(cli, " to select network\n");

    cli_reverse_on(cli);
    cli_print(cli, "ENTER");
    cli_reverse_off(cli);
    cli_print(cli, " to connect to network\n");

    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to forget known network\n");

    cli_reverse_on(cli);
    cli_print(cli, "S");
    cli_reverse_off(cli);
    cli_print(cli, " to scan for available networks\n");

    cli_reverse_on(cli);
    cli_print(cli, "CTRL");
    cli_reverse_off(cli);
    cli_print(cli, "-");
    cli_reverse_on(cli);
    cli_print(cli, "X");
    cli_reverse_off(cli);
    cli_print(cli, " to return to main menu\n");
}
