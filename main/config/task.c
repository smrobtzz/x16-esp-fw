// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>
#include <nvs.h>

#include <sys/param.h>
#include <sys/select.h>
#include <sys/socket.h>

#include "config.h"
#include "defs.h"
#include "hid.h"
#include "sd.h"
#include "wifi.h"

/*
 * NVS Contents
 * u32 log                - log options bitmap
 * string hostname        - hostname for mDNS
 * u8 wifi_ap_cnt         - number of saved wifi networks
 * string wifi_ap<n>_ssid - ssid of wifi ap `n`
 * blob wifi_ap<n>_mac    - mac of wifi ap `n`
 * string wifi_ap<n>_pass - password of wifi ap `n`
 */

void config_task(void *arg)
{
    struct sockaddr_in serv_addr;
    struct cli         cli;
    socklen_t          cli_addr_len = sizeof(cli._addr);
    fd_set             serv_fds;
    int                utf8_serv_fd, cp437_serv_fd, max_fd;
    int                en = 1;

    // Loading configuration from NVS is done in `app_main`,
    // but these functions may block for a while, so do them
    // here.
    if (g_config.known_aps[0].valid) {
        wifi_configure(g_config.known_aps[0].ssid, g_config.known_aps[0].password);
    }
    mdns_hostname_set(g_config.hostname);

    utf8_serv_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    cp437_serv_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (utf8_serv_fd == -1 || cp437_serv_fd == -1) {
        LOG(config, "failed to open config server socket: %d", errno);
        close(utf8_serv_fd);
        close(cp437_serv_fd);
        vTaskDelete(NULL);
    }
    max_fd = MAX(utf8_serv_fd, cp437_serv_fd);

    if (setsockopt(utf8_serv_fd, SOL_SOCKET, SO_REUSEADDR, &en, sizeof(en)) == -1
        || setsockopt(cp437_serv_fd, SOL_SOCKET, SO_REUSEADDR, &en, sizeof(en)) == -1) {
        LOG(config, "failed to set SO_REUSEADDR on config server socket: %d", errno);
        close(utf8_serv_fd);
        close(cp437_serv_fd);
        vTaskDelete(NULL);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(22);
    if (bind(utf8_serv_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        LOG(config, "failed to bind config server utf8 socket: %d", errno);
        close(utf8_serv_fd);
        close(cp437_serv_fd);
        vTaskDelete(NULL);
    }
    serv_addr.sin_port = htons(23);
    if (bind(cp437_serv_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        LOG(config, "failed to bind config server cp437 socket: %d", errno);
        close(utf8_serv_fd);
        close(cp437_serv_fd);
        vTaskDelete(NULL);
    }

    if (listen(utf8_serv_fd, 1) == -1
        || listen(cp437_serv_fd, 1) == -1) {
        LOG(config, "failed to listen on config server socket: %d", errno);
        close(utf8_serv_fd);
        close(cp437_serv_fd);
        vTaskDelete(NULL);
    }

    FD_ZERO(&cli.fds_rd);

    while (true) {
        FD_ZERO(&serv_fds);
        FD_SET(utf8_serv_fd, &serv_fds);
        FD_SET(cp437_serv_fd, &serv_fds);

        if (select(max_fd + 1, &serv_fds, NULL, NULL, NULL) <= 0)
            continue;

        if (FD_ISSET(utf8_serv_fd, &serv_fds)) {
            LOG(config, "config in utf8 mode");
            cli.cp437 = false;
            cli.fd = accept(utf8_serv_fd, (struct sockaddr *)&cli._addr, &cli_addr_len);
        } else if (FD_ISSET(cp437_serv_fd, &serv_fds)) {
            LOG(config, "config in cp437 mode");
            cli.cp437 = true;
            cli.fd = accept(cp437_serv_fd, (struct sockaddr *)&cli._addr, &cli_addr_len);
        }

        FD_SET(cli.fd, &cli.fds_rd);
        setsockopt(cli.fd, IPPROTO_TCP, TCP_NODELAY, &en, sizeof(en));
        cli.addr = (struct sockaddr *)&cli._addr;

        config_handle_client(&cli);

        FD_CLR(cli.fd, &cli.fds_rd);
        close(cli.fd);
    }
}

void config_handle_client(struct cli *cli)
{
    char buf[40] = { 0 };

    if (cli->addr->sa_family == AF_INET) {
        struct in_addr *ip = &(((struct sockaddr_in *)cli->addr)->sin_addr);
        inet_ntop(AF_INET, ip, buf, 40);
    } else if (cli->addr->sa_family == AF_INET6) {
        struct in6_addr *ip6 = &(((struct sockaddr_in6 *)cli->addr)->sin6_addr);
        inet_ntop(AF_INET6, ip6, buf, 40);
    }
    LOG(config, "accepted client: %s", buf);

    while (config_main_menu(cli)) { }
}

void config_nvs_update_wifi(nvs_handle_t nvs)
{
    esp_err_t res;
    char      buf[14] = "wifi_ap0_";
    uint8_t   wifi_ap_cnt = 0;

    for (int i = 0; i < 4 && g_config.known_aps[i].valid; i++) {
        struct wifi_ap *ap = &g_config.known_aps[i];

        strcpy(buf + 9, "ssid");
        res = nvs_set_str(nvs, buf, ap->ssid);
        if (res != ESP_OK)
            goto nvs_fail;

        strcpy(buf + 9, "pass");
        res = nvs_set_str(nvs, buf, ap->password);
        if (res != ESP_OK)
            goto nvs_fail;

        strcpy(buf + 9, "mac");
        res = nvs_set_blob(nvs, buf, ap->mac, 6);
        if (res != ESP_OK)
            goto nvs_fail;

        buf[7]++; // wifi_ap0 -> wifi_ap1
        wifi_ap_cnt++;
    }

    res = nvs_set_u8(nvs, "wifi_ap_cnt", wifi_ap_cnt);
    if (res != ESP_OK)
        goto nvs_fail;

    return;
nvs_fail:
    LOG(config, "nvs operation failed during config_nvs_update_wifi: %s", esp_err_to_name(res));
}

void config_nvs_update_hostname(nvs_handle_t nvs)
{
    esp_err_t res;

    res = nvs_set_str(nvs, "hostname", g_config.hostname);
    if (res != ESP_OK)
        LOG(config, "nvs operation failed during config_nvs_update_hostname: %s", esp_err_to_name(res));
}

void config_nvs_update_log(nvs_handle_t nvs)
{
    esp_err_t res;

    res = nvs_set_u32(nvs, "log", g_config.log.val);
    if (res != ESP_OK)
        LOG(config, "nvs operation failed during config_nvs_update_log: %s", esp_err_to_name(res));
}

void config_nvs_update_repeat(nvs_handle_t nvs)
{
    esp_err_t res;

    res = nvs_set_u32(nvs, "repeat_delay", pdTICKS_TO_MS(g_config.repeat_delay));
    if (res != ESP_OK)
        goto fail;

    res = nvs_set_u32(nvs, "repeat_interval", pdTICKS_TO_MS(g_config.repeat_interval));
    if (res != ESP_OK)
        goto fail;

    return;
fail:
    LOG(config, "nvs operation failed during config_nvs_update_repeat: %s", esp_err_to_name(res));
}

void config_nvs_update(nvs_handle_t nvs)
{
    esp_err_t res;

    config_nvs_update_wifi(nvs);
    config_nvs_update_hostname(nvs);
    config_nvs_update_log(nvs);
    config_nvs_update_repeat(nvs);

    res = nvs_commit(nvs);
    if (res != ESP_OK)
        LOG(config, "nvs_commit failed: %s", esp_err_to_name(res));
}
