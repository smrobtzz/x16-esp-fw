// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>

#include "config.h"
#include "defs.h"

wifi_ap_record_t *wifi_scan(struct cli *cli, uint16_t *ap_num)
{
    wifi_ap_record_t *ap_found;
    esp_err_t         res;

    cli_cls(cli);
    cli_println(cli, "Scanning...");

    if (!g_wifi_started) {
        res = esp_wifi_start();
        if (res != ESP_OK) {
            LOG(config, "failed to start WiFi: %s", esp_err_to_name(res));
        }
        g_wifi_started = true;
    }

    res = esp_wifi_scan_start(NULL, true);
    if (res != ESP_OK) {
        LOG(config, "failed to start WiFi scan: %s", esp_err_to_name(res));
        return NULL;
    }

    res = esp_wifi_scan_stop();
    if (res != ESP_OK)
        LOG(config, "failed to stop WiFi scan: %s", esp_err_to_name(res));

    res = esp_wifi_scan_get_ap_num(ap_num);
    if (res != ESP_OK)
        LOG(config, "failed to get ap_num: %s", esp_err_to_name(res));

    if (*ap_num == 0)
        LOG(config, "wifi_scan ap_num = 0");

    ap_found = calloc(*ap_num, sizeof(wifi_ap_record_t));
    if (ap_found == NULL) {
        LOG(config, "failed to allocate wifi_ap_record_t array");
        esp_wifi_clear_ap_list();
        return NULL;
    }

    esp_wifi_scan_get_ap_records(ap_num, ap_found);

    return ap_found;
}
