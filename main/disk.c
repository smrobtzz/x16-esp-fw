// SPDX-License-Identifier: GPL-3.0-only
#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include <esp_err.h>
#include <esp_log.h>
#include <esp_vfs_fat.h>

#include <diskio_impl.h>
#include <ff.h>

#include "defs.h"
#include "disk.h"

DSTATUS ff_volume_init(uint8_t);
DSTATUS ff_volume_status(uint8_t);
DRESULT ff_volume_read(uint8_t, uint8_t *, uint32_t, unsigned);
DRESULT ff_volume_write(uint8_t, const uint8_t *, uint32_t, unsigned);
DRESULT ff_volume_ioctl(uint8_t, uint8_t, void *);

SemaphoreHandle_t       g_volumes_lock;
static struct volume   *s_vol_list = NULL;
static struct volume   *s_mounted_volumes[9];
static ff_diskio_impl_t s_volume_diskio = {
    .init = ff_volume_init,
    .status = ff_volume_status,
    .read = ff_volume_read,
    .write = ff_volume_write,
    .ioctl = ff_volume_ioctl
};

static void disk_free_volumes(struct disk *disk);
static void volume_unmount(struct volume *vol, bool always);

void disk_init()
{
    g_volumes_lock = xSemaphoreCreateMutex();
    memset(s_mounted_volumes, 0, sizeof(s_mounted_volumes));
}

// Register a disk, adding its volumes to `s_vol_list`
esp_err_t disk_register(struct disk *disk)
{
    esp_err_t res = ESP_OK;
    uint8_t  *mbr, *bpb;

    mbr = malloc(disk->sector_size);
    if (mbr == NULL)
        return ESP_ERR_NO_MEM;

    bpb = malloc(disk->sector_size);
    if (bpb == NULL) {
        free(mbr);
        return ESP_ERR_NO_MEM;
    }

    // Read MBR
    if ((res = disk->read(disk, mbr, 0, 1)) != ESP_OK)
        goto done;

    // Check for signature 0xaa55
    if (mbr[510] != 0x55 && mbr[511] != 0xaa) {
        res = ESP_ERR_INVALID_ARG;
        goto done;
    }

    // Read partitions
    disk->first_volume = NULL;
    disk->last_volume = NULL;
    for (int i = 0; i < 4; i++) {
        uint8_t *part = mbr + 446 + 16 * i;
        if ((part[0] < 0x80 && part[0] != 0x00) || part[4] == 0)
            continue;

        uint32_t offset = *(uint32_t *)(part + 0x8);
        uint32_t len = *(uint32_t *)(part + 0xc);

        if ((res = disk->read(disk, bpb, offset, 1)) != ESP_OK)
            goto done;

        struct volume *volume = malloc(sizeof(struct volume));
        if (volume == NULL) {
            res = ESP_ERR_NO_MEM;
            disk_free_volumes(disk);
            goto done;
        }

        memcpy(volume->label, bpb + 0x47, 11);
        volume->label[11] = 0;
        snprintf(volume->name, 17, "%sp%u", disk->name, i);
        volume->len = len;
        volume->offset = offset;
        volume->disk = disk;
        volume->next = NULL;
        volume->mounts = 0;
        volume->prev = disk->last_volume;
        LOG(disk, "registered volume %s", volume->name);

        if (disk->first_volume == NULL)
            disk->first_volume = volume;
        if (disk->last_volume != NULL)
            disk->last_volume->next = volume;
        disk->last_volume = volume;
    }

    if (disk->last_volume != NULL) {
        xSemaphoreTake(g_volumes_lock, portMAX_DELAY);
        disk->last_volume->next = s_vol_list;
        s_vol_list = disk->first_volume;
        xSemaphoreGive(g_volumes_lock);
    }

done:
    free(bpb);
    free(mbr);
    return res;
}

// Unmount the volumes belonging to a disk and free them.
// Unless called while this disk is being registered,
// `g_volumes_lock` must be held
static void disk_free_volumes(struct disk *disk)
{
    if (disk->first_volume == NULL)
        return;

    if (s_vol_list == disk->first_volume) {
        s_vol_list = disk->last_volume->next;
    }

    if (disk->first_volume->prev != NULL) {
        disk->first_volume->prev->next = disk->last_volume->next;
    }

    if (disk->last_volume->next != NULL) {
        disk->last_volume->next->prev = disk->first_volume->prev;
    }

    disk->last_volume->next = NULL;

    struct volume *vol = disk->first_volume;
    while (vol != NULL) {
        struct volume *next = vol->next;
        volume_unmount(vol, true);
        free(vol);
        vol = next;
    }
}

// Unmount and free owned volumes, and delete the disk
void disk_unregister(struct disk *disk)
{
    xSemaphoreTake(g_volumes_lock, portMAX_DELAY);
    disk_free_volumes(disk);
    free(disk->name);
    free(disk);
    xSemaphoreGive(g_volumes_lock);
}

// `g_volumes_lock` must be locked
uint8_t next_drive()
{
    for (uint8_t i = 0; i < 9; i++) {
        if (s_mounted_volumes[i] == NULL) {
            return i;
        }
    }

    return 0xff;
}

// `g_volumes_lock` must be locked
void volume_mount(struct volume *vol)
{
    esp_err_t res;
    uint8_t   drive;
    FATFS    *ff;
    char      fatfs_name[3] = "0:";
    char      vfs_path[18] = "/";
    memcpy(vfs_path + 1, vol->name, 17);

    if (vol->mounts++ != 0)
        return;

    drive = next_drive();
    if (drive == 0xFF) {
        LOG(disk, "volume mount failed: maximum number of volumes already mounted");
        return;
    }
    vol->drive = drive;
    fatfs_name[0] = vol->drive + '0';
    s_mounted_volumes[drive] = vol;

    LOG(disk, "vfs_path: %s, fatfs_name: %s", vfs_path, fatfs_name);
    res = esp_vfs_fat_register(vfs_path, fatfs_name, 16, &ff);
    if (res != ESP_OK)
        goto fail1;

    ff_diskio_register(drive, &s_volume_diskio);
    res = f_mount(ff, fatfs_name, 1);
    if (res != FR_OK)
        goto fail2;

    return;
fail2:
    ff_diskio_register(drive, NULL);
    esp_vfs_fat_unregister_path(vfs_path);
fail1:
    LOG(disk, "volume_mount failed: %s", esp_err_to_name(res));
}

// `g_volumes_lock` must be locked
static void volume_unmount(struct volume *vol, bool always)
{
    char *fatfs_name = "0:";
    fatfs_name[0] = vol->drive + '0';
    char vfs_path[18] = "/";
    memcpy(vfs_path + 1, vol->name, 17);

    if ((vol->mounts == 0) || (--vol->mounts != 0 && !always))
        return;

    f_mount(NULL, fatfs_name, 1);
    ff_diskio_register(vol->drive, NULL);
    esp_vfs_fat_unregister_path(vfs_path);
}

// `g_volumes_lock` must be locked
// return value is valid until `g_volumes_lock` is unlocked
struct volume *volume_by_label(char *name)
{
    struct volume *vol;

    vol = s_vol_list;
    while (vol != NULL) {
        if (strcmp(vol->label, name) == 0)
            break;
        vol = vol->next;
    }

    return vol;
}

// `g_volumes_lock` must be locked
// return value is valid until `g_volumes_lock` is unlocked
struct volume *volume_by_name(char *name)
{
    struct volume *vol;

    vol = s_vol_list;
    while (vol != NULL) {
        if (strcasecmp(vol->name, name) == 0)
            break;
        vol = vol->next;
    }

    return vol;
}

DSTATUS ff_volume_init(uint8_t pdrv)
{
    return 0;
}

DSTATUS ff_volume_status(uint8_t pdrv)
{
    return 0;
}

DRESULT ff_volume_read(uint8_t pdrv, uint8_t *buf, uint32_t sector, unsigned count)
{
    struct volume *vol = s_mounted_volumes[pdrv];
    esp_err_t      res = vol->disk->read(vol->disk, buf, vol->offset + sector, count);
    return (res == ESP_OK) ? RES_OK : RES_ERROR;
}

DRESULT ff_volume_write(uint8_t pdrv, const uint8_t *buf, uint32_t sector, unsigned count)
{
    struct volume *vol = s_mounted_volumes[pdrv];
    esp_err_t      res = vol->disk->write(vol->disk, (uint8_t *)buf, vol->offset + sector, count);
    return (res == ESP_OK) ? RES_OK : RES_ERROR;
}

DRESULT ff_volume_ioctl(uint8_t pdrv, uint8_t cmd, void *buf)
{
    switch (cmd) {
    case CTRL_SYNC:
        return RES_OK;
    case GET_SECTOR_COUNT:
        *((uint32_t *)buf) = s_mounted_volumes[pdrv]->len;
        return RES_OK;
    case GET_SECTOR_SIZE:
        *((uint16_t *)buf) = s_mounted_volumes[pdrv]->disk->sector_size;
        return RES_OK;
    default:
        return RES_ERROR;
    }
}
