// SPDX-License-Identifier: GPL-3.0-only
#include <stdint.h>

#include <driver/gpio.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>

#include "defs.h"
#include "hid.h"

static void boot_kb_remove(struct hid *dev);
static void boot_kb_handle_report(
    struct hid *dev,
    size_t      report_len);
static void boot_kb_set_lights(struct hid *dev, uint8_t lights);

struct boot_keyboard_data {
    uint8_t keys[6];
    uint8_t mods;
    uint8_t report_buf[8];
};

// Mapping from HID key presses to X16 key presses
const uint8_t KEY_PRESS_CODE[0xe8] = {
    0, 0, 0, 0, 0x1f, 0x32, 0x30, 0x21, 0x13, 0x22, 0x23, 0x24, 0x18, 0x25, 0x26, 0x27, 0x34,
    0x33, 0x19, 0x1a, 0x11, 0x14, 0x20, 0x15, 0x17, 0x31, 0x12, 0x2f, 0x16, 0x2e, 0x2, 0x3,
    0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0x2b, 0x6e, 0xf, 0x10, 0x3d, 0xc, 0xd, 0x1b, 0x1c,
    0x1d, 0, 0x28, 0x29, 0x1, 0x35, 0x36, 0x37, 0x1e, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76,
    0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x4b, 0x50, 0x55, 0x4c, 0x51, 0x56, 0x59,
    0x4f, 0x54, 0x53, 0x5a, 0x5f, 0x64, 0x69, 0x57, 0x6c, 0x5d, 0x62, 0x67, 0x5c, 0x61, 0x66,
    0x5b, 0x60, 0x65, 0x63, 0x68,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0x3a, 0x2c, 0x3c, 0, 0x40, 0x39, 0x3e, 0
};

// Mapping from X16 lights format (0:scroll, 1:num, 2:caps)
// to HID boot keyboard lights format (0:num, 1:caps, 2:scroll)
const uint8_t KB_LIGHTS_MAP[] = {
    0b000, 0b100, 0b001, 0b101, 0b010, 0b110, 0b011, 0b111
};

static void key_press(uint8_t key)
{
    if (key > 0xe7)
        return;
    uint8_t tl_key = KEY_PRESS_CODE[key];
    if (tl_key != 0)
        xQueueSend(g_key_queue, &tl_key, 3);
}

static void key_release(uint8_t key)
{
    if (key > 0xe7)
        return;
    uint8_t tl_key = KEY_PRESS_CODE[key] + 0x80;
    if (tl_key != 0x80)
        xQueueSend(g_key_queue, &tl_key, 3);
}

static void key_timer_callback(TimerHandle_t timer)
{
    struct hid *dev = pvTimerGetTimerID(timer);

    if (dev->repeat_state == REPEAT_STATE_DELAY) {
        dev->repeat_state = REPEAT_STATE_REPEAT;
        xTimerChangePeriod(dev->repeat_timer, g_config.repeat_interval, portMAX_DELAY);
        xTimerStart(dev->repeat_timer, portMAX_DELAY);
    } else if (dev->repeat_state == REPEAT_STATE_REPEAT) {
        xTimerStart(dev->repeat_timer, portMAX_DELAY);
        key_press(dev->repeat_key);
    }
}

bool boot_kb_init(struct hid *dev)
{
    struct boot_keyboard_data *bkd = malloc(sizeof(struct boot_keyboard_data));
    if (bkd == NULL)
        return false;

    memset(bkd, 0, sizeof(struct boot_keyboard_data));
    dev->remove = boot_kb_remove;
    dev->handle_report = boot_kb_handle_report;
    dev->set_lights = boot_kb_set_lights;
    dev->report_buf = bkd->report_buf;
    dev->report_max_len = 8;
    dev->repeat_state = REPEAT_STATE_NONE;
    dev->repeat_timer = xTimerCreate("", 1, pdFALSE, NULL, key_timer_callback);
    dev->device_data = bkd;
    if (dev->repeat_timer == NULL) {
        free(bkd);
        return false;
    }
    vTimerSetTimerID(dev->repeat_timer, dev);

    return true;
}

static void boot_kb_remove(
    struct hid *dev)
{
    struct boot_keyboard_data *bkd = dev->device_data;

    xTimerStop(dev->repeat_timer, portMAX_DELAY);
    xTimerDelete(dev->repeat_timer, portMAX_DELAY);

    for (int i = 0; i < 6; i++) {
        if (bkd->keys[i] != 0) {
            key_release(bkd->keys[i]);
        }
    }

    for (int i = 0; i < 8; i++) {
        if ((bkd->mods & (1 << i)) != 0) {
            key_release(0xe0 + i);
        }
    }
}

static void boot_kb_handle_report(
    struct hid *dev,
    size_t      report_len)
{
    struct boot_keyboard_data *bkd = dev->device_data;

    if (dev->report_buf[0] != bkd->mods) {
        uint8_t chgd_mods = dev->report_buf[0] ^ bkd->mods;
        for (int i = 0; i < 8; i++) {
            if ((chgd_mods & (1 << i)) == 0)
                continue;

            if (dev->report_buf[0] & (1 << i)) {
                key_press(0xe0 + i);
            } else {
                key_release(0xe0 + i);
            }
        }

        bkd->mods = dev->report_buf[0];
    }

    // Check for (new) key presses
    bool still_pressed[6] = { 0 };
    for (int i = 0; i < 6; i++) {
        uint8_t key = dev->report_buf[2 + i];
        bool    pressed = true;

        if (key == 0)
            continue;

        for (int j = 0; j < 6; j++) {
            if (key == bkd->keys[j]) {
                pressed = false;
                still_pressed[j] = true;
            }
        }

        if (pressed) {
            dev->repeat_key = key;
            dev->repeat_state = REPEAT_STATE_DELAY;
            xTimerChangePeriod(dev->repeat_timer, g_config.repeat_delay, portMAX_DELAY);
            xTimerStart(dev->repeat_timer, portMAX_DELAY);
            key_press(key);

            // DEL && ALT && CTRL
            if (key == 0x4c && ((dev->report_buf[0] & 0b01000100) != 0) && ((dev->report_buf[0] & 0b00010001) != 0))
                gpio_set_level(CONFIG_X_GPIO_RESET, 0);
        }
    }

    // Check for key releases
    for (int i = 0; i < 6; i++) {
        if (still_pressed[i] == false && bkd->keys[i] != 0) {
            key_release(bkd->keys[i]);
            if (dev->repeat_key == bkd->keys[i]) {
                dev->repeat_state = REPEAT_STATE_NONE;
                xTimerStop(dev->repeat_timer, portMAX_DELAY);
            }
        }
    }

    memcpy(bkd->keys, dev->report_buf + 2, 6);
}

static void boot_kb_set_lights(
    struct hid *dev,
    uint8_t     lights)
{
    dev->set_report(dev, 2, 0, (uint8_t *)(KB_LIGHTS_MAP + lights), 1);
}
