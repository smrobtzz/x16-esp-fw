// SPDX-License-Identifier: GPL-3.0-only
#include <stdint.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>

#include "defs.h"
#include "esp_log.h"
#include "hid.h"

static void boot_mouse_remove(struct hid *dev);
static void boot_mouse_handle_report(
    struct hid *dev,
    size_t      report_len);

static void boot_mouse_remove(struct hid *dev)
{
    memset(g_mouse_evt, 0, sizeof(struct mouse_event) * 2);
}

static void boot_mouse_handle_report(
    struct hid *dev,
    size_t      report_len)
{
    g_mouse_evt[g_cur_mouse_evt].bt = dev->report_buf[0] | (1 << 3);
    g_mouse_evt[g_cur_mouse_evt].dx += (int8_t)dev->report_buf[1];
    g_mouse_evt[g_cur_mouse_evt].dy += -(int8_t)dev->report_buf[2];
    if (report_len >= 4)
        g_mouse_evt[g_cur_mouse_evt].dw += -(int8_t)dev->report_buf[3];
    else
        g_mouse_evt[g_cur_mouse_evt].dw = 0;
    g_mouse_evt[g_cur_mouse_evt].changed = true;
}

bool boot_mouse_init(struct hid *dev)
{
    dev->remove = boot_mouse_remove;
    dev->handle_report = boot_mouse_handle_report;
    dev->set_lights = NULL;

    dev->report_buf = malloc(4);
    if (dev->report_buf == NULL)
        return false;

    dev->report_max_len = 4;

    return true;
}
