// SPDX-License-Identifier: GPL-3.0-only

#include <usb/hid_host.h>

#include "defs.h"
#include "hid.h"

static void hid_event_task(void *arg);

static struct hid *s_hid_list = NULL;

void hid_init()
{
    g_hid_event_queue = xQueueCreate(4, sizeof(struct hid_event));
    g_key_queue = xQueueCreate(128, 1);
    xTaskCreate(hid_event_task, "HID", 8192, NULL, 2, NULL);
}

static void hid_event_task(void *arg)
{
    struct hid_event event;

    while (true) {
        if (xQueueReceive(g_hid_event_queue, &event, portMAX_DELAY) != pdTRUE)
            continue;

        switch (event.event) {
        case HID_EVENT_REGISTER_DEVICE: {
            struct hid *dev = event.arg;

            if (s_hid_list != NULL) {
                s_hid_list->prev = dev;
            }

            dev->next = s_hid_list;
            s_hid_list = dev;
        } break;
        case HID_EVENT_UNREGISTER_DEVICE: {
            struct hid *dev = event.arg;

            if (dev->next != NULL) {
                dev->next->prev = dev->prev;
            }

            if (dev->prev != NULL) {
                dev->prev->next = dev->next;
            } else {
                s_hid_list = dev->next;
            }

            free(dev);
        } break;
        case HID_EVENT_SET_LIGHTS: {
            struct hid *dev = s_hid_list;
            uint8_t     lights = (uint8_t)(uint32_t)event.arg;

            while (dev != NULL) {
                if (dev->set_lights != NULL)
                    dev->set_lights(dev, lights);

                dev = dev->next;
            }
        } break;
        default:
            break;
        }
    }
}

bool hid_device_init(struct hid *dev, uint8_t sub_class, uint8_t proto)
{
    if (sub_class != HID_SUBCLASS_BOOT_INTERFACE)
        return false;

    if (proto == HID_PROTOCOL_KEYBOARD) {
        return boot_kb_init(dev);
    } else if (proto == HID_PROTOCOL_MOUSE) {
        return boot_mouse_init(dev);
    }

    return false;
}
