// SPDX-License-Identifier: GPL-3.0-only
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include <driver/gpio.h>
#include <esp_log.h>

#include "defs.h"
#include "disk.h"
#include "dos.h"

void         dos_iface_start(struct device_iface *i);
void         dos_iface_cmd(struct device_iface *i, uint8_t cmd, uint8_t *data);
void         dos_iface_stop(struct device_iface *i);
dos_status_t dos_mount(dos_t *dos, char *location, struct vdev *vdev);

static const char *DOS_CMD_NAMES[] = {
    "TALK", "TKSA", "UNTLK", "LISTN", "SECND", "UNLSN", "CIOUT", "ACPTR",
    "MCIOUT", "MACPTR"
};

struct device_iface *dos_init()
{
    dos_t *dos;

    dos = malloc(sizeof(dos_t));
    // clang-format off
    *dos = (dos_t) {
        .i = {
            .start = dos_iface_start,
            .cmd = dos_iface_cmd,
            .stop = dos_iface_stop,
            .cmd_start = DOS_CMD_START,
            .cmd_end = DOS_CMD_END
        },
        .talking = 0xff,
        .listening = 0xff,
        .opening = false,
        .devices = {
            vdev_new(dos),
            vdev_new(dos),
            vdev_new(dos),
            vdev_new(dos),
        }
    };
    dos->devices[0].cwd_dir.dirent.name = dos->devices[0].cwd;
    dos->devices[1].cwd_dir.dirent.name = dos->devices[1].cwd;
    dos->devices[2].cwd_dir.dirent.name = dos->devices[2].cwd;
    dos->devices[3].cwd_dir.dirent.name = dos->devices[3].cwd;
    // clang-format on

    return (struct device_iface *)dos;
}

void dos_iface_start(struct device_iface *i)
{
    dos_t *dos = (dos_t *)i;

    dos_mount(dos, g_config.dos_volumes[0], &dos->devices[0]);
    dos_mount(dos, g_config.dos_volumes[1], &dos->devices[1]);
    dos_mount(dos, g_config.dos_volumes[2], &dos->devices[2]);
    dos_mount(dos, g_config.dos_volumes[3], &dos->devices[3]);

    for (int i = 0; i < 4; i++) {
        vdev_start(dos->devices + i);
    }

    LOG_IF(dos1, "started");
}

void dos_iface_stop(struct device_iface *i)
{
    dos_t *dos = (dos_t *)i;

    for (int i = 0; i < 4; i++) {
        vdev_stop(dos->devices + i);
    }
    LOG_IF(dos1, "stopped");
}

void dos_iface_cmd(struct device_iface *i, uint8_t cmd, uint8_t *data)
{
    struct vdev *vdev;
    dos_t       *dos = (dos_t *)i;
    uint8_t      h, l;
    bool         error = false;
    uint16_t     len, cnt;
    uint16_t     outlen = 0, outpos = 0, left = 0;

    len = data[3];
    if (len == 0)
        len = 256;

    LOG_IF(dos0, "%s $%02x",
        (data[2] > 9) ? "UNKNOWN" : DOS_CMD_NAMES[data[2]],
        data[3]);

    switch (data[2]) {
    case DOS_CMD_TALK:
        dos->talking = data[3] - 8;
        break;
    case DOS_CMD_TKSA:
        l = data[3] & 0x0f;
        vdev = &dos->devices[dos->talking];
        vdev->cur_chan = vdev->chans + l;
        vdev->cur_chan_idx = l;
        break;
    case DOS_CMD_LISTN:
        dos->listening = data[3] - 8;
        break;
    case DOS_CMD_SECND: {
        h = data[3] & 0xf0;
        l = data[3] & 0x0f;
        vdev = &dos->devices[dos->listening];
        vdev->cur_chan = vdev->chans + l;
        vdev->cur_chan_idx = l;
        switch (h) {
        case 0x60: // SECOND
            if (vdev->cur_chan_idx == DOS_CHANNEL_CMD) {
                dos->opening = true;
            }
            break;
        case 0xE0: // CLOSE
            vdev_chan_close(vdev);
            break;
        case 0xF0: // OPEN
            dos->opening = true;
            vdev->cur_chan->pos = 0;
            vdev->cur_chan->bufpos = 0;
            break;
        }
        break;
    }
    case DOS_CMD_UNLSN:
        if (dos->listening != 0xff) {
            vdev = &dos->devices[dos->listening];
            if (dos->opening) {
                dos->opening = false;
                vdev->cur_chan->name[vdev->cur_chan->pos] = 0;

                vdev_chan_open(vdev);
                vdev->cur_chan->pos = 0;
            }
            dos->listening = 0xff;
        }
        break;
    case DOS_CMD_CIOUT:
        if (dos->listening != 0xff) {
            vdev = &dos->devices[dos->listening];
            if (dos->opening) {
                if (vdev->cur_chan->pos < 255)
                    vdev->cur_chan->name[vdev->cur_chan->pos++] = data[3];
            } else if (vdev->cur_chan->file != NULL) {
                vdev_set_status(vdev, vdev->cur_chan->file->write(vdev->cur_chan->file, data + 3, 1));
            }
        }
        break;
    case DOS_CMD_UNTLK:
        dos->talking = 0xff;
        break;
    case DOS_CMD_ACPTR:
        if (dos->talking != 0xff) {
            vdev = &dos->devices[dos->talking];
            if (vdev->cur_chan_idx == 15) {
                data[0] = vdev->cur_chan->buf[vdev->cur_chan->bufpos];
                vdev->cur_chan->bufpos++;

                // BASIC complains about "OUT OF DATA" when it reads '\r'
                // and EOI is set while doing INPUT#. I have no idea why.
                // Having one character ('\0') available past the '\r' before
                // setting EOI fixes the issue (??????).
                if (vdev->cur_chan->bufpos > vdev->cur_chan->buflen) {
                    vdev_set_status(vdev, DOS_OK);
                    data[1] = 0x40;
                    error = true;
                }
            } else if (vdev->cur_chan->mode & DOS_OPEN_READ
                && (vdev->cur_chan->file != NULL
                    || !vdev->listing.done
                    || vdev->cur_chan->mode & DOS_OPEN_RAM
                    || vdev->cur_chan->mode & DOS_OPEN_FW)) {
                //  End of buffer, so read more data
                if (vdev->cur_chan->bufpos == vdev->cur_chan->buflen)
                    vdev_chan_read(vdev);

                // In the file is 0 bytes long, no buf will be allocated,
                // so don't read anything.
                if (vdev->cur_chan->len != 0) {
                    data[0] = vdev->cur_chan->buf[vdev->cur_chan->bufpos];
                    vdev->cur_chan->bufpos++;
                    vdev->cur_chan->pos++;
                }

                // End of file, set EOI and close file
                if (vdev->cur_chan->pos == vdev->cur_chan->len && vdev->listing.done) {
                    vdev_chan_close(vdev);
                    data[1] = 0x40;
                    error = true;
                }
            } else {
                data[1] = 0x42;
                error = true;
                break;
            }
        } else {
            data[1] = 0x42;
            error = true;
            break;
        }
        break;
    case DOS_CMD_MACPTR:
        vdev = &dos->devices[dos->talking];
        if (dos->talking != 0xff && vdev->cur_chan->mode & DOS_OPEN_READ) {
            if (vdev->cur_chan->file == NULL && vdev->cur_chan_idx != 15) {
                data[len] = 0;
                data[len + 1] = 0;
                data[len + 2] = 0x42;
            }

            outlen = 0;
            outpos = 0;
            left = len;
            while (left != 0) {
                // End of buffer, so read more data
                if (vdev->cur_chan->bufpos == vdev->cur_chan->buflen && vdev->cur_chan_idx != 15)
                    vdev_chan_read(vdev);

                cnt = left;
                if (vdev->cur_chan->bufpos + left > vdev->cur_chan->buflen)
                    cnt = vdev->cur_chan->buflen - vdev->cur_chan->bufpos;

                if (vdev->cur_chan_idx == 15) {
                    // FIXME
                    abort();
                    if (vdev->cur_chan->bufpos >= vdev->cur_chan->buflen)
                        vdev_set_status(vdev, DOS_OK);

                    memcpy(data + outpos, vdev->cur_chan->buf + vdev->cur_chan->bufpos, cnt);
                } else {
                    memcpy(data + outpos, vdev->cur_chan->buf + vdev->cur_chan->bufpos, cnt);
                }

                vdev->cur_chan->bufpos += cnt;
                vdev->cur_chan->pos += cnt;
                outpos += cnt;
                outlen += cnt;
                left -= cnt;

                if (vdev->cur_chan->pos == vdev->cur_chan->len)
                    break;
            }
            data[len] = (uint8_t)outlen;
            data[len + 1] = (uint8_t)(outlen >> 8);

            if (vdev->cur_chan->pos == vdev->cur_chan->len) {
                // End of file, set EOI and close file
                vdev_chan_close(vdev);
                data[len + 2] = DOS_STATUS_EOF;
            } else {
                data[len + 2] = DOS_STATUS_OK;
            }
            error = true;
        } else {
            data[len] = 0;
            data[len + 1] = 0;
            data[len + 2] = 0x42;
        }
        break;
    case DOS_CMD_MCIOUT:
        vdev = &dos->devices[dos->listening];
        if (dos->listening != 0xff && vdev->cur_chan->file != NULL)
            vdev_set_status(vdev, vdev->cur_chan->file->write(vdev->cur_chan->file, data + 4, len));
        data[0] = (uint8_t)len;
        data[1] = (uint8_t)(len >> 8);
        data[2] = DOS_STATUS_OK;
        error = true;
        break;
    default:
        data[1] = DOS_STATUS_NOT_PRESENT;
        break;
    }

    if (!error) {
        data[1] = DOS_STATUS_OK;
    } else {
        LOG_IF(dos0, "-> $%02x", data[1]);
    }
}

/*  Supported mount locations:
 *    vol:sd0p<n>
 *    vol:usb<n>p<m>
 *    label:A DRIVE
 *    smb://someurl.com
 *    auto
 *    none
 */
dos_status_t dos_mount(dos_t *dos, char *location, struct vdev *vdev)
{
    dos_status_t   res;
    struct volume *vol;

    if (strncasecmp("vol:", location, 4) == 0) {
        xSemaphoreTake(g_volumes_lock, portMAX_DELAY);
        vol = volume_by_name(location + 4);
        if (vol != NULL) {
            volume_mount(vol);
            res = vfs_fs_mount(&vdev->fs, vol->name);
        } else {
            res = DOS_ERR(SYNTAX_ERR);
        }
        xSemaphoreGive(g_volumes_lock);
    } else if (strncasecmp("label:", location, 6) == 0) {
        xSemaphoreTake(g_volumes_lock, portMAX_DELAY);
        vol = volume_by_label(location + 6);
        if (vol != NULL) {
            volume_mount(vol);
            res = vfs_fs_mount(&vdev->fs, vol->label);
        } else {
            res = DOS_ERR(SYNTAX_ERR);
        }
        xSemaphoreGive(g_volumes_lock);
    } else if (strncasecmp("smb:", location, 4) == 0) {
        res = smb_fs_mount(&vdev->fs, location);
    } else if (strncasecmp("none", location, 4) == 0) {
        vdev->fs = NULL;
        res = DOS_OK;
    } else {
        res = DOS_ERR(SYNTAX_ERR);
    }

    return res;
}

void dos_cmd_mount(dos_t *dos, struct vdev *vdev)
{
    LOG(dos1, "mount \"%s\"", vdev->cur_chan->name + 4);
    vdev_set_status(vdev, dos_mount(dos, vdev->cur_chan->name + 4, vdev));
}

void dos_cmd_unmount(dos_t *dos, struct vdev *vdev)
{
    if (vdev->fs == NULL) {
        vdev_set_status(vdev, DOS_OK);
        return;
    }
    LOG_IF(dos1, "unmount");
    vdev->fs->unmount(vdev->fs);
    vdev->fs = NULL;
    vdev_set_status(vdev, DOS_OK);
}
