// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/param.h>
#include <unistd.h>

#include "defs.h"
#include "dos.h"

struct vfs_fs {
    fs_t  fs;
    char  name[17];
    char  path_buf[260];
    char *path_tmp;
};

typedef struct {
    file_t file;
    int    fd;
} vfs_file_t;

typedef struct {
    dir_t          dir;
    dirent_t       dirent;
    DIR           *d;
    char          *cwd;
    struct vfs_fs *fs;
} vfs_dir_t;

dos_status_t vfs_fs_open(fs_t *fs, char *name, uint8_t mode, bool overwrite, file_t **out, uint32_t *len);
dos_status_t vfs_fs_opendir(fs_t *fs, char *name, dir_t **out);
dos_status_t vfs_fs_err(fs_t *fs);
void         vfs_fs_unmount(fs_t *fs);
const char  *vfs_fs_name(fs_t *fs);
const char  *vfs_fs_type(fs_t *fs);
dos_status_t vfs_fs_delete(fs_t *fs, char *name);
dos_status_t vfs_fs_stat(fs_t *fs, char *path, dirent_t *out);
dos_status_t vfs_fs_mkdir(fs_t *fs, char *path);
dos_status_t vfs_fs_rmdir(fs_t *fs, char *path);
dos_status_t vfs_file_close(file_t *file);
dos_status_t vfs_file_read(file_t *file, channel_t *chan);
dos_status_t vfs_file_write(file_t *file, uint8_t *buf, size_t len);
dos_status_t vfs_file_seek(file_t *file, uint32_t pos);
dirent_t    *vfs_dir_next(dir_t *dir);
void         vfs_dir_close(dir_t *dir);

dos_status_t vfs_fs_mount(fs_t **out_fs, char *vfs_name)
{
    int            res;
    struct vfs_fs *fs = malloc(sizeof(struct vfs_fs));
    if (fs == NULL)
        return DOS_ERR(NOT_READY);

    *fs = (struct vfs_fs) {
        .fs = {
            .open = vfs_fs_open,
            .opendir = vfs_fs_opendir,
            .unmount = vfs_fs_unmount,
            .name = vfs_fs_name,
            .type = vfs_fs_type,
            .delete = vfs_fs_delete,
            .stat = vfs_fs_stat,
            .mkdir = vfs_fs_mkdir,
            .rmdir = vfs_fs_rmdir },
        .name = "",
    };
    res = snprintf(fs->path_buf, 260, "/%s/", vfs_name);
    fs->path_tmp = fs->path_buf + res;
    *out_fs = (fs_t *)fs;
    LOG(dos, "vfs_fs_mount path_buf = %s", fs->path_buf);

    return DOS_OK;
}

void vfs_fs_unmount(fs_t *fs)
{
}

char *vfs_get_name(struct vfs_fs *fs, char *name)
{
    size_t name_len = strlen(name);

    assert(name_len <= 255);

    if (name[0] == '/') {
        name_len--;
        name++;
    }

    memcpy(fs->path_tmp, name, name_len + 1);
    return fs->path_buf;
}

char *vfs_get_name2(struct vfs_fs *fs, char *prefix, char *name)
{
    size_t name_len = strlen(name);
    size_t prefix_len = strlen(prefix);

    assert(prefix_len + name_len <= 255);

    if (name[0] == '/') {
        name_len--;
        name++;
    }

    if (prefix[prefix_len - 1] != '/') {
        prefix[prefix_len] = '/';
        prefix_len++;
    }

    memcpy(fs->path_tmp, prefix, prefix_len);
    memcpy(fs->path_tmp + prefix_len, name, name_len + 1);
    return fs->path_buf;
}

dos_status_t vfs_fs_open(fs_t *fs_in, char *name, uint8_t mode, bool overwrite, file_t **out, uint32_t *len)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    struct stat    st;
    vfs_file_t    *file = malloc(sizeof(vfs_file_t));
    char          *vfs_name = vfs_get_name(fs, name);
    int            res, flags;

    file->file = (file_t) {
        .close = vfs_file_close,
        .read = vfs_file_read,
        .write = vfs_file_write,
        .seek = vfs_file_seek,
    };
    *out = NULL;

    flags = 0;
    switch (mode) {
    case DOS_OPEN_READ:
        flags = O_RDONLY;
        break;
    case DOS_OPEN_WRITE:
        flags = O_WRONLY | O_CREAT;
        break;
    case DOS_OPEN_APPEND:
        flags = O_WRONLY | O_APPEND;
        break;
    default:
        break;
    }

    res = stat(vfs_name, &st);
    if (mode == DOS_OPEN_WRITE && !overwrite) {
        if (res == 0) {
            return DOS_ERR(FILE_EXISTS);
        } else if (errno != ENOENT) {
            return dos_from_errno(errno);
        }
    }
    if (len != NULL)
        *len = (uint32_t)st.st_size;

    file->fd = open(vfs_name, flags);
    if (file->fd == -1)
        return dos_from_errno(errno);

    if (mode == DOS_OPEN_WRITE) {
        res = ftruncate(file->fd, 0);
        if (res != 0)
            return dos_from_errno(errno);
    }

    *out = (file_t *)file;
    return DOS_OK;
}

dos_status_t vfs_fs_opendir(fs_t *fs_in, char *name, dir_t **out)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    vfs_dir_t     *dir = malloc(sizeof(vfs_dir_t));
    dir->dir = (dir_t) {
        .next = vfs_dir_next,
        .close = vfs_dir_close
    };
    dir->d = opendir(vfs_get_name(fs, name));
    dir->cwd = name;
    dir->fs = fs;

    if (dir->d == NULL) {
        free(dir);
        return dos_from_errno(errno);
    }

    *out = (dir_t *)dir;
    return DOS_OK;
}

const char *vfs_fs_name(fs_t *fs_in)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    return fs->name;
}

const char *vfs_fs_type(fs_t *fs)
{
    return "FAT32";
}

dos_status_t vfs_fs_delete(fs_t *fs_in, char *name)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    int            res = remove(vfs_get_name(fs, name));
    if (res == -1)
        return dos_from_errno(errno);

    return DOS_OK;
}

dos_status_t vfs_fs_stat(fs_t *fs_in, char *path, dirent_t *out)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    struct stat    st;
    if (stat(vfs_get_name(fs, path), &st) != 0)
        return DOS_ERR(FILE_NOT_FOUND);

    out->sz = st.st_size;
    out->type = (st.st_mode & S_IFDIR) ? DOS_DIRENT_DIR : DOS_DIRENT_FILE;
    out->mtime = st.st_mtim;

    return DOS_OK;
}

dos_status_t vfs_fs_mkdir(fs_t *fs_in, char *path)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    if (mkdir(vfs_get_name(fs, path), S_IRWXU | S_IRWXG | S_IRWXO) == -1) {
        return dos_from_errno(errno);
    } else {
        return DOS_OK;
    }
}

dos_status_t vfs_fs_rmdir(fs_t *fs_in, char *path)
{
    struct vfs_fs *fs = (struct vfs_fs *)fs_in;
    if (rmdir(vfs_get_name(fs, path)) == -1) {
        return dos_from_errno(errno);
    } else {
        return DOS_OK;
    }
}

dos_status_t vfs_file_close(file_t *file)
{
    vfs_file_t *vfs_file = (vfs_file_t *)file;
    close(vfs_file->fd);
    free(file);
    return DOS_OK;
}

dos_status_t vfs_file_read(file_t *file, channel_t *chan)
{
    int         res;
    vfs_file_t *vfs_file = (vfs_file_t *)file;
    size_t      cnt;

    cnt = MIN(chan->len - chan->pos, 32768);
    if (cnt == 0)
        return DOS_OK;

    chan->buf = realloc(chan->buf, cnt);
    if (chan->buf == NULL) {
        LOG(vfs, "failed to alloc channel buf");
        return DOS_ERR(NOT_READY);
    }

    res = read(vfs_file->fd, chan->buf, cnt);
    if (res < 0) {
        LOG(vfs, "failed to read file: %d", errno);
        return DOS_ERR(NOT_READY);
    }
    chan->buflen = res;
    return DOS_OK;
}

dos_status_t vfs_file_write(file_t *file, uint8_t *buf, size_t len)
{
    int         res;
    vfs_file_t *vfs_file = (vfs_file_t *)file;

    // FIXME: Can this ever return <len?
    res = write(vfs_file->fd, buf, len);
    if (res < 0)
        return dos_from_errno(errno);
    return DOS_OK;
}

dos_status_t vfs_file_seek(file_t *file, uint32_t pos)
{
    vfs_file_t *vfs_file = (vfs_file_t *)file;

    lseek(vfs_file->fd, pos, SEEK_SET);
    return DOS_OK;
}

dirent_t *vfs_dir_next(dir_t *dir)
{
    vfs_dir_t     *vfs_dir = (vfs_dir_t *)dir;
    struct dirent *ent = readdir(vfs_dir->d);
    struct stat    st = {
           .st_size = 0,
           .st_mtime = 0
    };

    if (ent == NULL)
        return NULL;

    stat(vfs_get_name2(vfs_dir->fs, vfs_dir->cwd, ent->d_name), &st);

    vfs_dir->dirent.name = ent->d_name;
    vfs_dir->dirent.sz = st.st_size;
    vfs_dir->dirent.type = (ent->d_type == DT_DIR) ? DOS_DIRENT_DIR : DOS_DIRENT_FILE;
    vfs_dir->dirent.mtime = st.st_mtim;

    return &vfs_dir->dirent;
}

void vfs_dir_close(dir_t *dir)
{
    vfs_dir_t *vfs_dir = (vfs_dir_t *)dir;
    closedir(vfs_dir->d);
    free(dir);
}
