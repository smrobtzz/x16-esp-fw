// SPDX-License-Identifier: GPL-3.0-only
#include <esp_log.h>

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>

#include <smb2/libsmb2.h>
#include <smb2/smb2.h>
#include <sys/param.h>

#include "defs.h"
#include "dos.h"

typedef struct {
    fs_t                 fs;
    struct smb2_context *ctx;
    struct smb2_url     *url;
    char                *name;
} smb_fs_t;

typedef struct {
    file_t               file;
    struct smb2_context *ctx;
    struct smb2fh       *f;
} smb_file_t;

typedef struct {
    dir_t                dir;
    dirent_t             dirent;
    struct smb2dir      *smb2dir;
    struct smb2_context *ctx;
} smb_dir_t;

dos_status_t smb_fs_open(fs_t *fs, char *name, uint8_t mode, bool overwrite, file_t **out, uint32_t *len);
dos_status_t smb_fs_opendir(fs_t *fs, char *name, dir_t **out);
dos_status_t smb_fs_err(smb_fs_t *fs);
void         smb_fs_unmount(fs_t *fs);
const char  *smb_fs_name(fs_t *fs);
const char  *smb_fs_type(fs_t *fs);
dos_status_t smb_fs_delete(fs_t *fs, char *name);
dos_status_t smb_fs_stat(fs_t *fs, char *path, dirent_t *out);
dos_status_t smb_fs_mkdir(fs_t *fs, char *path);
dos_status_t smb_fs_rmdir(fs_t *fs, char *path);
dos_status_t smb_file_close(file_t *file);
dos_status_t smb_file_read(file_t *file, channel_t *chan);
dos_status_t smb_file_write(file_t *file, uint8_t *buf, size_t len);
dos_status_t smb_file_seek(file_t *file, uint32_t pos);
dirent_t    *smb_dir_next(dir_t *dir);
void         smb_dir_close(dir_t *dir);

static smb_fs_t smb_fs = {
    .fs = {
        .open = smb_fs_open,
        .opendir = smb_fs_opendir,
        .unmount = smb_fs_unmount,
        .name = smb_fs_name,
        .type = smb_fs_type,
        .delete = smb_fs_delete,
        .stat = smb_fs_stat,
        .mkdir = smb_fs_mkdir,
        .rmdir = smb_fs_rmdir },
};

dos_status_t smb_fs_mount(fs_t **out_fs, char *uri)
{
    // why
    uri[0] = 's';
    uri[1] = 'm';
    uri[2] = 'b';

    smb_fs.ctx = smb2_init_context();
    smb_fs.url = smb2_parse_url(smb_fs.ctx, uri);
    if (smb_fs.url == NULL) {
        LOG(smb, "failed to parse url");
        smb2_destroy_context(smb_fs.ctx);
        return DOS_ERR(INVALID_URL);
    }

    smb2_set_version(smb_fs.ctx, SMB2_VERSION_ANY2);
    if (smb2_connect_share(
            smb_fs.ctx,
            smb_fs.url->server,
            smb_fs.url->share,
            smb_fs.url->user)
        != 0) {
        smb2_destroy_url(smb_fs.url);
        smb2_destroy_context(smb_fs.ctx);
        LOG(smb, "failed to connect");
        return DOS_ERR(CONNECT_FAIL);
    }

    smb2_set_timeout(smb_fs.ctx, 1);

    *out_fs = (fs_t *)&smb_fs;
    return DOS_OK;
}

void smb_fs_unmount(fs_t *fs)
{
    smb2_disconnect_share(smb_fs.ctx);
    smb2_destroy_url(smb_fs.url);
    smb2_destroy_context(smb_fs.ctx);
}

dos_status_t smb_fs_open(fs_t *fs, char *name, uint8_t mode, bool overwrite, file_t **out, uint32_t *len)
{
    int                 res, flags;
    struct smb2_stat_64 st;
    smb_fs_t           *smb_fs = (smb_fs_t *)fs;
    smb_file_t         *file = malloc(sizeof(smb_file_t));
    name++;

    file->file = (file_t) {
        .close = smb_file_close,
        .read = smb_file_read,
        .write = smb_file_write,
        .seek = smb_file_seek,
    };
    file->ctx = smb_fs->ctx;
    *out = NULL;

    flags = 0;
    switch (mode) {
    case DOS_OPEN_READ:
        flags = O_RDONLY;
        break;
    case DOS_OPEN_WRITE:
        flags = O_WRONLY | O_CREAT;
        break;
    case DOS_OPEN_APPEND:
        flags = O_WRONLY | O_APPEND;
        break;
    default:
        break;
    }

    res = smb2_stat(smb_fs->ctx, name, &st);
    if (mode == DOS_OPEN_WRITE && !overwrite && res == 0) {
        return DOS_ERR(FILE_EXISTS);
    } else if (len != NULL) {
        if (res == 0) {
            *len = st.smb2_size;
        } else {
            *len = 0;
        }
    }

    file->f = smb2_open(smb_fs->ctx, name, flags);
    if (file->f == NULL) {
        ESP_LOGE("smb", "err %s", smb2_get_error(smb_fs->ctx));
        // return smb_fs_err(smb_fs);
    }

    if (mode == DOS_OPEN_WRITE) {
        res = smb2_ftruncate(smb_fs->ctx, file->f, 0);
        if (res != 0) {
            // return dos_from_errno(ret);
        }
    }

    *out = (file_t *)file;
    return DOS_OK;
}

dos_status_t smb_fs_opendir(fs_t *fs, char *name, dir_t **out)
{
    smb_fs_t  *smb_fs = (smb_fs_t *)fs;
    smb_dir_t *dir = malloc(sizeof(smb_dir_t));
    name++;

    dir->dir = (dir_t) {
        .next = smb_dir_next,
        .close = smb_dir_close
    };
    dir->smb2dir = smb2_opendir(smb_fs->ctx, name);
    dir->ctx = smb_fs->ctx;

    if (dir->smb2dir == NULL) {
        ESP_LOGE(DOS_TAG, "smb2_opendir error %s", smb2_get_error(smb_fs->ctx));
        // return smb_fs_err(smb_fs);
    }

    *out = (dir_t *)dir;
    return DOS_OK;
}

const char *smb_fs_name(fs_t *fs)
{
    return ((smb_fs_t *)fs)->url->share;
}

const char *smb_fs_type(fs_t *fs)
{
    return "SMB";
}

dos_status_t smb_fs_stat(fs_t *fs, char *path, dirent_t *out)
{
    smb_fs_t           *smb_fs = (smb_fs_t *)fs;
    struct smb2_stat_64 st;
    path++;

    if (smb2_stat(smb_fs->ctx, path, &st) != 0)
        return DOS_ERR(FILE_NOT_FOUND);

    out->sz = st.smb2_size;
    out->type = (st.smb2_type == SMB2_TYPE_DIRECTORY) ? DOS_DIRENT_DIR : DOS_DIRENT_FILE;
    out->mtime.tv_sec = st.smb2_mtime;
    out->mtime.tv_nsec = st.smb2_mtime_nsec;

    return DOS_OK;
}

dos_status_t smb_fs_mkdir(fs_t *fs, char *path)
{
    smb_fs_t *smb_fs = (smb_fs_t *)fs;
    int       res;
    path++;

    if ((res = smb2_mkdir(smb_fs->ctx, path)) < 0) {
        return dos_from_errno(-res);
    } else {
        return DOS_OK;
    }
}

dos_status_t smb_fs_rmdir(fs_t *fs, char *path)
{
    smb_fs_t *smb_fs = (smb_fs_t *)fs;
    int       res;
    path++;

    if ((res = smb2_rmdir(smb_fs->ctx, path)) < 0) {
        return dos_from_errno(-res);
    } else {
        return DOS_OK;
    }
}

dos_status_t smb_fs_delete(fs_t *fs, char *name)
{
    smb_fs_t *smb_fs = (smb_fs_t *)fs;
    int       res;
    name++;

    if ((res = smb2_unlink(smb_fs->ctx, name)) < 0) {
        return dos_from_errno(-res);
    } else {
        return DOS_OK;
    }
}

dos_status_t smb_file_close(file_t *file)
{
    smb_file_t *smb_file = (smb_file_t *)file;

    smb2_close(smb_file->ctx, smb_file->f);
    free(file);

    return DOS_OK;
}

dos_status_t smb_file_read(file_t *file, channel_t *chan)
{
    int         res;
    smb_file_t *smb_file = (smb_file_t *)file;
    size_t      cnt;

    cnt = MIN(chan->len - chan->pos, 32768);
    if (cnt == 0)
        return DOS_OK;

    chan->buf = realloc(chan->buf, cnt);
    if (chan->buf == NULL) {
        LOG(smb, "failed to alloc channel buf");
        return DOS_ERR(NOT_READY);
    }

    res = smb2_read(smb_file->ctx, smb_file->f, chan->buf, cnt);
    if (res < 0) {
        LOG(smb, "failed to read file: %d", res);
        return DOS_ERR(NOT_READY);
    }
    chan->buflen = res;
    return DOS_OK;
}

dos_status_t smb_file_write(file_t *file, uint8_t *buf, size_t len)
{
    int         res;
    smb_file_t *smb_file = (smb_file_t *)file;

    res = smb2_write(smb_file->ctx, smb_file->f, buf, len);

    if (res < 0)
        return dos_from_errno(-res);
    return DOS_OK;
}

dos_status_t smb_file_seek(file_t *file, uint32_t pos)
{
    smb_file_t *smb_file = (smb_file_t *)file;

    smb2_lseek(smb_file->ctx, smb_file->f, pos, SEEK_SET, NULL);
    return DOS_OK;
}

dirent_t *smb_dir_next(dir_t *dir)
{
    smb_dir_t         *smb_dir = (smb_dir_t *)dir;
    struct smb2dirent *ent = smb2_readdir(smb_dir->ctx, smb_dir->smb2dir);

    if (ent == NULL) {
        return NULL;
    }

    smb_dir->dirent.name = ent->name;
    smb_dir->dirent.sz = ent->st.smb2_size;
    smb_dir->dirent.type = (ent->st.smb2_type == SMB2_TYPE_DIRECTORY)
        ? DOS_DIRENT_DIR
        : DOS_DIRENT_FILE;

    return &smb_dir->dirent;
}

void smb_dir_close(dir_t *dir)
{
    smb_dir_t *smb_dir = (smb_dir_t *)dir;

    smb2_closedir(smb_dir->ctx, smb_dir->smb2dir);
    free(dir);
}
