// SPDX-License-Identifier: GPL-3.0-only
#include <arpa/inet.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>

#include <driver/gpio_filter.h>
#include <driver/gptimer.h>
#include <esp_log.h>
#include <ff.h>

#include "defs.h"
#include "device.h"
#include "dos.h"
#include "sd.h"
#include "spi.h"

static void         vdev_chan_free(struct vdev *vdev);
static dos_status_t vdev_listing_open(struct vdev *, file_t **);
static dos_status_t vdev_listing_close(file_t *);
static dos_status_t vdev_listing_read(file_t *, channel_t *);
static dir_t       *vdev_cwd_dir_open(struct vdev *vdev);
static dirent_t    *vdev_cwd_dir_next(dir_t *);
static void         vdev_cwd_dir_close(dir_t *);
static void         vdev_handle_command(struct vdev *);
static char        *vdev_resolve_path(struct vdev *vdev, char *path);
static void         vdev_cmd_cd(struct vdev *);
static void         vdev_cmd_md(struct vdev *);
static void         vdev_cmd_rd(struct vdev *);
static void         vdev_cmd_c(struct vdev *);
static bool         vdev_ensure_fs_mounted(struct vdev *);

// clang-format off
static const char *STATUS[] = {
    "OK",                                                        // 0
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  // 1-10
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  // 11-20
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,        // 21-29
    "SYNTAX ERROR",                                              // 30
    NULL, NULL, NULL, NULL,                                      // 31-34
    "CONNECTION FAILED",                                         // 35
    "INVALID URL",                                               // 36
    NULL, NULL, NULL, NULL,                                      // 37-40
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  // 41-50
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  // 51-60
    NULL,                                                        // 61
    "FILE NOT FOUND",                                            // 62
    "FILE EXISTS",                                               // 63
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  // 64-73
    "DRIVE NOT READY",                                           // 74
    "NOT MOUNTED",                                               // 75
};

static const char NO_MOUNT_MSG[] =
    "\x01\x08"
    "\x01\x01\x01\x00NO VOLUME IS MOUNTED TO THIS \x00"
    "\x01\x01\x02\x00" "DEVICE NUMBER\x00"
    "\x01\x01\x03\x00\x00"
    "\x01\x01\x04\x00USE CFG.PRG TO SET THE DEFAULT\x00"
    "\x01\x01\x05\x00VOLUME OR RUN THE FOLLOWING\x00"
    "\x01\x01\x06\x00" "COMMAND TO MOUNT THE FIRST\x00"
    "\x01\x01\x07\x00PARTITION ON THE SD CARD:\x00"
    "\x01\x01\x08\x00@\"X-M:VOL:SD0P0\"\x00";
// clang-format on

extern const uint8_t net_bin_start[] asm("_binary_net_bin_start");
extern const uint8_t net_bin_end[] asm("_binary_net_bin_end");
extern const uint8_t cfg_prg_start[] asm("_binary_cfg_prg_start");
extern const uint8_t cfg_prg_end[] asm("_binary_cfg_prg_end");

static dirent_t dirent_dot_dot = {
    .name = "..",
    .type = DOS_DIRENT_DIR,
    .sz = 0,
    .mtime = { 0 }
};

struct vdev vdev_new(dos_t *dos)
{
    channel_t *chans;

    chans = calloc(16, sizeof(channel_t));
    if (chans == NULL)
        goto fail_nomem;

    for (int i = 0; i < 16; i++) {
        chans[i].name = malloc(256);
        if (chans[i].name == NULL)
            goto fail_nomem;
    }

    chans[15].buf = malloc(81);
    if (chans[15].buf == NULL)
        goto fail_nomem;

    // clang-format off
    return (struct vdev) {
        .chans = chans,
        .cur_chan = chans,
        .cur_chan_idx = 0,

        .dos = dos,
        .listing = {
            .file = {
                .read = vdev_listing_read,
                .close = vdev_listing_close,
                .write = NULL,
            } },
        .cwd_dir = {
            .dir = {
                .next = vdev_cwd_dir_next,
                .close = vdev_cwd_dir_close
            },
            .done = false,
            .dirent = {
                .type = DOS_DIRENT_DIR,
                .sz = 0,
            } 
        }

    };
    // clang-format on

fail_nomem:
    LOG(dos, "failed to allocate vdev channel data");
    abort();
}

void vdev_start(struct vdev *vdev)
{
    strcpy(vdev->cwd, "/");
    vdev->cwd_len = 1;
    vdev->listing.done = true;
}

void vdev_chan_open(struct vdev *vdev)
{
    bool         overwrite = false;
    uint8_t      mode = DOS_OPEN_READ;
    char        *modestr;
    char        *name = vdev->cur_chan->name;
    size_t       name_len = vdev->cur_chan->pos;
    dos_status_t status;

    LOG_IF(dos1, "open \"%s\", %u", vdev->cur_chan->name, (unsigned)vdev->cur_chan_idx);

    if (name_len == 0)
        return;

    if (vdev->cur_chan_idx == DOS_CHANNEL_CMD) {
        vdev_handle_command(vdev);
    } else {
        if (name_len > 0 && vdev->cur_chan->name[0] == '$') {
            if (!vdev_ensure_fs_mounted(vdev)) {
                vdev->cur_chan->buf = (uint8_t *)NO_MOUNT_MSG;
                vdev->cur_chan->buflen = sizeof(NO_MOUNT_MSG);
                vdev->cur_chan->len = sizeof(NO_MOUNT_MSG);
                vdev->cur_chan->mode = DOS_OPEN_READ | DOS_OPEN_FW;
                vdev_set_status(vdev, DOS_OK);
                return;
            }

            status = vdev_listing_open(vdev, &vdev->cur_chan->file);
            vdev_set_status(vdev, status);
            if (status.code == D_OK)
                vdev->cur_chan->mode = DOS_OPEN_READ;
            return;
        }

        if (name_len > 1 && memcmp(vdev->cur_chan->name, "@:", 2) == 0) {
            overwrite = true;
            name += 2;
            name_len -= 2;
        }

        modestr = strchr(name, ',');
        if (modestr != NULL)
            *modestr = 0;

        if (vdev_resolve_path(vdev, name) == NULL) {
            vdev_set_status(vdev, DOS_ERR(FILE_NOT_FOUND));
            return;
        }

        if (vdev->cur_chan_idx == DOS_CHANNEL_SAVE) {
            mode = DOS_OPEN_WRITE;
        }

        if (modestr != NULL) {
            *modestr = ',';
            if (strlen(modestr) != 4 || modestr[2] != ',') {
                vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
            } else {
                switch (modestr[3]) {
                case 'R':
                    // File defaults to reading
                    break;
                case 'W':
                    mode = DOS_OPEN_WRITE;
                    break;
                case 'A':
                    mode = DOS_OPEN_APPEND | DOS_OPEN_WRITE;
                    break;
                case 'M':
                    mode = DOS_OPEN_READ | DOS_OPEN_WRITE;
                    break;
                default:
                    vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
                }
            }
        }

        if (vdev->fs == NULL) {
            vdev_set_status(vdev, DOS_ERR(FILE_NOT_FOUND));
        } else {
            channel_t *chan = vdev->cur_chan;

            if (g_autoboot_prg != NULL && strcmp(vdev->pathbuf, "/AUTOBOOT.X16") == 0) {
                chan->buf = g_autoboot_prg;
                chan->buflen = g_autoboot_size;
                chan->len = g_autoboot_size;
                chan->mode = DOS_OPEN_READ | DOS_OPEN_RAM;

                g_autoboot_prg = NULL;
                vdev_set_status(vdev, DOS_OK);
            } else if (strcasecmp(vdev->pathbuf, "/CFG.PRG") == 0) {
                chan->buf = (uint8_t *)cfg_prg_start;
                chan->buflen = cfg_prg_end - cfg_prg_start;
                chan->len = cfg_prg_end - cfg_prg_start;
                chan->mode = DOS_OPEN_READ | DOS_OPEN_FW;
                vdev_set_status(vdev, DOS_OK);
            } else if (strcasecmp(vdev->pathbuf, "/NET.BIN") == 0) {
                chan->buf = (uint8_t *)net_bin_start;
                chan->buflen = net_bin_end - net_bin_start;
                chan->len = net_bin_end - net_bin_start;
                chan->mode = DOS_OPEN_READ | DOS_OPEN_FW;
                vdev_set_status(vdev, DOS_OK);
            } else {
                if (!vdev_ensure_fs_mounted(vdev))
                    return;

                dos_status_t status = vdev->fs->open(vdev->fs, vdev->pathbuf, mode, overwrite, &chan->file, &chan->len);
                if (status.code == D_OK)
                    chan->mode = mode;
                vdev_set_status(vdev, status);
            }
        }
    }
}

void vdev_chan_close(struct vdev *vdev)
{
    if (vdev->cur_chan->file != NULL) {
        vdev->cur_chan->file->close(vdev->cur_chan->file);
        vdev->cur_chan->file = NULL;
        vdev->cur_chan->name[0] = 0;
    }
    vdev_chan_free(vdev);
    vdev->cur_chan->mode = 0;
}

void vdev_chan_read(struct vdev *vdev)
{
    // Might be an error message, which would not be backed by a file
    if (vdev->cur_chan->file == NULL)
        return;
    vdev->cur_chan->bufpos = 0;
    vdev->cur_chan->file->read(vdev->cur_chan->file, vdev->cur_chan);
}

static void vdev_chan_free(struct vdev *device)
{
    LOG_IF(dos0, "free channel %u", (unsigned)device->cur_chan_idx);
    if (device->cur_chan_idx == 15)
        return;

    if (!(device->cur_chan->mode & DOS_OPEN_FW))
        free((void *)device->cur_chan->buf);
    device->cur_chan->buf = 0;
    device->cur_chan->buflen = 0;
}

static dos_status_t vdev_listing_open(struct vdev *vdev, file_t **file)
{
    dir_t       *dir = NULL;
    dos_status_t status;
    char        *buf = calloc(1, 36), *fil, *typ;
    uint8_t      kind = LISTING_REG;
    uint8_t      filter = FILTER_ANY;

    if (vdev->cur_chan->pos >= 3) {
        if (vdev->cur_chan->name[1] == '=') {
            switch (vdev->cur_chan->name[2]) {
            case 'T':
                kind = LISTING_TIME;
                break;
            case 'L':
                kind = LISTING_LONG;
                break;
            case 'C':
                kind = LISTING_CWD;
                break;
            default:
                break;
            }
        }
    }
    if ((fil = strchr(vdev->cur_chan->name, ':')) != NULL) {
        fil++;
        if ((typ = strchr(fil, '=')) != NULL) {
            if (*(typ + 1) == 'P') {
                filter = FILTER_PRG;
            } else if (*(typ + 1) == 'D') {
                filter = FILTER_DIR;
            }
        }
    }
    if (fil != NULL && fil != typ) {
        vdev->listing.name = fil;
    } else {
        vdev->listing.name = NULL;
    }
    vdev->listing.filter = filter;
    vdev->listing.kind = kind;
    vdev->listing.done = false;

    if (kind == LISTING_CWD) {
        status = DOS_OK;
        dir = vdev_cwd_dir_open(vdev);
    } else {
        status = vdev->fs->opendir(vdev->fs, vdev->cwd, &dir);
        vdev->listing.should_dot_dot = vdev->cwd_len != 1;
    }
    if (dir == NULL || buf == NULL) {
        ESP_LOGE(DOS_TAG, "failed to open dir");
        return status;
    }

    // 2 byte load address
    // 2 byte link pointer
    // 2 byte line number
    // 1 byte null terminator
    buf[0] = 0x01;
    buf[1] = 0x08;
    buf[2] = 0x01;
    buf[3] = 0x01;
    vdev->cur_chan->bufpos = 0;
    vdev->cur_chan->buflen = 7 + snprintf(buf + 6, 30, "\x12\"%-16s\" %s", vdev->fs->name(vdev->fs), vdev->fs->type(vdev->fs));
    vdev->cur_chan->pos = 0;
    // The length of the directory listing is not known until it has
    // been created. Set the high bit of cur_chan->len so that we always try
    // to read more of the directory listing. When listing is done, unset
    // the bit so we stop reading.
    vdev->cur_chan->len = vdev->cur_chan->buflen | 0x80000000;
    vdev->cur_chan->buf = (uint8_t *)buf;

    vdev->listing.dir = dir;
    *file = (file_t *)&vdev->listing;

    return DOS_OK;
}

static dos_status_t vdev_listing_read(file_t *file, channel_t *chan)
{
    listing_file_t *listing = (listing_file_t *)file;
    struct tm      *time;
    dirent_t       *ent;
    uint32_t        nclst;
    uint16_t        sz;
    size_t          off = 0, left = 32, len = 32, name_len;
    FATFS          *fatfs;
    char           *buf = (char *)chan->buf;
    char           *unit = "";
    int             res, ws = 0;

    while (true) {
        if (listing->should_dot_dot) {
            ent = &dirent_dot_dot;
            listing->should_dot_dot = false;
        } else {
            ent = listing->dir->next(listing->dir);
        }
        if (ent == NULL) {
            if (!listing->done) {
                f_getfree("", &nclst, &fatfs);
                sz = (uint16_t)(((uint64_t)nclst * (uint64_t)fatfs->csize * (uint64_t)fatfs->ssize)
                    / (1024 * 1024 * 1024));
                buf[0] = 1;
                buf[1] = 1;
                buf[2] = sz;
                buf[3] = sz >> 8;
                memcpy(buf + 4, "GB FREE.", 9);
                buf[13] = 0;
                buf[14] = 0;
                chan->buflen = 15;
                chan->len += 15;
                chan->len &= ~0x80000000;
                listing->done = true;
            }
            return DOS_OK;
        }

        if ((listing->filter == FILTER_PRG && ent->type != DOS_DIRENT_FILE)
            || (listing->filter == FILTER_DIR && ent->type != DOS_DIRENT_DIR)) {
            continue;
        } else {
            break;
        }
    }

    buf[0] = 1;
    buf[1] = 1;

    off += 4;
    left -= 4;

    if (listing->kind == LISTING_LONG) {
        if (ent->sz >= 1024 * 1024) {
            unit = "MB";
            sz = ent->sz / (1024 * 1024);
        } else {
            unit = "KB";
            sz = ent->sz / 1024;
        }

        if (sz >= 10 && sz < 100) {
            ws += 1;
        } else if (sz < 10) {
            ws += 2;
        }

        buf[2] = sz;
        buf[3] = sz >> 8;
    } else {
        sz = ent->sz / 256 + ((ent->sz % 256 == 0) ? 0 : 1);
        // Files >16MiB are listed as 65535 blocks
        if (sz == 0 && ent->sz != 0)
            sz = 65535;
        buf[2] = sz;
        buf[3] = sz >> 8;

        if (sz >= 1000) {
            ws = 0;
        } else if (sz >= 100) {
            ws = 1;
        } else if (sz >= 10) {
            ws = 2;
        } else {
            ws = 3;
        }

        for (int i = 0; i < ws; i++) {
            buf[off + i] = ' ';
        }

        off += ws;
        left -= ws;
    }

    name_len = strlen(ent->name);
    time = gmtime(&ent->mtime.tv_sec);
line:
    switch (listing->kind) {
    case LISTING_REG:
    case LISTING_CWD:
        res = snprintf(buf + off, left, "\"%s\"%-*s%s", ent->name,
            (name_len >= 16) ? 1 : (17 - name_len), " ",
            (ent->type == DOS_DIRENT_FILE) ? "PRG" : "DIR");
        break;
    case LISTING_TIME:
        res = snprintf(buf + off, left, "\"%s\"%-*s%s  %04d-%02d-%02d %02d:%02d:%02d", ent->name,
            (name_len >= 16) ? 1 : (17 - name_len), " ",
            (ent->type == DOS_DIRENT_FILE) ? "PRG" : "DIR",
            1900 + time->tm_year, 1 + time->tm_mon, time->tm_mday,
            time->tm_hour, time->tm_min, time->tm_sec);
        break;
    case LISTING_LONG:
        res = snprintf(buf + off, left, "%-*s \"%s\"%-*s%s  %04d-%02d-%02d %02d:%02d:%02d %02X %08llX",
            ws + strlen(unit), unit,
            ent->name, (name_len >= 16) ? 1 : (17 - name_len), " ",
            (ent->type == DOS_DIRENT_FILE) ? "PRG" : "DIR",
            1900 + time->tm_year, 1 + time->tm_mon, time->tm_mday,
            time->tm_hour, time->tm_min, time->tm_sec,
            (ent->type == DOS_DIRENT_FILE) ? 0 : 0x10,
            ent->sz);
        break;
    default:
        res = 0;
        break;
    }

    if (res >= left) {
        len += (res + 1 - left);
        left += (res + 1 - left);
        buf = realloc(buf, len);
        if (buf == 0) {
            chan->len = 0;
            return DOS_OK;
        }
        goto line;
    }
    left -= res + 1;

    chan->len += len - left;
    chan->buflen = len - left;
    chan->buf = (uint8_t *)buf;
    return DOS_OK;
}

static dos_status_t vdev_listing_close(file_t *file)
{
    listing_file_t *listing = (listing_file_t *)file;

    if (listing->dir != NULL)
        listing->dir->close(listing->dir);
    return DOS_OK;
}

static dir_t *vdev_cwd_dir_open(struct vdev *vdev)
{
    vdev->cwd_dir.cwd = strdup(vdev->cwd);
    vdev->cwd_dir.cwd_pos = vdev->cwd_len - 1;
    vdev->cwd_dir.done = false;
    return (dir_t *)&vdev->cwd_dir;
}

static dirent_t *vdev_cwd_dir_next(dir_t *dir)
{
    cwd_dir_t *cdir = (cwd_dir_t *)dir;

    if (cdir->done)
        return NULL;

    cdir->cwd[cdir->cwd_pos] = 0;

    // Backtrack to a path separator or the start of the string
    while (cdir->cwd[cdir->cwd_pos] != '/' && cdir->cwd_pos != 0) {
        cdir->cwd_pos--;
    }

    if (strlen(cdir->cwd + cdir->cwd_pos) == 0) {
        cdir->cwd[0] = '/';
        cdir->cwd[1] = 0;
        cdir->dirent.name = cdir->cwd + cdir->cwd_pos;
        cdir->done = true;
    } else {
        cdir->dirent.name = cdir->cwd + cdir->cwd_pos + 1;
    }

    return &cdir->dirent;
}

void vdev_cwd_dir_close(dir_t *dir)
{
    cwd_dir_t *cdir = (cwd_dir_t *)dir;
    free(cdir->cwd);
}

void vdev_stop(struct vdev *vdev)
{
    for (int i = 0; i < 16; i++) {
        vdev->cur_chan = &vdev->chans[i];
        file_t *file = vdev->cur_chan->file;
        if (file != NULL)
            file->close(file);
        if (i != 15) {
            free(vdev->chans[i].buf);
            vdev->cur_chan->buf = NULL;
            vdev->cur_chan->buflen = 0;
            vdev->cur_chan->bufpos = 0;
            vdev->cur_chan->len = 0;
            vdev->cur_chan->pos = 0;
            vdev->cur_chan->file = NULL;
        }
    }
}

static void vdev_handle_command(struct vdev *vdev)
{
#define CHECK_DOS_CMD(prefix, func)                                     \
    if (strncmp(vdev->cur_chan->name, prefix, sizeof(prefix) - 1) == 0) \
        func(vdev);
#define CHECK_DOS_CMD_B(prefix) \
    if (strncmp(vdev->cur_chan->name, prefix, sizeof(prefix) - 1) == 0)

    // FIXME: all of this
    // clang-format off
    CHECK_DOS_CMD_B("X-M:") {
        dos_cmd_mount(vdev->dos, vdev);
    } else CHECK_DOS_CMD_B("X-U") {
        if (!vdev_ensure_fs_mounted(vdev))
            return;

        dos_cmd_unmount(vdev->dos, vdev);
    } else CHECK_DOS_CMD_B("S") {
        LOG_IF(dos1, "delete \"%s\"", vdev->cur_chan->name + 1);

        if (!vdev_ensure_fs_mounted(vdev))
            return;

        vdev_resolve_path(vdev, vdev->cur_chan->name + 1);
        vdev_set_status(vdev, vdev->fs->delete (vdev->fs, vdev->pathbuf));
    } else CHECK_DOS_CMD("CD", vdev_cmd_cd)
    else CHECK_DOS_CMD("MD", vdev_cmd_md)
    else CHECK_DOS_CMD("RD", vdev_cmd_rd)
    else CHECK_DOS_CMD("C", vdev_cmd_c)
    else CHECK_DOS_CMD_B("P") {
        channel_t *chan;
        uint32_t   pos;

        if (vdev->cur_chan->pos < 6) {
            vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
            return;
        }

        chan = vdev->chans + vdev->cur_chan->name[1];
        pos = *(uint32_t *)&vdev->cur_chan->name[2];
        if (!vdev_ensure_fs_mounted(vdev))
            return;

        if (chan->file != NULL && chan->mode != 0) {
            vdev_set_status(vdev, chan->file->seek(chan->file, pos));
            chan->pos = pos;
            // Set bufpos to buflen to cause any future reads to refill the buffer
            chan->bufpos = chan->buflen;
        } else {
            vdev_set_status(vdev, DOS_ERR(NOT_READY));
        }
    } else CHECK_DOS_CMD_B("T") {
        channel_t *chan;

        if (vdev->cur_chan->pos < 2) {
            vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
            return;
        }

        chan = vdev->chans + vdev->cur_chan->name[1];
        if (!vdev_ensure_fs_mounted(vdev))
            return;

        // If TELL succeeds, 07,pppppppp ssssssss,00,00 is returned on the command channel,
        // where pppppppp is a hexadecimal representation of the position,
        // and ssssssss is a hexadecimal representation of the file's size.
        if (chan->file != NULL && chan->mode != 0) {
            int len = snprintf((char *)vdev->chans[15].buf, 81, "07,%08lx %08lx,00,00\x0D",
                chan->pos,
                chan->len);
            vdev->chans[15].buflen = (uint32_t)len;
            vdev->chans[15].bufpos = 0;
        } else {
            vdev_set_status(vdev, DOS_ERR(NOT_READY));
        }
    } else {
        LOG(dos, "unknown command: \"%s\"", vdev->cur_chan->name);
        vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
        return;
    }
    // clang-format on
}

void vdev_set_status(struct vdev *vdev, dos_status_t status)
{
    int len = snprintf((char *)vdev->chans[15].buf, 81, "%02u,%s,%02u,%02u\x0D",
        (unsigned)status.code,
        STATUS[status.code],
        (unsigned)status.extra1,
        (unsigned)status.extra2);
    vdev->chans[15].buflen = (uint32_t)len;
    vdev->chans[15].bufpos = 0;
    LOG_IF(dos1, "set status %u", (unsigned)status.code);
}

static void vdev_cmd_cd(struct vdev *vdev)
{
    size_t       len;
    dirent_t     dent;
    dos_status_t res;

    LOG_IF(dos1, "cd \"%s\"", vdev->cur_chan->name + 2);

    if (!vdev_ensure_fs_mounted(vdev))
        return;

    // Map :← to :..
    if (strncmp(vdev->cur_chan->name + 2, ":\x5f", 2) == 0) {
        char path[4];
        strcpy(path, ":..");
        vdev_resolve_path(vdev, path);
    } else {
        vdev_resolve_path(vdev, vdev->cur_chan->name + 2);
    }

    res = vdev->fs->stat(vdev->fs, vdev->pathbuf, &dent);
    if (res.code != D_OK) {
        vdev_set_status(vdev, res);
        return;
    }

    if (dent.type != DOS_DIRENT_DIR) {
        vdev_set_status(vdev, DOS_ERR(FILE_NOT_FOUND));
    }

    len = strlen(vdev->pathbuf);
    // Add trailing / if one can fit
    if (len <= sizeof(vdev->pathbuf) - 2) {
        vdev->pathbuf[len] = '/';
        vdev->pathbuf[len + 1] = 0;
    }
    strcpy(vdev->cwd, vdev->pathbuf);
    vdev->cwd_len = strlen(vdev->cwd);
    vdev_set_status(vdev, DOS_OK);
}

static void vdev_cmd_md(struct vdev *vdev)
{
    dirent_t     dent;
    dos_status_t res;

    LOG_IF(dos1, "md \"%s\"", vdev->cur_chan->name + 2);

    if (!vdev_ensure_fs_mounted(vdev))
        return;

    vdev_resolve_path(vdev, vdev->cur_chan->name + 2);
    res = vdev->fs->stat(vdev->fs, vdev->pathbuf, &dent);
    if (res.code == D_OK) {
        vdev_set_status(vdev, DOS_ERR(FILE_EXISTS));
        return;
    }

    res = vdev->fs->mkdir(vdev->fs, vdev->pathbuf);
    vdev_set_status(vdev, res);
}

static void vdev_cmd_rd(struct vdev *vdev)
{
    dos_status_t res;

    LOG_IF(dos1, "rd \"%s\"", vdev->cur_chan->name + 2);

    if (!vdev_ensure_fs_mounted(vdev))
        return;

    vdev_resolve_path(vdev, vdev->cur_chan->name + 2);

    res = vdev->fs->rmdir(vdev->fs, vdev->pathbuf);
    vdev_set_status(vdev, res);
}

// `C`[_path_a_]`:`_target_name_`=`[_path_b_]`:`_source_name_[`,`...]
static void vdev_cmd_c(struct vdev *vdev)
{
    dos_status_t status;
    char        *copy = vdev->cur_chan->name + 1;
    char        *copy_end = strchr(copy, ',');
    char        *dest_path;
    char        *eq;
    file_t      *src, *dest;
    channel_t    chan;

    if (!vdev_ensure_fs_mounted(vdev)) {
        vdev_set_status(vdev, DOS_ERR(NOT_MOUNTED));
        return;
    }

    dest_path = malloc(256);
    if (dest_path == NULL) {
        LOG(dos, "vdev_cmd_c: failed to allocate dest_path");
        return;
    }

    while (true) {
        // 1. Null terminate `copy`
        if (copy_end != NULL)
            *copy_end = 0;

        // 2. Split `copy` into `src` and `dest`
        eq = strchr(copy, '=');
        // There must be an = separating
        // source and destination paths
        if (eq == NULL)
            goto bad_syntax;

        // Before the = is the source, so null terminate it resolve the path
        *eq = 0;
        if (vdev_resolve_path(vdev, copy) == NULL)
            goto bad_syntax;
        strcpy(dest_path, vdev->pathbuf);

        // After the = is the destination, it was null terminated earlier by 1
        if (vdev_resolve_path(vdev, eq + 1) == NULL)
            goto bad_syntax;

        // 3. Copy the file
        // Open source and destination files
        memset(&chan, 0, sizeof(channel_t));
        status = vdev->fs->open(vdev->fs, vdev->pathbuf, DOS_OPEN_READ, false, &src, &chan.len);
        if (status.code != D_OK) {
            LOG(dos, "vdev_cmd_c: failed to open src: %s", vdev->pathbuf);
            goto fail_status;
        }

        status = vdev->fs->open(vdev->fs, dest_path, DOS_OPEN_WRITE, false, &dest, NULL);
        if (status.code != D_OK) {
            LOG(dos, "vdev_cmd_c: failed to open dest: %s", dest_path);
            goto fail_status;
        }

        // Copy data
        do {
            chan.buflen = 0;
            status = src->read(src, &chan);
            if (status.code != D_OK)
                goto fail_status;
            status = dest->write(dest, chan.buf, chan.buflen);
            if (status.code != D_OK)
                goto fail_status;
        } while (chan.buflen != 0);

        // Close the files
        src->close(src);
        dest->close(dest);

        // 4. Advance to the next copy
        if (copy_end != NULL) {
            copy = copy_end + 1;
            copy_end = strchr(copy, ',');
        } else {
            break;
        }
    }

    free(chan.buf);
    free(dest_path);
    vdev_set_status(vdev, DOS_OK);
    return;
bad_syntax:
    free(dest_path);
    vdev_set_status(vdev, DOS_ERR(SYNTAX_ERR));
    return;
fail_status:
    free(dest_path);
    vdev_set_status(vdev, status);
}

dos_status_t dos_from_errno(int errn)
{
    switch (errn) {
    case ENOENT:
    case ENOTDIR:
        return DOS_ERR(FILE_NOT_FOUND);
    case ENAMETOOLONG:
        return DOS_ERR(SYNTAX_ERR);
    case EEXIST:
    case ENOTEMPTY:
    // rmdir erroneously returns EACCES when
    // the directory is not empty
    case EACCES:
        return DOS_ERR(FILE_EXISTS);
    default:
        LOG(dos1, "not ready: %d", errn);
        return DOS_ERR(NOT_READY);
    }
}

// Parse & resolve a path, putting the result in dos->pathbuf
//
// Path parsing is very lenient, so there are numerous paths
// that will resolve to the same place
// Examples:
// From Directory       Path                   Resolves To
//                      //foo/bar/:abcdefg     /foo/bar/abcdefg
//                      //foo/bar/:abc*        /foo/bar/abcdefg
// /foo                 /bar/:abcdefg          /foo/bar/abcdefg
// /foo                 /bar/:abc*             /foo/bar/abcdefg
//                      /foo/bar/abc*          /foo/bar/abcdefg
//                      /foo/bar/abcdefg       /foo/bar/abcdefg
// /foo                 ./bar/abc*             /foo/bar/abcdefg
// /foo                 ./bar/abcdefg          /foo/bar/abcdefg
// /foo                 bar/abcdefg            /foo/bar/abcdefg
// /foo                 bar/abc*               /foo/bar/abcdefg
// /foo/baz             ../bar/abcdefg         /foo/bar/abcdefg
// /foo/baz             ../bar/abc*            /foo/bar/abcdefg
static char *vdev_resolve_path(struct vdev *vdev, char *path)
{
    char  *in;
    char  *out = vdev->pathbuf;
    size_t out_len = 0;
    size_t left = sizeof(vdev->pathbuf) - 1;
    // path format: [[/relative prefix | //absolute prefix]:]<name>[*]
    char     *prefix = path;
    size_t    prefix_len;
    char     *name;
    size_t    name_len;
    dir_t    *dir;
    dirent_t *dent;

    // If no FS is mounted, the path definitely cannot be resolved
    if (vdev->fs == NULL)
        return NULL;

    // 1. Handle the section before :
    if ((name = strchr(path, ':')) != NULL) {
        do {
            // `path` contains a path before :
            *name = 0;
            // Set `name` to next character after :
            name++;
            prefix_len = strlen(prefix);

            // Path starts with :
            if (prefix_len == 0)
                break;

            // Must start & end with /
            if (prefix[0] != '/' || prefix[prefix_len - 1] != '/')
                return NULL;

            // Prefix too long?
            if (prefix_len > left)
                return NULL;

            // Copy path starting at character after /
            //     For absolute paths, this character is /
            //     For relative paths, this character is not /
            memcpy(out, prefix + 1, prefix_len - 1);
            out += prefix_len - 1;
            out_len += prefix_len - 1;
            left -= prefix_len - 1;

            // `out` is guranteed to look like
            // /path/to/something/
            // or
            // path/to/something/
        } while (0);
    } else {
        name = path;
    }

    // 2. Handle the section after :
    name_len = strlen(name);

    // `name` must not be blank, exceed the output buffer size, or end with /
    if (name_len == 0 || name_len > left)
        return NULL;

    // Remove trailing / from file name
    if (name[name_len - 1] == '/')
        name_len--;

    // `name` will either be appended to an empty
    // `out`, or after a /
    memcpy(out, name, name_len);
    out_len += name_len;
    left -= name_len;

    // 3. If relative, prepend current directory to output
    //    Current directory always ends with /
    if (vdev->pathbuf[0] != '/') {
        // Must be able to fit `vdev->cwd`
        if (vdev->cwd_len > left)
            return NULL;

        memmove(vdev->pathbuf + vdev->cwd_len, vdev->pathbuf, out_len);
        memcpy(vdev->pathbuf, vdev->cwd, vdev->cwd_len);
        out_len += vdev->cwd_len;
    }

    // 4. Modify path to remove .. & . components
    // The path can only get shorter here, so it is
    // safe to reuse `left`
    left = out_len;
    in = vdev->pathbuf;
    out = in;
    out[out_len] = 0;
    while (left != 0) {
        if (in[0] == '/') {
            if (left >= 2 && in[1] == '/') {
                // Cannot have consecutive /
                return NULL;
            } else if (left >= 2 && in[1] == '.' && (in[2] == '/' || in[2] == 0)) {
                // Directory = ./ so do nothing
                in += 2;
                left -= 2;
                continue;
            } else if (left >= 3 && in[1] == '.' && in[2] == '.' && (in[3] == '/' || in[3] == 0)) {
                // Directory = ../ so rewind `out` to the last /
                if (*out == '/' && out != vdev->pathbuf)
                    out--;
                while ((out == in || *out != '/') && out != vdev->pathbuf)
                    out--;
                in += 3;
                left -= 3;
                continue;
            }
        }
        *(out++) = *(in++);
        left--;
    }
    *out = 0;
    out = vdev->pathbuf;
    out_len = strlen(out);
    left = sizeof(vdev->pathbuf) - out_len - 1;

    // 5. This path ends with a glob
    if (out[out_len - 1] == '*') {
        out[out_len - 1] = 0;
        // Set `name` to the end of the output path
        name = out + (out_len - 1);
        // Rewind `name` to the the last /, or the start
        // of the path
        while (*name != '/' && name != vdev->pathbuf) {
            name--;
        }

        *name = 0;
        out_len = strlen(out);
        // Need to iterate the directory contents to
        // resolve the glob
        if ((vdev->fs->opendir(vdev->fs, vdev->pathbuf, &dir)).code != D_OK)
            return NULL;
        *name = '/';
        name++;
        name_len = strlen(name);

        while ((dent = dir->next(dir)) != NULL) {
            // Check every directory entry for a match starting
            // with `name`
            if (strncasecmp(name, dent->name, name_len) == 0) {
                name_len = strlen(dent->name);
                if (out_len + name_len > left) {
                    dir->close(dir);
                    return NULL;
                }
                memcpy(name, dent->name, name_len);
                name[name_len] = 0;
                break;
            }
        }

        dir->close(dir);
    }

    return out;
}

static bool vdev_ensure_fs_mounted(struct vdev *vdev)
{
    if (vdev->fs == NULL) {
        vdev_set_status(vdev, DOS_ERR(NOT_MOUNTED));
        return false;
    } else {
        return true;
    }
}
