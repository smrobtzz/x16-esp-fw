// SPDX-License-Identifier: GPL-3.0-only
#include <stddef.h>
#include <stdlib.h>

#include <driver/spi_slave_hd.h>
#include <driver/uart.h>
#include <esp_log.h>

#include <lwip/dns.h>
#include <lwip/icmp.h>
#include <lwip/inet_chksum.h>
#include <lwip/ip_addr.h>
#include <lwip/raw.h>
#include <lwip/tcp.h>
#include <lwip/tcpip.h>
#include <lwip/udp.h>
#include <sys/param.h>

#include "defs.h"
#include "device.h"
#include "net.h"
#include "serial.h"

static struct net_socket *s_sockets;

void net_iface_start(struct device_iface *i);
void net_iface_cmd(struct device_iface *i, uint8_t cmd, uint8_t *data);
void net_iface_stop(struct device_iface *i);

ip_addr_t     *net_ip2lwip(struct net_ip *ip);
struct net_ip *net_lwip2ip(ip_addr_t *ip);

uint8_t net_open(struct net_iface *net, struct net_ip *ip, uint8_t socket, uint8_t type, uint8_t arg0, uint8_t arg1);
uint8_t net_close(struct net_iface *net, uint8_t socket);
uint8_t net_send(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t size, uint16_t *size_out);
uint8_t net_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t size, uint16_t *size_out);
uint8_t net_gethostbyname(struct net_iface *net, uint8_t *data, struct net_ip *ip_out);

uint8_t net_sock_dbg_send(uint8_t *data, uint16_t size);
uint8_t net_sock_serial_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t len, uint16_t *size_out);

void net_sock_add_pbuf(struct net_socket *s, struct pbuf *pb);
void net_sock_update_status(struct net_socket *s);
void net_serial_did_receive();

err_t net_tcp_recv_fn(void *arg, struct tcp_pcb *pcb, struct pbuf *pb, err_t err);
void  net_tcp_err_fn(void *arg, err_t err);
err_t net_tcp_sent_fn(void *arg, struct tcp_pcb *pcb, uint16_t len);

void    net_udp_recv_fn(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, uint16_t port);
uint8_t net_sock_udp_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t len, uint16_t *size_out);

void net_dns_found_cb(const char *name, const ip_addr_t *ip, void *arg);

void net_sock_add_pbuf(struct net_socket *s, struct pbuf *pb)
{
    struct net_buf *buf = malloc(sizeof(struct net_buf));
    if (buf == NULL) {
        LOG(net, "failed to allocate net_buf");
        return;
    }

    buf->data = malloc(pb->len);
    buf->len = pb->len;
    buf->pos = 0;
    buf->next = NULL;
    if (buf->data == NULL) {
        LOG(net, "failed to allocate net_buf->data");
        free(buf);
        return;
    }

    memcpy(buf->data, pb->payload, pb->len);

    xSemaphoreTake(s->socklock, portMAX_DELAY);
    if (s->btail != NULL)
        s->btail->next = buf;
    s->btail = buf;

    if (s->bhead == NULL)
        s->bhead = buf;

    s->status |= SOCK_STAT_HAS_DATA;
    net_sock_update_status(s);
    xSemaphoreGive(s->socklock);
}

void net_sock_update_status(struct net_socket *s)
{
    spi_slave_hd_write_buffer(SPI2_HOST, 10 + s->id, &s->status, 1);
}

uint8_t net_raw_recv_fn(void *arg, struct raw_pcb *pcb, struct pbuf *pb, const ip_addr_t *addr)
{
    struct net_socket *s = (struct net_socket *)arg;

    net_sock_add_pbuf(s, pb);
    pbuf_free(pb);

    return 1;
}

err_t net_tcp_connected_fn(void *arg, struct tcp_pcb *pcb, err_t err)
{
    struct net_socket *s = (struct net_socket *)arg;

    s->status |= SOCK_STAT_CONNECTED;
    net_sock_update_status(s);

    return ERR_OK;
}

err_t net_tcp_recv_fn(void *arg, struct tcp_pcb *pcb, struct pbuf *pb, err_t err)
{
    static uint16_t    num_bytes = 0;
    struct net_socket *s = (struct net_socket *)arg;

    if (pb == NULL)
        return ERR_OK;

    net_sock_add_pbuf(s, pb);
    tcp_recved(pcb, pb->len);
    num_bytes += pb->len;

    LOG_IF(net, "tcp_recv %u bytes", (unsigned)num_bytes);
    pbuf_free(pb);

    return ERR_OK;
}

void net_tcp_err_fn(void *arg, err_t err)
{
    struct net_socket *s = (struct net_socket *)arg;

    // If this function is called, the socket was automatically
    // disconnected
    xSemaphoreTake(s->socklock, portMAX_DELAY);
    s->status &= ~SOCK_STAT_CONNECTED;
    switch (err) {
    case ERR_ABRT:
        s->status |= SOCK_ERR_ABRT;
        break;
    case ERR_RST:
        s->status |= SOCK_ERR_RST;
        break;
    case ERR_CLSD:
        s->status |= SOCK_ERR_CLSD;
        break;
    default:
        s->status |= SOCK_ERR_OTHER;
        break;
    }
    net_sock_update_status(s);
    xSemaphoreGive(s->socklock);
    LOG(net, "tcp_err %d", (int)err);
}

err_t net_tcp_sent_fn(void *arg, struct tcp_pcb *pcb, uint16_t len)
{
    LOG_IF(net, "tcp_sent %u", (unsigned)len);
    return ERR_OK;
}

struct device_iface *net_init()
{
    ip_addr_t         dns_ip;
    struct net_iface *net_iface = malloc(sizeof(struct net_iface));
    if (net_iface == NULL)
        return NULL;

    *net_iface = (struct net_iface) {
        .d = (struct device_iface) {
            .start = net_iface_start,
            .cmd = net_iface_cmd,
            .stop = net_iface_stop,
            .cmd_start = NET_CMD_START,
            .cmd_end = NET_CMD_END }
    };
    memset(net_iface->s, 0, sizeof(struct net_socket) * 4);
    net_iface->s[0].id = 0;
    net_iface->s[0].type = NET_SOCK_INVALID;
    net_iface->s[0].socklock = xSemaphoreCreateMutex();
    net_iface->s[1].id = 1;
    net_iface->s[1].type = NET_SOCK_INVALID;
    net_iface->s[1].socklock = xSemaphoreCreateMutex();
    net_iface->s[2].id = 2;
    net_iface->s[2].type = NET_SOCK_INVALID;
    net_iface->s[2].socklock = xSemaphoreCreateMutex();
    net_iface->s[3].id = 3;
    net_iface->s[3].type = NET_SOCK_INVALID;
    net_iface->s[3].socklock = xSemaphoreCreateMutex();

    s_sockets = net_iface->s;

    LOCK_TCPIP_CORE();
    dns_init();
    ipaddr_aton("1.1.1.1", &dns_ip);
    dns_setserver(0, &dns_ip);
    UNLOCK_TCPIP_CORE();

    return (struct device_iface *)net_iface;
}

void net_iface_start(struct device_iface *i)
{
}

void net_iface_cmd(struct device_iface *di, uint8_t cmd, uint8_t *data)
{
    struct net_iface *net = (struct net_iface *)di;
    switch (cmd) {
    case NET_CMD_OPEN: {
        struct net_open_args args;
        memcpy(&args, data + 4, sizeof(struct net_open_args));

        data[0] = 1;
        data[1] = net_open(net, &args.addr, args.socket, args.type, args.arg0, args.arg1);
    } break;
    case NET_CMD_CLOSE: {
        struct net_close_args args;
        memcpy(&args, data + 4, sizeof(struct net_close_args));

        data[0] = 1;
        data[1] = net_close(net, args.socket);
    } break;
    case NET_CMD_SEND: {
        struct net_send_args args;
        memcpy(&args, data + 4, sizeof(struct net_send_args));

        if (args.size > NET_MAX_BUF_SIZE)
            args.size = NET_MAX_BUF_SIZE;

        data[0] = 3;
        data[1] = net_send(net, args.socket, data + 4 + sizeof(struct net_send_args) + 1, args.size, (uint16_t *)(data + 2));
    } break;
    case NET_CMD_RECV: {
        struct net_recv_args args;
        memcpy(&args, data + 4, sizeof(struct net_recv_args));

        if (args.size > NET_MAX_BUF_SIZE)
            args.size = NET_MAX_BUF_SIZE;

        data[0] = 3;
        data[1] = net_recv(net, args.socket, data + 4, args.size, (uint16_t *)(data + 2));
    } break;
    case NET_CMD_GETHOSTBYNAME:
        data[0] = 18;
        data[19] = net_gethostbyname(net, data + 4, (struct net_ip *)(data + 1));
        break;
    default:
        data[0] = 0;
        break;
    }
}

void net_iface_stop(struct device_iface *i)
{
    struct net_iface *net = (struct net_iface *)i;

    for (uint8_t i = 0; i < 4; i++) {
        net_close(net, i);
    }
}

ip_addr_t *net_ip2lwip(struct net_ip *ip)
{
    static ip_addr_t lwip_ip;
    memcpy(&lwip_ip.u_addr, ip->data, 16);
    lwip_ip.type = (ip->type == IP_V4) ? 0 : 6;
    return &lwip_ip;
}

struct net_ip *net_lwip2ip(ip_addr_t *ip)
{
    static struct net_ip net_ip;
    memcpy(net_ip.data, &ip->u_addr, 16);
    net_ip.type = (ip->type == 0) ? IP_V4 : IP_V6;
    return &net_ip;
}

uint8_t net_open(struct net_iface *net, struct net_ip *ip, uint8_t socket, uint8_t type, uint8_t arg0, uint8_t arg1)
{
    struct net_socket *s;
    LOG(net, "net_open(%s, %u, %u, %u, %u)", ipaddr_ntoa(net_ip2lwip(ip)), (unsigned)socket, (unsigned)type, (unsigned)arg0, (unsigned)arg1);

    s = &net->s[socket];
    xSemaphoreTake(s->socklock, portMAX_DELAY);
    s->type = type;
    s->ip = *ip;
    s->status = 0;
    net_sock_update_status(s);
    xSemaphoreGive(s->socklock);

    switch (type) {
    case NET_SOCK_RAW: {
        LOCK_TCPIP_CORE();
        s->raw_pcb = raw_new(arg0);
        raw_recv(s->raw_pcb, net_raw_recv_fn, s);
        UNLOCK_TCPIP_CORE();
    } break;
    case NET_SOCK_TCP: {
        LOCK_TCPIP_CORE();
        s->tcp_pcb = tcp_new();
        tcp_connect(s->tcp_pcb, net_ip2lwip(&s->ip), (uint16_t)arg0 | ((uint16_t)arg1 << 8), net_tcp_connected_fn);

        tcp_arg(s->tcp_pcb, s);
        tcp_recv(s->tcp_pcb, net_tcp_recv_fn);
        tcp_err(s->tcp_pcb, net_tcp_err_fn);
        tcp_sent(s->tcp_pcb, net_tcp_sent_fn);
        tcp_nagle_disable(s->tcp_pcb);
        UNLOCK_TCPIP_CORE();
    } break;
    case NET_SOCK_DBG:
        break;
    case NET_SOCK_SERIAL:
        return serial_set_params(arg0, arg1);
    case NET_SOCK_UDP:
        LOCK_TCPIP_CORE();
        s->udp_pcb = udp_new();
        udp_bind(s->udp_pcb, IP_ANY_TYPE, (uint16_t)arg0 | ((uint16_t)arg1 << 8));
        udp_recv(s->udp_pcb, net_udp_recv_fn, s);
        UNLOCK_TCPIP_CORE();
        break;
    default:
        return 1;
    }

    return 0;
}

uint8_t net_close(struct net_iface *net, uint8_t socket)
{
    struct net_socket *s = &net->s[socket];
    LOG(net, "net_close(%u)", socket);

    switch (s->type) {
    case NET_SOCK_RAW:
        LOCK_TCPIP_CORE();
        raw_remove(s->raw_pcb);
        UNLOCK_TCPIP_CORE();
        break;
    case NET_SOCK_TCP:
        LOCK_TCPIP_CORE();
        if ((s->status & SOCK_STAT_CONNECTED) != 0)
            tcp_close(s->tcp_pcb);
        UNLOCK_TCPIP_CORE();
        break;
    case NET_SOCK_UDP:
        LOCK_TCPIP_CORE();
        udp_remove(s->udp_pcb);
        UNLOCK_TCPIP_CORE();
        break;
    case NET_SOCK_INVALID:
        return 1;
    default:
        break;
    }
    s->type = NET_SOCK_INVALID;

    xSemaphoreTake(s->socklock, portMAX_DELAY);
    for (struct net_buf *buf = s->bhead; buf != NULL;) {
        struct net_buf *next = buf->next;
        free(buf->data);
        free(buf);
        buf = next;
    }

    s->bhead = NULL;
    s->btail = NULL;
    s->status &= ~SOCK_STAT_CONNECTED;
    net_sock_update_status(s);
    xSemaphoreGive(s->socklock);

    return 0;
}

uint8_t net_send(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t size, uint16_t *size_out)
{
    struct net_socket *s = &net->s[socket];

    LOG_IF(net, "net_send(%u, %u)", (unsigned)socket, (unsigned)size);

    switch (s->type) {
    case NET_SOCK_RAW: {
        struct pbuf *pb;

        LOCK_TCPIP_CORE();
        pb = pbuf_alloc(PBUF_IP, size, PBUF_RAM);
        memcpy(pb->payload, data, size);
        raw_sendto(s->raw_pcb, pb, net_ip2lwip(&s->ip));
        UNLOCK_TCPIP_CORE();

        *size_out = size;
    }
        return 0;
    case NET_SOCK_TCP: {
        err_t err;

        LOCK_TCPIP_CORE();
        if ((s->status & SOCK_STAT_CONNECTED) != 0) {
            err = tcp_write(s->tcp_pcb, data, size, TCP_WRITE_FLAG_COPY);
            if (err != ERR_OK)
                LOG(net, "tcp_write error: %d", (int)err);

            err = tcp_output(s->tcp_pcb);
            if (err != ERR_OK)
                LOG(net, "tcp_output error: %d", (int)err);
        }
        UNLOCK_TCPIP_CORE();

        *size_out = size;
    }
        return 0;
    case NET_SOCK_DBG:
        *size_out = size;
        return net_sock_dbg_send(data, size);
    case NET_SOCK_SERIAL:
        *size_out = size;
        uart_write_bytes(UART_NUM, data, size);
        return 0;
    default:
        break;
    }

    return 1;
}

uint8_t net_sock_dbg_send(uint8_t *data, uint16_t size)
{
    uint8_t *dat = data;
    uint16_t left = size;
    uint16_t pos = 0;
    unsigned uval = 0;
    int      ival = 0;

    while (left > 1) {
        dat = data + pos;
        uval = 0;
        ival = 0;
        switch (dat[0]) {
        case DBG_TAG_STR: {
            size_t len = strnlen((const char *)(dat + 1), left - 1);
            if (len == 0) {
                LOG(net, "DBG_TAG_STR len = 0");
                return 0;
            } else if (len >= left - 1) {
                LOG(net, "DBG_TAG_STR data too long by %u bytes", len - left - 1);
                return 0;
            }

            printf("%s", (const char *)(dat + 1));
            left -= len + 2;
            pos += len + 2;
        } break;
        case DBG_TAG_UINT32:
            if (left < 6)
                return 0;
            uval |= ((uint32_t)dat[5]) << 24;
            uval |= ((uint32_t)dat[4]) << 16;
            left -= 2;
            pos += 2;
            // fall through
        case DBG_TAG_UINT16:
            if (left < 4)
                return 0;
            uval |= ((uint32_t)dat[3]) << 8;
            left -= 1;
            pos += 1;
            // fall through
        case DBG_TAG_UINT8:
            if (left < 3)
                return 0;
            uval |= ((uint32_t)dat[2]);
            left -= 3;
            pos += 3;
            if (dat[1] == DBG_BASE_HEX) {
                printf("%x", uval);
            } else if (dat[1] == DBG_BASE_DEC) {
                printf("%u", uval);
            }
            break;
        case DBG_TAG_INT32:
            if (left < 5)
                return 0;
            ival |= ((uint32_t)dat[4]) << 24;
            ival |= ((uint32_t)dat[3]) << 16;
            left -= 2;
            pos += 2;
            // fall through
        case DBG_TAG_INT16:
            if (left < 4)
                return 0;
            ival |= ((uint32_t)dat[2]) << 8;
            left -= 1;
            pos += 1;
            // fall through
        case DBG_TAG_INT8:
            if (left < 2)
                return 0;
            ival |= ((uint32_t)dat[1]);
            left -= 2;
            pos += 2;
            printf("%d", ival);
            break;
        }
    }
    return 1;
}

uint8_t net_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t len, uint16_t *size_out)
{
    uint16_t           left = len, taken = 0;
    struct net_socket *s = &net->s[socket];
    LOG_IF(net, "net_recv(%u, %u)", (unsigned)socket, (unsigned)len);

    if (s->type == NET_SOCK_SERIAL)
        return net_sock_serial_recv(net, socket, data, len, size_out);
    if (s->type == NET_SOCK_UDP)
        return net_sock_udp_recv(net, socket, data, len, size_out);

    *size_out = 0;
    while (left > 0) {
        struct net_buf *buf = s->bhead;
        if (buf == NULL)
            break;

        taken = MIN(buf->len, left);
        memcpy(data, buf->data + buf->pos, taken);
        buf->pos += taken;
        buf->len -= taken;
        left -= taken;
        *size_out += taken;
        data += taken;

        if (buf->len == 0) {
            xSemaphoreTake(s->socklock, portMAX_DELAY);
            s->bhead = buf->next;

            if (s->bhead == NULL) {
                s->btail = NULL;
                s->status &= ~SOCK_STAT_HAS_DATA;
                net_sock_update_status(s);
            }
            xSemaphoreGive(s->socklock);

            free(buf->data);
            free(buf);
        }
    }

    return 0;
}

uint8_t net_sock_serial_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t len, uint16_t *size_out)
{
    size_t             left = 0;
    struct net_socket *s = &net->s[socket];

    xSemaphoreTake(s->socklock, portMAX_DELAY);
    *size_out = uart_read_bytes(UART_NUM, data, len, 4);
    uart_get_buffered_data_len(UART_NUM, &left);

    if (left == 0) {
        s->status &= ~SOCK_STAT_HAS_DATA;
        net_sock_update_status(s);
    }
    xSemaphoreGive(s->socklock);

    return 0;
}

uint8_t net_gethostbyname(struct net_iface *net, uint8_t *data, struct net_ip *ip_out)
{
    struct net_dns_found_cb_args args;
    ip_addr_t                    ip;
    err_t                        res;
    data[data[0] + 1] = 0;

    LOG_IF(net, "net_gethostbyname(%s)", data + 1);

    args.task = xTaskGetCurrentTaskHandle();
    args.ip = &ip;

    LOCK_TCPIP_CORE();
    res = dns_gethostbyname((const char *)(data + 1), &ip, net_dns_found_cb, &args);
    UNLOCK_TCPIP_CORE();

    if (res == ERR_INPROGRESS) {
        ulTaskNotifyTakeIndexed(0, pdFALSE, portMAX_DELAY);
    }

    LOG_IF(net, "-> %s", ipaddr_ntoa(&ip));
    memcpy(ip_out, net_lwip2ip(&ip), sizeof(struct net_ip));

    return 0;
}

void net_dns_found_cb(const char *name, const ip_addr_t *ip, void *arg)
{
    struct net_dns_found_cb_args *args = (struct net_dns_found_cb_args *)arg;
    *args->ip = *ip;
    xTaskNotifyIndexed(args->task, 0, 1, eSetValueWithOverwrite);
}

void net_serial_did_receive()
{
    for (int i = 0; i < 4; i++) {
        struct net_socket *s = &s_sockets[i];
        xSemaphoreTake(s->socklock, portMAX_DELAY);

        if (s->type == NET_SOCK_SERIAL) {
            s->status |= SOCK_STAT_HAS_DATA;
            net_sock_update_status(s);
        }

        xSemaphoreGive(s->socklock);
    }
}

void net_udp_recv_fn(void *arg, struct udp_pcb *pcb, struct pbuf *pb, const ip_addr_t *addr, uint16_t port)
{
    struct net_socket *s = (struct net_socket *)arg;
    struct net_buf    *buf = malloc(sizeof(struct net_buf));
    size_t             buf_len = pb->len + sizeof(struct net_ip) + 2;

    if (buf == NULL) {
        LOG(net, "failed to allocate udp net_buf");
        pbuf_free(pb);
    }

    buf->data = malloc(buf_len);
    if (buf->data == NULL) {
        LOG(net, "failed to allocate udp buffer data");
        free(buf);
        pbuf_free(pb);
        return;
    }

    buf->len = buf_len;
    buf->next = NULL;

    memcpy(buf->data, addr, sizeof(struct net_ip));
    memcpy(buf->data + sizeof(struct net_ip), &port, 2);
    memcpy(buf->data + sizeof(struct net_ip) + 2, pb->payload, pb->len);

    xSemaphoreTake(s->socklock, portMAX_DELAY);
    if (s->btail != NULL)
        s->btail->next = buf;
    s->btail = buf;

    if (s->bhead == NULL)
        s->bhead = buf;

    s->status |= SOCK_STAT_HAS_DATA;
    net_sock_update_status(s);
    xSemaphoreGive(s->socklock);

    pbuf_free(pb);
}

uint8_t net_sock_udp_recv(struct net_iface *net, uint8_t socket, uint8_t *data, uint16_t len, uint16_t *size_out)
{
    struct net_socket *s = &net->s[socket];
    struct net_buf    *buf = s->bhead;
    uint16_t           received = 0;

    *size_out = 0;
    if (buf == NULL)
        return 0;

    received = MIN(buf->len, len);
    memcpy(data, buf->data, received);
    *size_out = received;

    xSemaphoreTake(s->socklock, portMAX_DELAY);
    s->bhead = buf->next;

    if (s->bhead == NULL) {
        s->btail = NULL;
        s->status &= ~SOCK_STAT_HAS_DATA;
        net_sock_update_status(s);
    }
    xSemaphoreGive(s->socklock);

    free(buf);

    return 0;
}
