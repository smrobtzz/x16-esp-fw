// SPDX-License-Identifier: GPL-3.0-only
#include "defs.h"
#include <dirent.h>
#include <fcntl.h>
#include <string.h>
#include <sys/errno.h>
#include <sys/param.h>
#include <unistd.h>

#include <esp_http_server.h>
#include <esp_log.h>
#include <sys/stat.h>

struct http_hdlr {
    httpd_method_t method;
    esp_err_t (*handler)(httpd_req_t *r);
};

void webdav_prop_resp(httpd_req_t *req, char *path, bool recurse);

esp_err_t webdav_unimp(httpd_req_t *req);
esp_err_t webdav_get(httpd_req_t *req);
esp_err_t webdav_options(httpd_req_t *req);
esp_err_t webdav_propfind(httpd_req_t *req);
esp_err_t webdav_proppatch(httpd_req_t *req);
esp_err_t webdav_delete(httpd_req_t *req);
esp_err_t webdav_patch(httpd_req_t *req);
esp_err_t webdav_put(httpd_req_t *req);
esp_err_t webdav_mkcol(httpd_req_t *req);
esp_err_t webdav_lock(httpd_req_t *req);
esp_err_t webdav_unlock(httpd_req_t *req);
esp_err_t webdav_head(httpd_req_t *req);
esp_err_t webdav_move(httpd_req_t *req);

const struct http_hdlr HANDLERS[] = {
    { HTTP_GET, webdav_get },
    { HTTP_OPTIONS, webdav_options },
    { HTTP_PROPFIND, webdav_propfind },
    { HTTP_PROPPATCH, webdav_proppatch },
    { HTTP_DELETE, webdav_delete },
    { HTTP_PATCH, webdav_patch },
    { HTTP_PUT, webdav_put },
    { HTTP_MKCOL, webdav_mkcol },
    { HTTP_LOCK, webdav_lock },
    { HTTP_UNLOCK, webdav_unlock },
    { HTTP_HEAD, webdav_head },
    { HTTP_MOVE, webdav_move },

    { HTTP_POST, webdav_unimp },
    { HTTP_CONNECT, webdav_unimp },
    { HTTP_COPY, webdav_unimp },
};

const char *MONTHS[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
const char *DAYS[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

const char *ALLOW_FILE = "PROPFIND,OPTIONS,DELETE,COPY,MOVE,HEAD,POST,PUT,LOCK,UNLOCK";
const char *ALLOW_DIR = "PROPFIND,OPTIONS,DELETE,COPY,MOVE";

static char s_pathbuf[PATH_MAX + 1] = "/sd0p0";

char *webdav_path(char *path)
{
    size_t path_len = strlen(path);
    size_t off = 6;

    if (path[0] != '/') {
        s_pathbuf[6] = '/';
        off = 7;
    }

    assert(path_len + off < PATH_MAX);
    memcpy(s_pathbuf + off, path, path_len);
    s_pathbuf[off + path_len] = 0;

    return s_pathbuf;
}

bool webdav_range(char *range, size_t *start, size_t *end)
{
    if (strncmp(range, "bytes=", 6) != 0)
        return true;

    *start = strtol(range + 6, &range, 10);
    if (*range == '\0') {
        *end = SIZE_MAX;
    } else {
        *end = strtol(range + 1, NULL, 10);
    }
    return false;
}

void webdav_init()
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn = httpd_uri_match_wildcard;
    config.max_uri_handlers = 24;
    config.enable_so_linger = true;
    config.lru_purge_enable = true;
    config.stack_size = 16384;
    httpd_handle_t server;
    httpd_uri_t    hdlr = {
           .uri = "*",
           .user_ctx = NULL
    };
    int res;

    res = httpd_start(&server, &config);
    if (res != ESP_OK)
        goto fail;

    for (int i = 0; i < sizeof(HANDLERS) / sizeof(struct http_hdlr); i++) {
        hdlr.method = HANDLERS[i].method;
        hdlr.handler = HANDLERS[i].handler;
        hdlr.user_ctx = (void *)HANDLERS[i].method;
        httpd_register_uri_handler(server, &hdlr);
    }

    return;
fail:
    LOG(webdav, "failed to start server: %s", esp_err_to_name(res));
}

esp_err_t webdav_unimp(httpd_req_t *req)
{
    LOG(webdav, "unimplemented method %u", (unsigned)req->user_ctx);
    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_get(httpd_req_t *req)
{
    int         fd, res;
    char        str[255];
    struct stat st;
    size_t      start, end, left;
    char       *path = webdav_path((char *)req->uri);
    uint8_t    *buf;

    LOG_IF(webdav, "GET \"%s\"", req->uri);

    if (stat(path, &st) < 0)
        return httpd_resp_send_404(req);
    if (S_ISDIR(st.st_mode))
        return httpd_resp_send(req, NULL, 0);

    fd = open(path, O_RDONLY);
    if (fd < 0)
        goto fail;

    if (httpd_req_get_hdr_value_str(req, "Range", str, 255) == ESP_OK) {
        if (webdav_range(str, &start, &end))
            goto fail;
        LOG_IF(webdav, "range: \"%s\", %u-%u", str, start, end);

        httpd_resp_set_status(req, "206 Partial Content");
        snprintf(str, 255, "bytes %u-%u/%ld", start, end, st.st_size);
        httpd_resp_set_hdr(req, "Content-Range", str);
    } else {
        start = 0;
        end = st.st_size - 1;
    }

    left = end - start + 1;
    snprintf(str, 255, "%u", left);
    httpd_resp_set_hdr(req, "Content-Length", str);

    lseek(fd, start, SEEK_SET);
    buf = malloc(MIN(left, 32768));
    if (buf == 0) {
        LOG(webdav, "failed allocating buffer for GET");
        goto fail;
    }

    while (left != 0) {
        res = read(fd, buf, MIN(left, 32768));
        left -= res;
        if (res < 0 || res == 0)
            break;
        httpd_resp_send_chunk(req, (char *)buf, res);
    }

    free(buf);
    close(fd);
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;

fail:
    close(fd);
    return httpd_resp_send_500(req);
}

esp_err_t webdav_options(httpd_req_t *req)
{
    LOG_IF(webdav, "OPTIONS");
    httpd_resp_set_hdr(req, "Allow",
        "DELETE,GET,HEAD,POST,PUT,CONNECT,"
        "OPTIONS,COPY,LOCK,MKCOL,MOVE,PROPFIND,"
        "PROPPATCH,UNLOCK");
    httpd_resp_set_hdr(req, "Dav", "1,2,sabredav-partialupdate");
    httpd_resp_send(req, NULL, 0);

    return ESP_OK;
}

esp_err_t webdav_propfind(httpd_req_t *req)
{
    char        str[9];
    struct stat st;
    bool        is_dir, recurse = true;

    LOG_IF(webdav, "PROPFIND \"%s\"", req->uri);

    if (stat(webdav_path((char *)req->uri), &st) < 0)
        return httpd_resp_send_404(req);
    is_dir = S_ISDIR(st.st_mode);

    httpd_resp_set_status(req, "207 Multi-Status");
    httpd_resp_set_type(req, "application/xml;charset=utf-8");

    if (httpd_req_get_hdr_value_str(req, "Depth", str, 9) == ESP_OK) {
        ESP_LOGE("webdav", "Depth: %s", str);
        if (memcmp(str, "0", 1) == 0)
            recurse = false;
    }

    if (is_dir)
        httpd_resp_set_hdr(req, "Allow", ALLOW_FILE);
    else
        httpd_resp_set_hdr(req, "Allow", ALLOW_DIR);

    httpd_resp_sendstr_chunk(req, "<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    httpd_resp_sendstr_chunk(req, "<D:multistatus xmlns:D=\"DAV:\">");

    webdav_prop_resp(req, (char *)req->uri, recurse);

    httpd_resp_sendstr_chunk(req, "</D:multistatus>");

    return httpd_resp_send_chunk(req, NULL, 0);
}

esp_err_t webdav_proppatch(httpd_req_t *req)
{
    LOG_IF(webdav, "PROPPATCH -> PROPFIND");
    return webdav_propfind(req);
}

void webdav_prop_resp(httpd_req_t *req, char *path, bool recurse)
{
    char          *full_path = webdav_path(path);
    struct tm     *mtime;
    char           str[255];
    int            res;
    bool           is_dir;
    DIR           *dir;
    struct dirent *ent;
    struct stat    st;
    int            ret;

    httpd_resp_sendstr_chunk(req, "<D:response><D:href>");
    if (path[0] == '/')
        ret = snprintf(str, 255, "http://%s.local%s", CONFIG_X_NET_NAME, path);
    else
        ret = snprintf(str, 255, "http://%s.local/%s", CONFIG_X_NET_NAME, path);
    assert(ret >= 0);
    httpd_resp_send_chunk(req, str, ret);
    httpd_resp_sendstr_chunk(req, "</D:href><D:propstat>");

    if (stat(full_path, &st) < 0) {
        ESP_LOGE("webdav", "stat %s failed", full_path);
        httpd_resp_sendstr_chunk(req,
            "<D:status>"
            "HTTP/1.1 500 Internal Server Error"
            "</D:status>"
            "</D:propstat>"
            "</D:response>");
        return;
    }
    is_dir = S_ISDIR(st.st_mode);

    httpd_resp_sendstr_chunk(req, "<D:status>HTTP/1.1 200 OK</D:status>");
    httpd_resp_sendstr_chunk(req, "<D:prop><D:getlastmodified>");

    mtime = gmtime(&st.st_mtime);
    res = snprintf(str, 255, "%s, %02d %s %04d %02d:%02d:%02d GMT",
        DAYS[mtime->tm_wday], mtime->tm_mday, MONTHS[mtime->tm_mon],
        1900 + mtime->tm_year, mtime->tm_hour, mtime->tm_min, mtime->tm_sec);
    assert(res >= 0);

    httpd_resp_send_chunk(req, str, res);
    httpd_resp_sendstr_chunk(req, "</D:getlastmodified>");

    if (is_dir) {
        httpd_resp_sendstr_chunk(req, "<D:resourcetype><D:collection/></D:resourcetype>");
    } else {
        httpd_resp_sendstr_chunk(req, "<D:resourcetype/><D:getcontentlength>");
        res = snprintf(str, 255, "%ld", st.st_size);
        assert(res >= 0);
        httpd_resp_send_chunk(req, str, res);
        httpd_resp_sendstr_chunk(req, "</D:getcontentlength>");
    }

    httpd_resp_sendstr_chunk(req, "</D:prop></D:propstat></D:response>");

    if (is_dir && recurse) {
        dir = opendir(full_path);
        if (dir == NULL)
            return;

        while ((ent = readdir(dir)) != NULL) {
#pragma GCC diagnostic ignored "-Wformat-truncation"
            if (path[strlen(path) - 1] == '/')
                snprintf(str, 255, "%s%s", path, ent->d_name);
            else
                snprintf(str, 255, "%s/%s", path, ent->d_name);
            webdav_prop_resp(req, str, false);
        }

        closedir(dir);
    }
}

esp_err_t webdav_delete(httpd_req_t *req)
{
    LOG_IF(webdav, "DELETE \"%s\"", req->uri);
    if (remove(webdav_path((char *)req->uri)) == -1) {
        if (errno == ENOENT)
            httpd_resp_send_404(req);
        else
            httpd_resp_send_500(req);
    }
    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_patch(httpd_req_t *req)
{
    char     str[32];
    size_t   start, end, left = req->content_len;
    uint8_t *buf = NULL;
    char    *path = webdav_path((char *)req->uri);
    int      fd, res;

    LOG_IF(webdav, "PATCH \"%s\"", req->uri);

    if (access(path, F_OK) != 0)
        httpd_resp_set_status(req, "201 Created");

    fd = open(path, O_WRONLY | O_CREAT);
    if (fd < 0)
        return httpd_resp_send_500(req);

    if (httpd_req_get_hdr_value_str(req, "X-Update-Range", str, 32) != ESP_OK)
        goto done;

    if (strncmp(str, "append", 6)) {
        lseek(fd, 0, SEEK_END);
    } else {
        if (webdav_range(str, &start, &end))
            goto done;
        lseek(fd, start, SEEK_SET);
    }

    buf = malloc(MIN(left, 32768));
    while (left != 0) {
        res = httpd_req_recv(req, (char *)buf, MIN(left, 32768));
        if (res == 0) {
            break;
        } else if (res < 0) {
            goto done;
        } else {
            left -= res;
            res = write(fd, buf, res);
            if (res < 0)
                goto done;
        }
    }

done:
    close(fd);
    free(buf);
    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_put(httpd_req_t *req)
{
    uint8_t *buf = NULL;
    size_t   left = req->content_len;
    char    *path = webdav_path((char *)req->uri);
    int      fd, res;
    LOG_IF(webdav, "PUT \"%s\"", path);

    if (access(path, F_OK) != 0)
        httpd_resp_set_status(req, "201 Created");

    fd = open(path, O_WRONLY | O_CREAT);
    if (fd < 0)
        return httpd_resp_send_500(req);

    buf = malloc(MIN(left, 8192));
    if (buf == NULL && left != 0) {
        close(fd);
        return httpd_resp_send_500(req);
    }

    if (ftruncate(fd, 0) < 0)
        goto done;

    while (left != 0) {
        res = httpd_req_recv(req, (char *)buf, MIN(left, 8192));
        if (res == 0) {
            break;
        } else if (res < 0) {
            goto done;
        } else {
            left -= res;
            res = write(fd, buf, res);
            if (res < 0)
                goto done;
        }
    }

done:
    close(fd);
    free(buf);
    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_mkcol(httpd_req_t *req)
{
    char *path = webdav_path((char *)req->uri);

    LOG_IF(webdav, "MKCOL \"%s\"", req->uri);

    if (mkdir(path, S_IRWXU) == -1) {
        switch (errno) {
        case EEXIST:
            httpd_resp_set_status(req, "405 Method Not Allowed");
            break;
        case ENOENT:
            httpd_resp_set_status(req, "409 Conflict");
            break;
        default:
            httpd_resp_set_status(req, "500 Internal Server Error");
            break;
        }
    } else {
        httpd_resp_set_status(req, "201 Created");
    }

    return httpd_resp_send(req, NULL, 0);
}

// macOS requires LOCK support for writing. Locking a file is impossible, so just pretend to
esp_err_t webdav_lock(httpd_req_t *req)
{
    LOG_IF(webdav, "LOCK \"%s\"", req->uri);

    httpd_resp_set_type(req, "application/xml;charset=utf-8");

    httpd_resp_set_hdr(req, "Allow", ALLOW_FILE);
    httpd_resp_set_hdr(req, "Lock-Token", "<some:token_idk>");

    httpd_resp_sendstr_chunk(req, "<?xml version=\"1.0\" encoding=\"utf-8\"?><D:prop xmlns:D=\"DAV:\"><D:lockdiscovery><D:activelock><D:locktype><write/></D:locktype><D:lockscope><exclusive/></D:lockscope><D:lockroot><D:href>");
    httpd_resp_sendstr_chunk(req, req->uri);
    httpd_resp_sendstr_chunk(req, "</D:href></D:lockroot><D:depth>Infinity</D:depth><D:timeout>Second-3600</D:timeout><d:locktoken><d:href>some:token_idk</d:href></d:locktoken></D:activelock></D:lockdiscovery></D:prop>");

    return httpd_resp_send_chunk(req, NULL, 0);
}

esp_err_t webdav_unlock(httpd_req_t *req)
{
    LOG_IF(webdav, "UNLOCK \"%s\"", req->uri);

    httpd_resp_set_hdr(req, "Allow", ALLOW_FILE);
    httpd_resp_set_hdr(req, "Lock-Token", "<some:token_idk>");

    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_head(httpd_req_t *req)
{
    char        str[21];
    struct stat st;
    char       *path = webdav_path((char *)req->uri);

    if (stat(path, &st) < 0)
        return httpd_resp_send_404(req);

    if (S_ISDIR(st.st_mode))
        return httpd_resp_send(req, NULL, 0);

    snprintf(str, 21, "%lu", st.st_size);
    httpd_resp_set_hdr(req, "Content-Length", str);
    return httpd_resp_send(req, NULL, 0);
}

esp_err_t webdav_move(httpd_req_t *req)
{
    char  *path = strdup(webdav_path((char *)req->uri));
    char   dest_buf[255];
    char  *dest_path = NULL;
    size_t len, slashes = 0;

    if (path == NULL)
        goto fail;
    if (httpd_req_get_hdr_value_str(req, "Destination", dest_buf, 255) != ESP_OK)
        goto fail;

    LOG_IF(webdav, "MOVE %s -> %s", path, dest_buf);

    len = strlen(dest_buf);

    // The Destination header contains a url instead of just a path,
    // so skip to the third / and hope that is the path we're looking for
    //
    // Ex:
    // http://calypso.local/something.txt   -->   /something.txt
    for (size_t i = 0; i < len; i++) {
        if (dest_buf[i] == '/') {
            slashes++;
            if (slashes == 3) {
                dest_path = webdav_path(&dest_buf[i]);
                break;
            }
        }
    }

    if (dest_path == NULL)
        goto fail;

    LOG_IF(webdav, "MOVE %s -> %s", path, dest_path);
    if (rename(path, dest_path) != 0) {
        LOG(webdav, "MOVE rename() failed: %d", errno);
        goto fail;
    }

    free(path);
    httpd_resp_set_hdr(req, "Location", dest_buf);
    httpd_resp_set_status(req, "201 Created");
    return httpd_resp_send(req, NULL, 0);

fail:
    free(path);
    return httpd_resp_send_500(req);
}
