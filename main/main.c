// SPDX-License-Identifier: GPL-3.0-only
#include <driver/gpio.h>
#include <esp_log.h>
#include <esp_wifi.h>
#include <mdns.h>
#include <nvs_flash.h>

#include "esp_vfs.h"
#include "esp_vfs_fat.h"

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <sdkconfig.h>

#include <smb2/libsmb2.h>
#include <smb2/smb2.h>

#include "config.h"
#include "defs.h"
#include "disk.h"
#include "dos.h"
#include "flash.h"
#include "hid.h"
#include "remote.h"
#include "sd.h"
#include "spi.h"
#include "wifi.h"

extern void webdav_init();
extern void config_task(void *);
extern void serial_task(void *);

QueueHandle_t g_scr_dat_queue;

static void pinit()
{
    gpio_config_t g = {
        .pull_up_en = GPIO_PULLUP_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE,
    };

#if CONFIG_X_SD_ENABLE
    gpio_hold_en(CONFIG_X_GPIO_SD_EN);
    g.pin_bit_mask = (1ULL << CONFIG_X_GPIO_SD_EN);
    g.mode = GPIO_MODE_INPUT_OUTPUT;
    gpio_config(&g);
    gpio_set_level(CONFIG_X_GPIO_SD_EN, 0);
    gpio_hold_dis(CONFIG_X_GPIO_SD_EN);

    g.pin_bit_mask = (1ULL << CONFIG_X_GPIO_SD_MOSI) | (1ULL << CONFIG_X_GPIO_SD_MISO)
        | (1ULL << CONFIG_X_GPIO_SD_SCLK) | (1ULL << CONFIG_X_GPIO_SD_CS);
    g.mode = GPIO_MODE_INPUT;
    gpio_config(&g);
#endif

#if CONFIG_X_SPI_DEV_ENABLE
    g.pin_bit_mask = (1ULL << CONFIG_X_GPIO_FPGA_MOSI) | (1ULL << CONFIG_X_GPIO_FPGA_MISO)
        | (1ULL << CONFIG_X_GPIO_FPGA_SCLK) | (1ULL << CONFIG_X_GPIO_FPGA_CS);
    g.mode = GPIO_MODE_INPUT;
    gpio_config(&g);
    gpio_set_pull_mode(CONFIG_X_GPIO_FPGA_CS, GPIO_PULLUP_ONLY);
#endif

    g.pin_bit_mask = (1ULL << CONFIG_X_GPIO_RESET) | (1ULL << CONFIG_X_GPIO_CDONE);
    g.mode = GPIO_MODE_INPUT_OUTPUT_OD;
    gpio_config(&g);
    gpio_set_level(CONFIG_X_GPIO_CDONE, 1);
}

void app_main(void)
{
    gpio_install_isr_service(ESP_INTR_FLAG_LEVEL3);
    pinit();

    ESP_LOGE(TAG, "started");

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES
        || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    wifi_init_sta();

    gpio_set_level(CONFIG_X_GPIO_RESET, 0);

    ESP_ERROR_CHECK_WITHOUT_ABORT(mdns_init());

    config_load();
    disk_init();
    sd_init();

    xTaskCreate(reset_task, "Reset", 4096, NULL, 3, &g_reset_task);
    xTaskCreate(config_task, "Config", 4096, NULL, 1, &g_config_task);

#if CONFIG_X_WEBDAV_ENABLE
    webdav_init();
#endif

    hid_init();

#if CONFIG_X_DOS_ENABLE
    xTaskCreate(device_task, "Device", 8192, NULL, 3, &g_dos_task);
#endif

#if CONFIG_X_I2C_ENABLE
    xTaskCreate(i2c_task, "I2C", 16384, NULL, 1, &g_i2c_task);
#endif

#if CONFIG_X_VERA_FLASH_ENABLE
    xTaskCreate(flash_task, "Flash", 4096, NULL, 1, &g_flash_task);
#endif

#if CONFIG_X_USB_HOST_ENABLE
    xTaskCreate(usb_task, "USB", 4096, NULL, 2, NULL);
#endif

    xTaskCreate(serial_task, "Serial", 4096, NULL, 1, NULL);

    remote_init();
}
