## How to build
Install esp-idf, then:
```
git clone --recursive https://codeberg.org/smrobtzz/x16-esp-fw
idf.py build
```

## Features
These features are available after setting up Calypso.

[caly](https://codeberg.org/smrobtzz/caly) is a utility that runs on Windows, macOS, and Linux and provides easy, scriptable access to many of these features.

#### WebDAV

A WebDAV server is available at http://calypso.local. You can use it to move files to and from your X16's SD card without removing the SD card and even while your X16 is in use.

For automating file transfers, `caly` may also be used.

Some examples:

Copy the folder `HANGMAN` to X16:
```
caly upload HANGMAN
```

Copy the folder `SOURCE` from X16:
```
caly download SOURCE
```

#### USB keyboard or mouse
USB keyboards and mice can be connected to the built-in USB Type-A port on Calypso and will work just like PS/2 keyboards and mice do. Hubs are not currently supported, and some devices may not work.

#### Load & run PRG on X16
A PRG file can be run remotely from another computer. This feature (ab)uses the AUTOBOOT.X16 functionality, so your existing AUTOBOOT.X16 will not run.

For example, to run the program `TEST.PRG`, run:
```
caly run TEST.PRG
```

You can also specify the `--hard` option, which performs a hard reset of the X16. This may be needed if your program has used `stp` or has the default IRQ handler disabled:
```
caly run --hard TEST.PRG
```

#### ESP32 OTA updates
The ESP32 can be updated over WiFi.

For example, to update the firmware with the file `build/x16_esp_fw.bin`, run:
```
caly update x16_esp_fw.bin
```

#### VERA updates
VERA can be flashed using the following command (assuming `VERA.BIN` contains the VERA bitstream release you want to install):
```
caly vera --flash VERA.BIN
```

You can also dump the existing VERA firmware using:
```
caly vera --download VERA_OUT.BIN
```

#### Remote control mode
You can control your X16's mouse and keyboard remotely from another computer using caly:
```
caly remote
```

#### SMB Shares, USB Mass Storage, and Multiple DOS Devices

If you assign Calypso to multiple device numbers in the X16 control panel,
those devices can each map to a different volume (partition, network share, etc.)

Using the "X-M" and "X-U" commands you can mount and unmount volumes to a device number.

Some examples:
| Syntax | Usage |
|--------|-------|
| `@"X-U"`            | Unmount the current volume |
| `@"X-M:vol:sd0p0"`  | Mount the first partition on the SD card |
| `@"X-M:vol:usb0p0"` | Mount the first partition on a USB mass storage device |
| `@"X-M:vol:usb0p1"` | Mount the second partition on a USB mass storage device |
| `@"X-M:smb://guest@192.168.123.234/guest_share` | As user `guest`, mount the SMB share `guest_share` located at `192.168.123.234` |
